package com.guillaumevdn.supremeshops;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SSLocaleMisc {

	private static Text n(String id, Object... content) {
		return new Text(id, SSLocale.file, content);
	}

	private static List<String> l(String... content) {
		return Utils.asList(content);
	}

	public static final Text MISC_SUPREMESHOPS_PERIODMONTHLY = n("MISC_SUPREMESHOPS_PERIODMONTHLY",
			"en_US", l("monthly"),
			"fr_FR", l("mensuel"),
			"zh_TW", l("&f每月"),
			"ru_RU", l("ежемесячно"),
			"hu_HU", l("havi"),
			"es_ES", l("Mensual")
			);

	public static final Text MISC_SUPREMESHOPS_PERIODWEEKLY = n("MISC_SUPREMESHOPS_PERIODWEEKLY",
			"en_US", l("weekly"),
			"fr_FR", l("hebdomadaire"),
			"zh_TW", l("&f每周"),
			"ru_RU", l("еженедельно"),
			"hu_HU", l("heti"),
			"es_ES", l("Semanal")
			);

	public static final Text MISC_SUPREMESHOPS_DAILY = n("MISC_SUPREMESHOPS_DAILY",
			"en_US", l("daily"),
			"fr_FR", l("quotidien"),
			"zh_TW", l("&f每日"),
			"ru_RU", l("ежедневно"),
			"hu_HU", l("napi"),
			"es_ES", l("diario")
			);

	public static final Text MISC_SUPREMESHOPS_MONEYFORMAT = n("MISC_SUPREMESHOPS_MONEYFORMAT",
			"en_US", l("{amount}$"),
			"fr_FR", l("{amount}$"),
			"zh_CN", l("{amount}$"),
			"zh_TW", l("{amount}$"),
			"ru_RU", l("{amount}$"),
			"hu_HU", l("{amount}$"),
			"es_ES", l("{amount}$")
			);

	public static final Text MISC_SUPREMESHOPS_XPLEVELFORMAT = n("MISC_SUPREMESHOPS_XPLEVELFORMAT",
			"en_US", l("{amount} LVL"),
			"fr_FR", l("{amount} LVL"),
			"zh_CN", l("{amount}级"),
			"zh_TW", l("{amount}级"),
			"ru_RU", l("{amount} LVL"),
			"hu_HU", l("{amount} szint"),
			"es_ES", l("{amount} Niveles")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERPOINTSPOINTSFORMAT = n("MISC_SUPREMESHOPS_PLAYERPOINTSPOINTSFORMAT",
			"en_US", l("{amount} PlayerPoints points"),
			"fr_FR", l("{amount} poinst PlayerPoints"),
			"zh_CN", l("{amount} 点券"),
			"zh_TW", l("{amount} PlayerPoints 點數"),
			"ru_RU", l("{amount} очков PlayerPoints"),
			"hu_HU", l("{amount} PlayerPoints pont"),
			"es_ES", l("{amount} PlayerPoints puntos")
			);

	public static final Text MISC_SUPREMESHOPS_TOKENENCHANTTOKENSFORMAT = n("MISC_SUPREMESHOPS_TOKENENCHANTTOKENSFORMAT",
			"en_US", l("{amount} TokenEnchant tokens"),
			"fr_FR", l("{amount} tokens TokenEnchant"),
			"zh_TW", l("{amount} TokenEnchant 代幣"),
			"ru_RU", l("{amount} TokenEnchant токенов"),
			"hu_HU", l("{amount} TokenEnchant token"),
			"es_ES", l("{amount} TokenEnchant tokens")
			);

	public static final Text MISC_SUPREMESHOPS_SNEAK = n("MISC_SUPREMESHOPS_SNEAK",
			"en_US", l("sneak"),
			"fr_FR", l("s'accroupir"),
			"zh_CN", l("潜行"),
			"zh_TW", l("&f半蹲"),
			"ru_RU", l("SHIFT"),
			"hu_HU", l("lopakodás"),
			"es_ES", l("Agacharse")
			);

	public static final Text MISC_SUPREMESHOPS_RIGHTCLICK = n("MISC_SUPREMESHOPS_RIGHTCLICK",
			"en_US", l("Right-Click"),
			"fr_FR", l("Clic-Droit"),
			"zh_CN", l("右击"),
			"zh_TW", l("&f右鍵點擊"),
			"ru_RU", l("ПКМ"),
			"hu_HU", l("Jobb-Klikk"),
			"es_ES", l("Clic derecho")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMGIVENAME = n("MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMGIVENAME",
			"en_US", l("§a+{amount}$"),
			"zh_CN", l("&a+{amount}$"),
			"zh_TW", l("&a+{amount}$"),
			"ru_RU", l("&a+{amount}$"),
			"hu_HU", l("&a+{amount}$"),
			"es_ES", l("&a+{amount}$")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMTAKENAME = n("MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMTAKENAME",
			"en_US", l("§c-{amount}$"),
			"zh_CN", l("&c-{amount}$"),
			"zh_TW", l("&c-{amount}$"),
			"ru_RU", l("&c-{amount}$"),
			"hu_HU", l("&c-{amount}$"),
			"es_ES", l("&c-{amount}$")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMGIVENAME = n("MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMGIVENAME",
			"en_US", l("§a+{amount} LVL"),
			"zh_CN", l("&a+{amount}级"),
			"zh_TW", l("&a+{amount}級"),
			"ru_RU", l("&a+{amount} LVL"),
			"hu_HU", l("&a+{amount} szint"),
			"es_ES", l("&a+{amount} nivel(es)")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMTAKENAME = n("MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMTAKENAME",
			"en_US", l("§c-{amount} LVL"),
			"zh_CN", l("&c-{amount}级"),
			"zh_TW", l("&c-{amount}級"),
			"ru_RU", l("&c-{amount} LVL"),
			"hu_HU", l("&c-{amount} szint"),
			"es_ES", l("&c-{amount} nivel(es)")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMGIVENAME = n("MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMGIVENAME",
			"en_US", l("§a+{amount}"),
			"zh_CN", l("&a+{amount}"),
			"zh_TW", l("&a+{amount}"),
			"ru_RU", l("&a+{amount}"),
			"hu_HU", l("&a+{amount}"),
			"es_ES", l("&a+{amount}")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMTAKENAME = n("MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMTAKENAME",
			"en_US", l("§c-{amount}"),
			"zh_CN", l("&c-{amount}"),
			"zh_TW", l("&c-{amount}"),
			"ru_RU", l("&c-{amount}"),
			"hu_HU", l("&c-{amount}"),
			"es_ES", l("&c-{amount}")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_ITEM = n("MISC_SUPREMESHOPS_OBJECTTYPE_ITEM",
			"en_US", l("item"),
			"fr_FR", l("item"),
			"zh_CN", l("物品"),
			"zh_TW", l("&f物品"),
			"ru_RU", l("предмет"),
			"hu_HU", l("tétel"),
			"es_ES", l("ítem")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_VAULTMONEY = n("MISC_SUPREMESHOPS_OBJECTTYPE_VAULTMONEY",
			"en_US", l("money"),
			"fr_FR", l("argent"),
			"zh_CN", l("金钱"),
			"zh_TW", l("&f金錢"),
			"ru_RU", l("валюта"),
			"hu_HU", l("pénz"),
			"es_ES", l("dinero")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_XPLEVEL = n("MISC_SUPREMESHOPS_OBJECTTYPE_XPLEVEL",
			"en_US", l("XP level"),
			"fr_FR", l("niveau d'XP"),
			"zh_CN", l("经验等级"),
			"zh_TW", l("&f經驗等級"),
			"ru_RU", l("уровень XP"),
			"hu_HU", l("XP szint"),
			"es_ES", l("nivel de XP")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_PLAYERPOINTSPOINTS = n("MISC_SUPREMESHOPS_OBJECTTYPE_PLAYERPOINTSPOINTS",
			"en_US", l("PlayerPoints points"),
			"fr_FR", l("points PlayerPoints"),
			"zh_CN", l("PlayerPoints点券数"),
			"zh_TW", l("&fPlayerPoints 點數"),
			"ru_RU", l("очки PlayerPoints"),
			"hu_HU", l("PlayerPoints pont"),
			"es_ES", l("PlayerPoints puntos")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_TOKENENCHANT_TOKENS = n("MISC_SUPREMESHOPS_OBJECTTYPE_TOKENENCHANT_TOKENS",
			"en_US", l("TokenEnchant tokens"),
			"fr_FR", l("tokens TokenEnchant"),
			"zh_TW", l("&fTokenEnchant 代幣"),
			"ru_RU", l("TokenEnchant токены"),
			"hu_HU", l("TokenEnchant token"),
			"es_ES", l("TokenEnchant tokens")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_COMMANDS = n("MISC_SUPREMESHOPS_OBJECTTYPE_COMMANDS",
			"en_US", l("commands"),
			"fr_FR", l("commandes"),
			"zh_CN", l("指令"),
			"zh_TW", l("&f指令"),
			"ru_RU", l("команды"),
			"hu_HU", l("parancsok"),
			"es_ES", l("comandos")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_MERCHANT = n("MISC_SUPREMESHOPS_OBJECTTYPE_MERCHANT",
			"en_US", l("merchant"),
			"fr_FR", l("marchand"),
			"zh_CN", l("商人"),
			"zh_TW", l("&f商人"),
			"ru_RU", l("торговец"),
			"hu_HU", l("kereskedő"),
			"es_ES", l("mercader")
			);

	public static final Text MISC_SUPREMESHOPS_OBJECTTYPE_SHOP = n("MISC_SUPREMESHOPS_OBJECTTYPE_SHOP",
			"en_US", l("shop"),
			"fr_FR", l("shop"),
			"zh_CN", l("商店"),
			"zh_TW", l("&f商店"),
			"ru_RU", l("магазин"),
			"hu_HU", l("bolt"),
			"es_ES", l("tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_BLOCK = n("MISC_SUPREMESHOPS_SHOPTYPE_BLOCK",
			"en_US", l("block"),
			"fr_FR", l("bloc"),
			"zh_CN", l("物理"),
			"zh_TW", l("&f方塊"),
			"ru_RU", l("блок"),
			"hu_HU", l("blokk"),
			"es_ES", l("bloque")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_SHOP = n("MISC_SUPREMESHOPS_SHOPTYPE_SHOP",
			"en_US", l("sign"),
			"fr_FR", l("panneau"),
			"zh_TW", l("&f告示牌"),
			"ru_RU", l("табличка"),
			"hu_HU", l("tábla"),
			"es_ES", l("señal")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_GUI = n("MISC_SUPREMESHOPS_SHOPTYPE_GUI",
			"en_US", l("GUI (menu)"),
			"fr_FR", l("GUI (menu)"),
			"zh_CN", l("GUI (菜单)"),
			"zh_TW", l("&fGUI (選單)"),
			"ru_RU", l("GUI (меню)"),
			"hu_HU", l("GUI (menü)"),
			"es_ES", l("GUI (menú)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_MERCHANT = n("MISC_SUPREMESHOPS_SHOPTYPE_MERCHANT",
			"en_US", l("merchant shop"),
			"fr_FR", l("shop de marchand"),
			"zh_TW", l("&f商人商店"),
			"ru_RU", l("магазин торговца"),
			"hu_HU", l("kereskedő bolt"),
			"es_ES", l("tienda de mercader")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_RENTBLOCK = n("MISC_SUPREMESHOPS_SHOPTYPE_RENTBLOCK",
			"en_US", l("rentable block"),
			"fr_FR", l("bloc à louer"),
			"zh_TW", l("&f可租借的方塊"),
			"ru_RU", l("арендуемый блок"),
			"hu_HU", l("kibérelhető blokk"),
			"es_ES", l("bloque alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_RENTABLESIGN = n("MISC_SUPREMESHOPS_SHOPTYPE_RENTABLESIGN",
			"en_US", l("rentable sign"),
			"fr_FR", l("panneau à louer"),
			"zh_TW", l("&f可租借的告示牌"),
			"ru_RU", l("арендуемая табличка"),
			"hu_HU", l("kibérelhető tábla"),
			"es_ES", l("señal alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPTYPE_ADMIN = n("MISC_SUPREMESHOPS_SHOPTYPE_ADMIN",
			"en_US", l("admin shop"),
			"fr_FR", l("shop admin"),
			"zh_CN", l("管理员商店"),
			"zh_TW", l("&f管理員商店"),
			"ru_RU", l("Админ-шоп"),
			"hu_HU", l("admin blot"),
			"es_ES", l("tienda de administrador")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_BLOCK = n("MISC_SUPREMESHOPS_MERCHANTTYPE_BLOCK",
			"en_US", l("block"),
			"fr_FR", l("bloc"),
			"zh_TW", l("&f方塊"),
			"ru_RU", l("блок"),
			"hu_HU", l("blokk"),
			"es_ES", l("bloque")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_SIGN = n("MISC_SUPREMESHOPS_MERCHANTTYPE_SIGN",
			"en_US", l("sign"),
			"fr_FR", l("panneau"),
			"zh_TW", l("&f告示牌"),
			"ru_RU", l("табличка"),
			"hu_HU", l("tábla"),
			"es_ES", l("señal")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_NPC = n("MISC_SUPREMESHOPS_MERCHANTTYPE_NPC",
			"en_US", l("NPC"),
			"fr_FR", l("NPC"),
			"zh_TW", l("&fNPC"),
			"ru_RU", l("NPC"),
			"hu_HU", l("NPC"),
			"es_ES", l("NPC")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLEBLOCK = n("MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLEBLOCK",
			"en_US", l("rentable block"),
			"fr_FR", l("bloc à louer"),
			"zh_TW", l("&f可租借的方塊"),
			"ru_RU", l("арендуемый блок"),
			"hu_HU", l("kibérelhető blokk"),
			"es_ES", l("bloque alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLESIGN = n("MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLESIGN",
			"en_US", l("rentable sign"),
			"fr_FR", l("panneau à louer"),
			"zh_TW", l("&f可租借的告示牌"),
			"ru_RU", l("арендуемая табличка"),
			"hu_HU", l("kibérelhető tábla"),
			"es_ES", l("señal alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLENPC = n("MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLENPC",
			"en_US", l("rentable NPC"),
			"fr_FR", l("NPC à louer"),
			"zh_TW", l("&f可租借的 NPC"),
			"ru_RU", l("арендуемый NPC"),
			"hu_HU", l("kibérelhető NPC"),
			"es_ES", l("NPC alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFWEEK = n("MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFWEEK",
			"en_US", l("day of week"),
			"fr_FR", l("jour de la semaine"),
			"zh_CN", l("一周的时间"),
			"zh_TW", l("&f一周的某一天"),
			"ru_RU", l("день недели"),
			"hu_HU", l("a hét napja"),
			"es_ES", l("día de la semana")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFMONTH = n("MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFMONTH",
			"en_US", l("day of month"),
			"fr_FR", l("jour du mois"),
			"zh_TW", l("&f一個月的某一個添"),
			"hu_HU", l("a hónap napja"),
			"es_ES", l("día del mes")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_GAMETIME = n("MISC_SUPREMESHOPS_CONDITIONTYPE_GAMETIME",
			"en_US", l("game time"),
			"fr_FR", l("temps en jeu"),
			"zh_CN", l("游戏时间"),
			"zh_TW", l("&f遊戲內時間"),
			"ru_RU", l("игровое время"),
			"hu_HU", l("játékidő"),
			"es_ES", l("hora del juego")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLD = n("MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLD",
			"en_US", l("in world"),
			"fr_FR", l("dans le monde"),
			"zh_CN", l("在世界内"),
			"zh_TW", l("&f在某個世界內"),
			"ru_RU", l("в мире"),
			"hu_HU", l("világban"),
			"es_ES", l("en el mundo")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLDGUARDREGION = n("MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLDGUARDREGION",
			"en_US", l("in WorldGuard region"),
			"fr_FR", l("dans une région WorldGuard"),
			"zh_CN", l("在WorldGuard区域内"),
			"zh_TW", l("&f在某個WorldGuard區域內"),
			"ru_RU", l("в регионе"),
			"hu_HU", l("WorldGuard régióban"),
			"es_ES", l("en la región de WorldGuard")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERMONEY = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERMONEY",
			"en_US", l("player money"),
			"fr_FR", l("argent du joueur"),
			"zh_CN", l("玩家钱数"),
			"zh_TW", l("&f玩家金錢餘額"),
			"ru_RU", l("баланс игрока"),
			"hu_HU", l("játékos pénz"),
			"es_ES", l("dinero del jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PERMISSION = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PERMISSION",
			"en_US", l("permission"),
			"fr_FR", l("permission"),
			"zh_CN", l("权限"),
			"zh_TW", l("&f權限"),
			"ru_RU", l("пермишен"),
			"hu_HU", l("jog"),
			"es_ES", l("permiso")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLACEHOLDERAPIVARIABLE = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLACEHOLDERAPIVARIABLE",
			"en_US", l("PlaceholderAPI variable value"),
			"fr_FR", l("valeur de variable PlaceholderAPI"),
			"zh_CN", l("PlaceholderAPI变量值"),
			"zh_TW", l("&fPlaceholderAPI 變量數值"),
			"ru_RU", l("тип переменной PlaceholderAPI"),
			"hu_HU", l("PlaceholderAPI változó érték"),
			"es_ES", l("valor de la variable de PlaceholderAPI")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERITEMS = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERITEMS",
			"en_US", l("player items"),
			"fr_FR", l("items du joueur"),
			"zh_CN", l("玩家物品"),
			"zh_TW", l("&f玩家物品"),
			"ru_RU", l("предметы игрока"),
			"hu_HU", l("játékos itemek"),
			"es_ES", l("ítems del jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERXPLEVEL = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERXPLEVEL",
			"en_US", l("player xp level"),
			"fr_FR", l("niveau d'xp du joueur"),
			"zh_CN", l("玩家经验等级"),
			"zh_TW", l("&f玩家經驗等級"),
			"ru_RU", l("уровень игрока"),
			"hu_HU", l("játékos XP szint"),
			"es_ES", l("nivel de XP del jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHEALTH = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHEALTH",
			"en_US", l("player health"),
			"fr_FR", l("vie du joueur"),
			"zh_CN", l("玩家血量"),
			"zh_TW", l("&f玩家血量"),
			"ru_RU", l("здоровье игрока"),
			"hu_HU", l("játékos élet"),
			"es_ES", l("vida del jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHUNGER = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHUNGER",
			"en_US", l("player hunger"),
			"fr_FR", l("niveau de faim du joueur"),
			"zh_CN", l("玩家饥饿值"),
			"zh_TW", l("&f玩家飢餓度"),
			"ru_RU", l("голод игрока"),
			"hu_HU", l("játékos éhség"),
			"es_ES", l("comida del jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERPOINTSPOINTS = n("MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERPOINTSPOINTS",
			"en_US", l("PlayerPoints points"),
			"fr_FR", l("points PlayerPoints"),
			"zh_CN", l("PlayerPoints点券数"),
			"zh_TW", l("&fPlayerPoints 點數數量"),
			"ru_RU", l("очки PlayerPoints"),
			"hu_HU", l("PlayerPoints pont"),
			"es_ES", l("PlayerPoints puntos")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_TOKENENCHANTTOKENS = n("MISC_SUPREMESHOPS_CONDITIONTYPE_TOKENENCHANTTOKENS",
			"en_US", l("TokenEnchant tokens"),
			"fr_FR", l("tokens TokenEnchant"),
			"zh_TW", l("&fTokenEnchant 代幣數量"),
			"ru_RU", l("TokenEnchant токены"),
			"hu_HU", l("TokenEnchant token"),
			"es_ES", l("TokenEnchant tokens")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPBLOCKTYPE = n("MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPBLOCKTYPE",
			"en_US", l("shop block type"),
			"fr_FR", l("type de bloc du shop"),
			"zh_CN", l("商店方块类型"),
			"zh_TW", l("&f商店方塊類型"),
			"ru_RU", l("тип блока магазина"),
			"hu_HU", l("bolt blokk típus"),
			"es_ES", l("tipo de tienda (de bloque)")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_ISADMIN = n("MISC_SUPREMESHOPS_CONDITIONTYPE_ISADMIN",
			"en_US", l("shop/merchant is admin"),
			"fr_FR", l("le shop/marchand est admin"),
			"zh_CN", l("商店商人为管理员"),
			"zh_TW", l("&f商店/商人 為管理員"),
			"ru_RU", l("магазин\\торговец администратора"),
			"hu_HU", l("bolt/kereskedő admin"),
			"es_ES", l("la tienda/el comerciante es admin")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPOBJECTCOUNT = n("MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPOBJECTCOUNT",
			"en_US", l("shop object count"),
			"fr_FR", l("nombre d'objet dans le shop"),
			"zh_CN", l("商店物品数"),
			"zh_TW", l("&f商店物件數量"),
			"ru_RU", l("количество предметов магазина"),
			"hu_HU", l("bolt tárgy szám"),
			"es_ES", l("contador de objetos de tienda")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPCOUNT = n("MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPCOUNT",
			"en_US", l("shop count"),
			"fr_FR", l("nombre de shops"),
			"zh_CN", l("商店数"),
			"zh_TW", l("&f商店數量"),
			"ru_RU", l("количество магазинов"),
			"hu_HU", l("bolt szám"),
			"es_ES", l("contador de tienda")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_CONDITIONCOUNT = n("MISC_SUPREMESHOPS_CONDITIONTYPE_CONDITIONCOUNT",
			"en_US", l("shop/trigger condition count"),
			"fr_FR", l("nombre de conditions dans le shop/trigger"),
			"zh_CN", l("商店/触发器 的条件数"),
			"zh_TW", l("&f商店/觸發 的條件數量"),
			"ru_RU", l("количество триггеров\\условий магазина"),
			"hu_HU", l("bolt/trigger feltátel szám"),
			"es_ES", l("número de condiciones dentro de la tienda/trigger")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPTYPE = n("MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPTYPE",
			"en_US", l("shop type"),
			"fr_FR", l("type du shop"),
			"zh_CN", l("商店类型"),
			"zh_TW", l("&f商店類型"),
			"ru_RU", l("тип магазина"),
			"hu_HU", l("bolt típus"),
			"es_ES", l("tipo de tienda")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONTYPE_TIMEOFDAY = n("MISC_SUPREMESHOPS_CONDITIONTYPE_TIMEOFDAY",
			"en_US", l("time of day"),
			"fr_FR", l("temps de la journée"),
			"zh_CN", l("一天中的时间"),
			"zh_TW", l("&f一天鐘的某一個時間"),
			"ru_RU", l("время дня"),
			"hu_HU", l("nap ideje"),
			"es_ES", l("hora del día")
			);

	public static final Text MISC_SUPREMESHOPS_BLOCKMERCHANTSIGNLINES = n("MISC_SUPREMESHOPS_BLOCKMERCHANTSIGNLINES",
			"en_US", l("&6&lMERCHANT", "&a{name}", "&d{owner}", "{merchant_open}"),
			"fr_FR", l("&6&lMARCHAND", "&a{name}", "&d{owner}", "{merchant_open}"),
			"zh_TW", l("&6&l商人", "&a{name}", "&d{owner}", "{merchant_open}"),
			"ru_RU", l("&6&lТорговец", "&a{name}", "&d{owner}", "{merchant_open}"),
			"hu_HU", l("&6&lKERESKEDŐ", "&a{name}", "&d{owner}", "{merchant_open}"),
			"es_ES", l("&6&lMERCADER", "&a{name}", "&d{owner}", "{merchant_open}")
			);

	public static final Text MISC_SUPREMESHOPS_SIGNMERCHANTSIGNLINES = n("MISC_SUPREMESHOPS_SIGNMERCHANTSIGNLINES",
			"en_US", l("&6&lSIGN MERCHANT", "&a{name}", "&d{owner}", "{merchant_open}"),
			"fr_FR", l("&6&lMARCHAND PANNEAU", "&a{name}", "&d{owner}", "{merchant_open}"),
			"zh_TW", l("&6&l告示牌商人", "&a{name}", "&d{owner}", "{merchant_open}"),
			"ru_RU", l("&6&lТорговец-табличка", "&a{name}", "&d{owner}", "{merchant_open}"),
			"hu_HU", l("&6&lTÁBLA KERESKEDŐ", "&a{name}", "&d{owner}", "{merchant_open}"),
			"es_ES", l("&6&lCARTEL DE MERCADER", "&a{name}", "&d{owner}", "{merchant_open}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTBLOCKMERCHANTSIGNLINES = n("MISC_SUPREMESHOPS_RENTBLOCKMERCHANTSIGNLINES",
			"en_US", l("&6&lRENT MERCHANT", "&a{name}", "&d{current_owner}", "{merchant_open}"),
			"fr_FR", l("&6&lMARCHAND LOUABLE", "&a{name}", "&d{current_owner}", "{merchant_open}"),
			"zh_TW", l("&6&l出租商人", "&a{name}", "&d{owner}", "{merchant_open}"),
			"ru_RU", l("&6&lАрендовать торговца", "&a{name}", "&d{current_owner}", "{merchant_open}"),
			"hu_HU", l("&6&lBÉR KERESKEDŐ", "&a{name}", "&d{rent}", "{merchant_open}"),
			"es_ES", l("&6&lALQUILER DE MERCADER", "&a{name}", "&d{current_owner}", "{merchant_open}")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDBLOCKMERCHANTSIGNLINES = n("MISC_SUPREMESHOPS_UNRENTEDBLOCKMERCHANTSIGNLINES",
			"en_US", l("&6&lRENT MERCHANT", "&aTo rent"),
			"fr_FR", l("&6&lMARCHAND LOUABLE", "&aA louer"),
			"zh_TW", l("&6&l出租商人", "&a出租"),
			"ru_RU", l("&6&lАрендовать торговца", "&aАрендовать"),
			"hu_HU", l("&6&lBÉR KERESKEDŐ", "&ahogy kibéreld"),
			"es_ES", l("&6&lMERCADER DE ALQUILER", "&aPara alquilar")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDSIGNMERCHANTSIGNLINES = n("MISC_SUPREMESHOPS_UNRENTEDSIGNMERCHANTSIGNLINES",
			"en_US", l("&6&lRENT MERCHANT", "&aTo rent"),
			"fr_FR", l("&6&lMARCHAND LOUABLE", "&aA louer"),
			"zh_TW", l("&6&l出租商人", "&a出租"),
			"ru_RU", l("&6&lАрендовать торговца", "&aАрендовать"),
			"hu_HU", l("&6&lBÉR KERESKEDŐ", "&ahogy kibéreld"),
			"es_ES", l("&6&lMERCADER DE ALQUILER", "&aPara alquilar")
			);

	public static final Text MISC_SUPREMESHOPS_BLOCKSHOPSIGNLINES = n("MISC_SUPREMESHOPS_BLOCKSHOPSIGNLINES",
			"en_US", l("&6&lSHOP", "&a{name}", "&d{owner}", "{shop_open}"),
			"fr_FR", l("&6&lSHOP", "&a{name}", "&d{owner}", "{shop_open}"),
			"zh_TW", l("&6&l商店", "&a{name}", "&d{owner}", "{shop_open}"),
			"ru_RU", l("&6&lМагазин", "&a{name}", "&d{owner}", "{shop_open}"),
			"hu_HU", l("&6&lBOLT", "&a{name}", "&d{owner}", "{shop_open}"),
			"es_ES", l("&6&lTIENDA", "&a{name}", "&d{owner}", "{shop_open}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTBLOCKSHOPSIGNLINES = n("MISC_SUPREMESHOPS_RENTBLOCKSHOPSIGNLINES",
			"en_US", l("&6&lRENT SHOP", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"fr_FR", l("&6&lSHOP LOUABLE", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"zh_TW", l("&6&l出租商店", "&a{name}", "&d{owner}", "{shop_open}"),
			"ru_RU", l("&6&lАрендный магазин", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"hu_HU", l("&6&lBÉR BOLT", "&a{name}", "&d{rent}", "{shop_open}"),
			"es_ES", l("&6&lALQUILER", "&a{name}", "&d{owner}", "{shop_open}")
			);

	public static final Text MISC_SUPREMESHOPS_SIGNSHOPSIGNLINES = n("MISC_SUPREMESHOPS_SIGNSHOPSIGNLINES",
			"en_US", l("&6&lSIGN SHOP", "&a{name}", "&d{owner}", "{shop_open}"),
			"fr_FR", l("&6&lSHOP PANNEAU", "&a{name}", "&d{owner}", "{shop_open}"),
			"zh_TW", l("&6&l告示牌商店", "&a{name}", "&d{owner}", "{shop_open}"),
			"ru_RU", l("&6&lМагазин-табличка", "&a{name}", "&d{owner}", "{shop_open}"),
			"hu_HU", l("&6&lTÁBLA BOLT", "&a{name}", "&d{owner}", "{shop_open}"),
			"es_ES", l("&6&lCARTEL DE TIENDA", "&a{name}", "&d{owner}", "{shop_open}")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDBLOCKSHOPSIGNLINES = n("MISC_SUPREMESHOPS_UNRENTEDBLOCKSHOPSIGNLINES",
			"en_US", l("&6&lRENT SHOP", "&aTo rent"),
			"fr_FR", l("&6&lSHOP LOUABLE", "&aA louer"),
			"zh_TW", l("&6&l出租商店", "&a出租"),
			"ru_RU", l("&6&lАрендовать магазин", "&aАрендовать"),
			"hu_HU", l("&6&lBÉR BOLT", "&ahogy kibéreld"),
			"es_ES", l("&6&lALQUILER", "&aPara alquilar")
			);

	public static final Text MISC_SUPREMESHOPS_RENTSIGNSHOPSIGNLINES = n("MISC_SUPREMESHOPS_RENTSIGNSHOPSIGNLINES",
			"en_US", l("&6&lRENT SHOP", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"fr_FR", l("&6&lSHOP LOUABLE", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"zh_TW", l("&6&l出租商店", "&a{name}", "&d{owner}", "{shop_open}"),
			"ru_RU", l("&6&lАрендный магазин", "&a{name}", "&d{current_owner}", "{shop_open}"),
			"es_ES", l("&6&lTIENDA DE ALQUILER", "&a{name}", "&d{current_owner}", "{shop_open}")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDSIGNSHOPSIGNLINES = n("MISC_SUPREMESHOPS_UNRENTEDSIGNSHOPSIGNLINES",
			"en_US", l("&6&lRENT SHOP", "&aTo rent"),
			"fr_FR", l("&6&lSHOP LOUABLE", "&aA louer"),
			"zh_TW", l("&6&l出租商店", "&a出租"),
			"ru_RU", l("&6&lАрендный магазин", "&aАрендовать"),
			"hu_HU", l("&6&lBÉR BOLT", "&ahogy kibéreld")
			);

	public static final Text MISC_SUPREMESHOPS_TAKENOBJECTDESCRIPTIONLINE = n("MISC_SUPREMESHOPS_TAKENOBJECTDESCRIPTIONLINE",
			"en_US", l("&c- {description}"),
			"fr_FR", l("&c- {description}"),
			"zh_TW", l("&c- {description}"),
			"ru_RU", l("&c- {description}"),
			"hu_HU", l("&c- {description}"),
			"es_ES", l("&c- {description}")
			);

	public static final Text MISC_SUPREMESHOPS_GIVENOBJECTDESCRIPTIONLINE = n("MISC_SUPREMESHOPS_GIVENOBJECTDESCRIPTIONLINE",
			"en_US", l("&a+ {description}"),
			"fr_FR", l("&a+ {description}"),
			"zh_TW", l("&a+ {description}"),
			"ru_RU", l("&a+ {description}"),
			"hu_HU", l("&a+ {description}"),
			"es_ES", l("&a+ {description}")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPOPEN = n("MISC_SUPREMESHOPS_SHOPOPEN",
			"en_US", l("&a[OPEN]"),
			"fr_FR", l("&a[OUVERT]"),
			"zh_CN", l("&a[打开]"),
			"zh_TW", l("&a[營業中]"),
			"ru_RU", l("&a[ОТКРЫТО]"),
			"hu_HU", l("&a[NYITVA]"),
			"es_ES", l("&a[ABIERTO]")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPCLOSED = n("MISC_SUPREMESHOPS_SHOPCLOSED",
			"en_US", l("&c[CLOSED]"),
			"fr_FR", l("&c[FERMÉ]"),
			"zh_CN", l("&c[关闭]"),
			"zh_TW", l("&c[打烊]"),
			"ru_RU", l("&c[ЗАКРЫТО]"),
			"hu_HU", l("&c[ZÁRVA]"),
			"es_ES", l("&c[CERRADO]")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTOPEN = n("MISC_SUPREMESHOPS_MERCHANTOPEN",
			"en_US", l("&a[OPEN]"),
			"fr_FR", l("&a[OUVERT]"),
			"zh_CN", l("&a[打开"),
			"zh_TW", l("&a[營業中]"),
			"ru_RU", l("&a[ОТКРЫТО]"),
			"hu_HU", l("&a[NYITVA]"),
			"es_ES", l("&a[ABIERTO]")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTCLOSED = n("MISC_SUPREMESHOPS_MERCHANTCLOSED",
			"en_US", l("&c[CLOSED]"),
			"fr_FR", l("&c[FERMÉ]"),
			"zh_CN", l("&c[关闭]"),
			"zh_TW", l("&c[打烊]"),
			"ru_RU", l("&c[ЗАКРЫТО]"),
			"hu_HU", l("&c[ZÁRVA]"),
			"es_ES", l("&c[CERRADO]")
			);

	public static final Text MISC_SUPREMESHOPS_LEFTCLICK = n("MISC_SUPREMESHOPS_LEFTCLICK",
			"en_US", l("Left-Click"),
			"fr_FR", l("Clic-Gauche"),
			"zh_CN", l("左击"),
			"zh_TW", l("&f左鍵點擊"),
			"ru_RU", l("ЛКМ"),
			"hu_HU", l("Bal-klikk"),
			"es_ES", l("Clic izquierdo")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTOWNERGUINAME = n("MISC_SUPREMESHOPS_MERCHANTLISTOWNERGUINAME",
			"en_US", l("{owner}'s merchants"),
			"fr_FR", l("Marchands de {owner}"),
			"zh_CN", l("{owner}的商人"),
			"zh_TW", l("&a{owner} &f的商人"),
			"ru_RU", l("Торговцы игрока {owner}"),
			"hu_HU", l("{owner} kereskedője"),
			"es_ES", l("Mercader de {owner}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTGUINAME = n("MISC_SUPREMESHOPS_MERCHANTLISTGUINAME",
			"en_US", l("Merchant list"),
			"fr_FR", l("Liste de marchands"),
			"zh_CN", l("商人列表"),
			"zh_TW", l("&f商人列表"),
			"ru_RU", l("Список торговцев"),
			"hu_HU", l("Kereskedő Lista"),
			"es_ES", l("Lista de mercaderes")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTMANAGEMENTGUINAME = n("MISC_SUPREMESHOPS_MERCHANTMANAGEMENTGUINAME",
			"en_US", l("Management - {name}"),
			"fr_FR", l("Gestion - {name}"),
			"zh_CN", l("管理 - {name}"),
			"zh_TW", l("&f管理者 - &a{name}"),
			"ru_RU", l("Менеджер - {name}"),
			"hu_HU", l("Kezelő - {name}"),
			"es_ES", l("Gestión - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTGUINAME = n("MISC_SUPREMESHOPS_SHOPLISTGUINAME",
			"en_US", l("Shop list"),
			"fr_FR", l("Liste de shops"),
			"zh_CN", l("商店列表"),
			"zh_TW", l("&f商店列表"),
			"ru_RU", l("Список магазинов"),
			"hu_HU", l("Bolt lista"),
			"es_ES", l("Lista de tiendas")
			);

	public static final Text MISC_SUPREMESHOPS_YOURSHOPLISTGUINAME = n("MISC_SUPREMESHOPS_YOURSHOPLISTGUINAME",
			"en_US", l("Your shops"),
			"fr_FR", l("Vos shops"),
			"zh_CN", l("你的商店"),
			"zh_TW", l("&f你的商店"),
			"ru_RU", l("Ваши магазины"),
			"hu_HU", l("Boltjaid"),
			"es_ES", l("Tus tiendas")
			);

	public static final Text MISC_SUPREMESHOPS_YOURMERCHANTSLISTGUINAME = n("MISC_SUPREMESHOPS_YOURMERCHANTSLISTGUINAME",
			"en_US", l("Your merchants"),
			"fr_FR", l("Vos marchands"),
			"zh_CN", l("你的商人"),
			"zh_TW", l("&f你的商人"),
			"ru_RU", l("Ваши торговцы"),
			"hu_HU", l("Kereskedőid"),
			"es_ES", l("Tus mercaderes")
			);

	public static final Text MISC_SUPREMESHOPS_SELECTSHOPLISTGUINAME = n("MISC_SUPREMESHOPS_SELECTSHOPLISTGUINAME",
			"en_US", l("Select - Shop list"),
			"fr_FR", l("Sélectionner - Liste de shops"),
			"zh_CN", l("选择 - 商店列表"),
			"zh_TW", l("&f選擇 - 商店列表"),
			"ru_RU", l("Выбрать - Список магазинов"),
			"hu_HU", l("Kiválaszt - Bolt lista"),
			"es_ES", l("Seleccionar - Lista de tiendas")
			);

	public static final Text MISC_SUPREMESHOPS_SELECTMERCHANTLISTGUINAME = n("MISC_SUPREMESHOPS_SELECTMERCHANTLISTGUINAME",
			"en_US", l("Select - Merchant list"),
			"fr_FR", l("Sélectionner - Liste de marchands"),
			"zh_CN", l("选择 - 商人列表"),
			"zh_TW", l("&f選擇 - 商人列表"),
			"ru_RU", l("Выбрать - Список торговцев"),
			"hu_HU", l("Kiválaszt - Kereskedő lista"),
			"es_ES", l("Seleccionar - Lista de mercaderes")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTOWNERGUINAME = n("MISC_SUPREMESHOPS_SHOPLISTOWNERGUINAME",
			"en_US", l("{owner}'s shops"),
			"fr_FR", l("Shops de {owner}"),
			"zh_CN", l("{owner}'s shops"),
			"zh_TW", l("&a{owner} &f的商店"),
			"ru_RU", l("Магазины игрока {owner}"),
			"hu_HU", l("{owner} boltjai"),
			"es_ES", l("Tiendas de {owner}")
			);

	public static final Text MISC_SUPREMESHOPS_SELECTSHOPLISTOWNERGUINAME = n("MISC_SUPREMESHOPS_SELECTSHOPLISTOWNERGUINAME",
			"en_US", l("Select - {owner}'s shops"),
			"fr_FR", l("Sélectionner - Shops de {owner}"),
			"zh_CN", l("选择 - {owner}的商店"),
			"zh_TW", l("&f選擇 - &a{owner}&f的商店"),
			"ru_RU", l("Выбрать - магазины игрока {owner}"),
			"hu_HU", l("Kiválaszt - {owner} boltjai"),
			"es_ES", l("Seleccionar - Tiendas de {owner}")
			);

	public static final Text MISC_SUPREMESHOPS_SELECTMERCHANTLISTOWNERGUINAME = n("MISC_SUPREMESHOPS_SELECTMERCHANTLISTOWNERGUINAME",
			"en_US", l("Select - {owner}'s merchants"),
			"fr_FR", l("Sélectionner - Merchands de {owner}"),
			"zh_CN", l("选择 - {owner}的商人"),
			"zh_TW", l("&f選擇 - &a{owner}&f的商人"),
			"ru_RU", l("Выбрать - торговцы игрока {owner}"),
			"hu_HU", l("Kiválaszt - {owner} kereskedői"),
			"es_ES", l("Seleccionar - Mercaderes de {owner}")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPMANAGEMENTGUINAME = n("MISC_SUPREMESHOPS_SHOPMANAGEMENTGUINAME",
			"en_US", l("Management - {name}"),
			"fr_FR", l("Gestion - {name}"),
			"zh_CN", l("管理 - {name}"),
			"zh_TW", l("&f管理者 - {name}"),
			"ru_RU", l("Менеджер - {name}"),
			"hu_HU", l("Kezelő - {name}"),
			"es_ES", l("Gestión - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTSHOPMANAGEMENTGUINAME = n("MISC_SUPREMESHOPS_RENTSHOPMANAGEMENTGUINAME",
			"en_US", l("Rent shop management - {name}"),
			"fr_FR", l("Gestion de shop louable - {name}"),
			"zh_TW", l("&f出租商店 管理者 - &a{name}"),
			"ru_RU", l("Арендовать управление магазином - {name}"),
			"hu_HU", l("Bérelhető bolt kezelő - {name}"),
			"es_ES", l("Gestión de tienda de alquiler - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTMERCHANTMANAGEMENTGUINAME = n("MISC_SUPREMESHOPS_RENTMERCHANTMANAGEMENTGUINAME",
			"en_US", l("Rent merchant management - {name}"),
			"fr_FR", l("Gestion de marchand louable - {name}"),
			"zh_TW", l("&f出租商人 管理者 - &a{name}"),
			"ru_RU", l("Арендовать управление торговцем - {name}"),
			"hu_HU", l("Bérleti kereskedői kezelés - {name}"),
			"es_ES", l("Gestión de alquiler de mercaderes - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITTAKEGUINAME = n("MISC_SUPREMESHOPS_EDITTAKEGUINAME",
			"en_US", l("Taken objects - {name}"),
			"fr_FR", l("Objets pris - {name}"),
			"zh_CN", l("拿走物品 - {name}"),
			"zh_TW", l("&f拿取的物件 - &a{name}"),
			"ru_RU", l("Взятые предметы - {name}"),
			"hu_HU", l("Elvett tárgyak - {name}"),
			"es_ES", l("Objetos quitados - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSGUINAME = n("MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSGUINAME",
			"en_US", l("Trade conditions - {name}"),
			"fr_FR", l("Conditions d'échange - {name}"),
			"zh_CN", l("交易条件 - {name}"),
			"zh_TW", l("&f交易條件- &a{name}"),
			"ru_RU", l("Условия обмена - {name}"),
			"hu_HU", l("Kereskedési feltételek - {name}"),
			"es_ES", l("Condiciones de comercio - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITTRADECONDITIONSGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITTRADECONDITIONSGUINAME",
			"en_US", l("Trade conditions - {name}"),
			"fr_FR", l("Conditions d'échange - {name}"),
			"zh_CN", l("交易条件 - {name}"),
			"zh_TW", l("&f交易條件- &a{name}"),
			"ru_RU", l("Условия обмена - {name}"),
			"hu_HU", l("Kereskedési feltételek - {name}"),
			"es_ES", l("Condiciones de comercio - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTCONDITIONSGUINAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTCONDITIONSGUINAME",
			"en_US", l("Rent conditions - {name}"),
			"fr_FR", l("Conditions de location - {name}"),
			"zh_TW", l("&f出租條件- &a{name}"),
			"ru_RU", l("Условия аренды - {name}"),
			"hu_HU", l("Bérlési feltételek - {name}"),
			"es_ES", l("Condiciones de alquiler - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITGIVEGUINAME = n("MISC_SUPREMESHOPS_EDITGIVEGUINAME",
			"en_US", l("Given objects - {name}"),
			"fr_FR", l("Objets donnés - {name}"),
			"zh_CN", l("给予物品 - {name}"),
			"zh_TW", l("&f給予的物件 - &a{name}"),
			"ru_RU", l("Отданные предметы - {name}"),
			"hu_HU", l("Adott tárgyak - {name}"),
			"es_ES", l("Objetos dados - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERSGUINAME = n("MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERSGUINAME",
			"en_US", l("Shop managers - {name}"),
			"fr_FR", l("Managers de shop - {name}"),
			"zh_TW", l("&f商店管理者 - &a{name}"),
			"ru_RU", l("Менеджеры магазина - {name}"),
			"hu_HU", l("Üzlet kezelők - {name}"),
			"es_ES", l("Managers de la tienda - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERPERMISSIONSGUINAME = n("MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERPERMISSIONSGUINAME",
			"en_US", l("Permissions of {player} - {name}"),
			"fr_FR", l("Permissions de {player} - {name}"),
			"zh_TW", l("&a{player} &f的權限 - &a{name}"),
			"ru_RU", l("Права игрока {player} - {name}"),
			"hu_HU", l("{player} joga - {name}"),
			"es_ES", l("Permisos de {player} - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSTATUSGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSTATUSGUINAME",
			"en_US", l("Status - {name}"),
			"fr_FR", l("Status - {name}"),
			"zh_TW", l("&f狀態 - &a{name}"),
			"ru_RU", l("Статус - {name}"),
			"hu_HU", l("Státusz - {name}"),
			"es_ES", l("Estado - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEGUINAME = n("MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEGUINAME",
			"en_US", l("Wage of {player} - {name}"),
			"fr_FR", l("Salaire de {player} - {name}"),
			"zh_TW", l("&a{player} &f的薪資 - &a{name}"),
			"ru_RU", l("ЗП игрока {player} - {name}"),
			"hu_HU", l("{player} bére - {name}"),
			"es_ES", l("Salario de {player} - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLESEERENTPRICEGUINAME = n("MISC_SUPREMESHOPS_RENTABLESEERENTPRICEGUINAME",
			"en_US", l("Rent price - {name}"),
			"fr_FR", l("Prix de location - {name}"),
			"zh_TW", l("&f出租價格 - &a{name}"),
			"ru_RU", l("Цена аренды - {name}"),
			"hu_HU", l("Bérlési ár - {name}"),
			"es_ES", l("Precio de alquiler - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEUNRENTEDGUINAME = n("MISC_SUPREMESHOPS_RENTABLEUNRENTEDGUINAME",
			"en_US", l("Rent price"),
			"fr_FR", l("Prix de location"),
			"zh_TW", l("&f出租價格"),
			"ru_RU", l("Цена аренды"),
			"hu_HU", l("Bérlési ár"),
			"es_ES", l("Precio de alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITRENTPRICEGUINAME = n("MISC_SUPREMESHOPS_SHOPEDITRENTPRICEGUINAME",
			"en_US", l("Rent price - {name}"),
			"fr_FR", l("Prix de location - {name}"),
			"zh_TW", l("&f出租價格 - &a{name}"),
			"ru_RU", l("Цена аренды - {name}"),
			"hu_HU", l("Bérlési ár - {name}"),
			"es_ES", l("Precio de alquiler - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITRENTPRICEGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITRENTPRICEGUINAME",
			"en_US", l("Rent price - {name}"),
			"fr_FR", l("Prix de location - {name}"),
			"zh_TW", l("&f出租價格 - &a{name}"),
			"ru_RU", l("Цена аренды - {name}"),
			"hu_HU", l("Bérlési ár - {name}"),
			"es_ES", l("Precio de alquiler - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSGUINAME",
			"en_US", l("Merchant managers - {name}"),
			"fr_FR", l("Managers de marchand - {name}"),
			"zh_CN", l("商人老板 - {name}"),
			"zh_TW", l("&f商店管理者  - &a{name}"),
			"ru_RU", l("Менеджеры торговца - {name}"),
			"hu_HU", l("Kereskedő kezelők - {name}"),
			"es_ES", l("Managers de mercaderes - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNGUINAME = n("MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNGUINAME",
			"en_US", l("Particle pattern - {name}"),
			"fr_FR", l("Pattern de particules - {name}"),
			"zh_CN", l("Particle pattern - {name}"),
			"zh_TW", l("&f粒子圖案 - &a{name}"),
			"ru_RU", l("Паттерн частиц - {name}"),
			"hu_HU", l("Részecske minta - {name}"),
			"es_ES", l("Patrón de partículas - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNGUINAME = n("MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNGUINAME",
			"en_US", l("Particle pattern (unrented) - {name}"),
			"fr_FR", l("Pattern de particules (non loué) - {name}"),
			"zh_TW", l("&f粒子圖案&c(待出租) - &a{name}"),
			"ru_RU", l("Паттерн частиц (не арендовано) - {name}"),
			"hu_HU", l("Részecske minta (nem kibérelt) - {name}"),
			"es_ES", l("Patrón de partículas (sin alquilar) - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITPARTICLEPATTERNGUINAME = n("MISC_SUPREMESHOPS_EDITPARTICLEPATTERNGUINAME",
			"en_US", l("Particle pattern - {name}"),
			"fr_FR", l("Pattern de particules - {name}"),
			"zh_TW", l("&f粒子圖案 - &a{name}"),
			"ru_RU", l("Паттерн частиц - {name}"),
			"hu_HU", l("Részecske minta - {name}"),
			"es_ES", l("Patrón de partículas - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTGUINAME",
			"en_US", l("Equipment - {name}"),
			"fr_FR", l("Équipement - {name}"),
			"zh_CN", l("Equipment - {name}"),
			"zh_TW", l("&f裝備 - &a{name}"),
			"ru_RU", l("Снаряжение - {name}"),
			"hu_HU", l("Felszerelés - {name}"),
			"es_ES", l("Equipamiento - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSKINGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSKINGUINAME",
			"en_US", l("Skin - {name}"),
			"fr_FR", l("Skin - {name}"),
			"zh_CN", l("Skin - {name}"),
			"zh_TW", l("&f皮膚 - &a{name}"),
			"ru_RU", l("Скин - {name}"),
			"hu_HU", l("Kinézet - {name}"),
			"es_ES", l("Skin - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITMERCHANTSHOPSGUINAME = n("MISC_SUPREMESHOPS_MERCHANTEDITMERCHANTSHOPSGUINAME",
			"en_US", l("Merchant shops - {name}"),
			"fr_FR", l("Shops marchand - {name}"),
			"zh_TW", l("&f商人商店 - &a{name}"),
			"ru_RU", l("Магазины торговца - {name}"),
			"hu_HU", l("Kereskedői üzletek - {name}"),
			"es_ES", l("Tiendas de mercaderes - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_ITEMBLACKLISTGUINAME = n("MISC_SUPREMESHOPS_ITEMBLACKLISTGUINAME",
			"en_US", l("Item blacklist"),
			"fr_FR", l("Blacklist d'items"),
			"zh_CN", l("Item blacklist"),
			"zh_TW", l("&f物品黑名單"),
			"ru_RU", l("Запрещённые предметы"),
			"hu_HU", l("Item tiltólista"),
			"es_ES", l("Ítems en la lista negra")
			);

	public static final Text MISC_SUPREMESHOPS_ITEMWHITELISTGUINAME = n("MISC_SUPREMESHOPS_ITEMWHITELISTGUINAME",
			"en_US", l("Item whitelist"),
			"fr_FR", l("Whitelist d'items"),
			"zh_CN", l("Item whitelist"),
			"zh_TW", l("&f物品白名單"),
			"ru_RU", l("Разрешённые предметы"),
			"hu_HU", l("Item fehérlista"),
			"es_ES", l("Ítems en la lista blanca")
			);

	public static final Text MISC_SUPREMESHOPS_PREVIEWGUINAME = n("MISC_SUPREMESHOPS_PREVIEWGUINAME",
			"en_US", l("Preview - {name}"),
			"fr_FR", l("Prévisualisation - {name}"),
			"zh_CN", l("Preview - {name}"),
			"zh_TW", l("&f預覽 - &a{name}"),
			"ru_RU", l("Превью - {name}"),
			"hu_HU", l("Előnézet - {name}"),
			"es_ES", l("Previsualizar - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_TRADEAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_TRADEAMOUNTSELECTGUINAME",
			"en_US", l("Trade - {name}"),
			"fr_FR", l("Échange - {name}"),
			"zh_CN", l("Trade - {name}"),
			"zh_TW", l("&f交易 - &a{name}"),
			"ru_RU", l("Обмен - {name}"),
			"hu_HU", l("Kereskedés - {name}"),
			"es_ES", l("Comerciar - {name}")
			);

	public static final Text MISC_SUPREMESHOPS_ADMINSHOPTRADEDEFAULTITEMNAME = n("MISC_SUPREMESHOPS_ADMINSHOPTRADEDEFAULTITEMNAME",
			"en_US", l("&6{name}"),
			"fr_FR", l("&6{name}"),
			"zh_CN", l("&6{name}"),
			"zh_TW", l("&6{name}"),
			"ru_RU", l("&6{name}"),
			"hu_HU", l("&6{name}"),
			"es_ES", l("&6{name}")
			);

	public static final Text MISC_SUPREMESHOPS_ADMINSHOPTRADEDEFAULTITEMLORE = n("MISC_SUPREMESHOPS_ADMINSHOPTRADEDEFAULTITEMLORE",
			"en_US", l("", "&7What you give :", "&6{taken_objects}", "", "&7What you receive :", "&6{given_objects}", "", "&dLeft-click to preview and trade"),
			"fr_FR", l("", "&7Ce que vous donnez :", "&6{taken_objects}", "", "&7Ce que vous recevez :", "&6{given_objects}", "", "&dClic-gauche pour prévisualiser et échanger"),
			"zh_CN", l("", "&7What you give :", "&6{taken_objects}", "", "&7What you receive :", "&6{given_objects}", "", "&dLeft-click to preview and trade"),
			"zh_TW", l("", "&7你要給的東西 :", "&6{taken_objects}", "", "&7你會獲得的東西 :", "&6{given_objects}", "", "&f左鍵點擊 &a預覽&f和&a交易"),
			"ru_RU", l("", "&7Вы отдадите :", "&6{taken_objects}", "", "&7Вы получите :", "&6{given_objects}", "", "&dЛКМ для предпросмотра и обмена"),
			"hu_HU", l("", "&7Amit adni fogsz :", "&6{taken_objects}", "", "&7Amit kapni fogsz :", "&6{given_objects}", "", "&dBal-klikk az előnézethez és kereskedéshez"),
			"es_ES", l("", "&7Que darás :", "&6{taken_objects}", "", "&7Que recibirás :", "&6{given_objects}", "", "&dLeft-click to preview and trade")
			);

	public static final Text MISC_SUPREMESHOPS_TRADEAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_TRADEAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} trade{plural}"),
			"fr_FR", l("&a{amount} échange{plural}"),
			"zh_CN", l("&a{amount} trade{plural}"),
			"zh_TW", l("&a{amount} 交易"),
			"ru_RU", l("&a{amount} обмен{plural}"),
			"hu_HU", l("&a{amount} kereskedés"),
			"es_ES", l("&a{amount} intercambio{plural}")
			);

	public static final Text MISC_SUPREMESHOPS_TRADEAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_TRADEAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Shop has stock for {shop_stock} trade{plural_shop_stock}", "&7You have stock for {player_stock} trade{plural_player_stock}", "", "&dClick to proceed to trade", "&dExit GUI to cancel"),
			"fr_FR", l("&7Le magasin a du stock pour {shop_stock} échange{plural_shop_stock}", "&7Tu as du stock pour {player_stock} échange{plural_player_stock}", "", "&dClic pour procéder à l'échange", "&dSortir du GUI pour annuler"),
			"zh_CN", l("&7Shop has stock for {shop_stock} trade{plural_shop_stock}", "&7You have stock for {player_stock} trade{plural_player_stock}", "", "&dClick to proceed to trade", "&dExit GUI to cancel"),
			"zh_TW", l("&7商店有 {shop_stock} 個交易庫存}", "&7你有 {player_stock} 交易的庫存", "", "&a點擊繼續交易", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7У магазина в наличии {shop_stock} {plural_shop_stock}", "&7У вас в наличии {player_stock} {plural_player_stock}", "", "&dНажмите, чтобы обменяться", "&dВыйдите из меню для отмены"),
			"hu_HU", l("&7Boltnak {shop_stock} készlete van a kereskedéshez", "&7Neked {player_stock} készleted van a kereskedéshez", "", "&dKattints a folytatáshoz.", "&dLépj ki a GUI-ból, hogy megszakítsd"),
			"es_ES", l("&7La tienda tiene un stock de {shop_stock} intercambio{plural_shop_stock}", "&7Tienes stock para {player_stock} intercambio{plural_player_stock}", "", "&dClic para proceder al intercambio", "&dSal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTGUINAME",
			"en_US", l("Item count"),
			"fr_FR", l("Nombre d'items"),
			"zh_CN", l("Item count"),
			"zh_TW", l("物品數量"),
			"ru_RU", l("Количество предметов"),
			"hu_HU", l("Item szám"),
			"es_ES", l("Contador de ítems")
			);

	public static final Text MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTGUINAME",
			"en_US", l("Rent price count"),
			"fr_FR", l("Nombre de prix de location"),
			"zh_TW", l("租借價格數量"),
			"ru_RU", l("Стоимость аренды"),
			"hu_HU", l("Bérleti díj száma"),
			"es_ES", l("Recuento de los precios de alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} item{plural}"),
			"fr_FR", l("&a{amount} item{plural}"),
			"zh_CN", l("&a{amount} item{plural}"),
			"zh_TW", l("&a{amount} 物品"),
			"ru_RU", l("&a{amount} предметов{plural}"),
			"hu_HU", l("&a{amount} item"),
			"es_ES", l("&a{amount} ítem{plural}")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_RESTOCKITEMAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to restock", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour restocker", "&7Sortir du GUI pour annuler"),
			"zh_CN", l("&7Click to restock", "&7Exit GUI to cancel"),
			"zh_TW", l("&f選取好數量後", "&a點擊確認補貨", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы пополнить запасы", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a feltöltéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para hacer restock", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} rent period{plural}"),
			"fr_FR", l("&a{amount} période{plural} de location"),
			"zh_TW", l("&a{amount} &f個租借週期"),
			"ru_RU", l("&a{amount} срок аренды{plural}"),
			"hu_HU", l("&a{amount} bérleti időszak"),
			"es_ES", l("&a{amount} período{plural} de alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to pay", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour payer", "&7Sortir du GUI pour annuler"),
			"zh_TW", l("&a點擊確認支付", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы заплатить", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a fizetéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para pagar", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTGUINAME",
			"en_US", l("Money count"),
			"fr_FR", l("Montant d'argent"),
			"zh_CN", l("Money count"),
			"zh_TW", l("金錢數量"),
			"ru_RU", l("Баланс счёта"),
			"hu_HU", l("Pénz száma"),
			"es_ES", l("Contador de dinero")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTGUINAME",
			"en_US", l("XP level count"),
			"fr_FR", l("Nombre de niveaux d'xp"),
			"zh_CN", l("XP level count"),
			"zh_TW", l("經驗等級數量"),
			"ru_RU", l("Количество уровней"),
			"hu_HU", l("XP szint száma"),
			"es_ES", l("Contador de nivel de XP")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSAMOUNTSELECTGUINAME",
			"en_US", l("PlayerPoints points amount"),
			"fr_FR", l("Nombre de points PlayerPoints"),
			"zh_CN", l("PlayerPoints points amount"),
			"zh_TW", l("PlayerPoints 點數 數量"),
			"ru_RU", l("Количество очков PlayerPoints"),
			"hu_HU", l("PlayerPoints pontok mennyisége"),
			"es_ES", l("Cantidad de puntos PlayerPoints")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTGUINAME = n("MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTGUINAME",
			"en_US", l("TokenEnchant tokens amount"),
			"fr_FR", l("Nombre de tokens TokenEnchant"),
			"zh_TW", l("TokenEnchant 代幣 數量"),
			"ru_RU", l("Количество TokenEnchant токенов"),
			"hu_HU", l("TokenEnchant tokenek mennyisége"),
			"es_ES", l("Cantidad de tokens TokenEnchant")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount}$"),
			"fr_FR", l("&a{amount}$"),
			"zh_CN", l("&a{amount}$"),
			"zh_TW", l("&a{amount}$"),
			"ru_RU", l("&a{amount}$"),
			"hu_HU", l("&a{amount}$"),
			"es_ES", l("&a{amount}$")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_RESTOCKVAULTMONEYAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to restock", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour restocker", "&7Sortir du GUI pour annuler"),
			"zh_CN", l("&7Click to restock", "&7Exit GUI to cancel"),
			"zh_TW", l("&f選取好數量後", "&a點擊確認補貨", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы пополнить запасы", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a feltöltéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para realizar un restock", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} LVL"),
			"fr_FR", l("&a{amount} LVL"),
			"zh_CN", l("&a{amount} LVL"),
			"zh_TW", l("&a{amount} 等級"),
			"ru_RU", l("&a{amount} LVL"),
			"hu_HU", l("&a{amount} szint"),
			"es_ES", l("&a{amount} nivel(es)")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} PlayerPoints points"),
			"fr_FR", l("&a{amount} points PlayerPoints"),
			"zh_CN", l("&a{amount} PlayerPoints points"),
			"zh_TW", l("&a{amount} PlayerPoints 點數"),
			"ru_RU", l("&a{amount} очков PlayerPoints"),
			"hu_HU", l("&a{amount} PlayerPoints pont"),
			"es_ES", l("&a{amount} PlayerPoints puntos")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to restock", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour restocker", "&7Sortir du GUI pour annuler"),
			"zh_CN", l("&7Click to restock", "&7Exit GUI to cancel"),
			"zh_TW", l("&f選取好數量後", "&a點擊確認補貨", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы пополнить запасы", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a feltöltéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para realizar un restock", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTVALUEITEMNAME = n("MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTVALUEITEMNAME",
			"en_US", l("&a{amount} TokenEnchant tokens"),
			"fr_FR", l("&a{amount} tokens TokenEnchant"),
			"zh_TW", l("&a{amount} TokenEnchant 代幣"),
			"ru_RU", l("&a{amount} TokenEnchant токенов"),
			"hu_HU", l("&a{amount} TokenEnchant token"),
			"es_ES", l("&a{amount} tokens de TokenEnchant")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to restock", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour restocker", "&7Sortir du GUI pour annuler"),
			"zh_TW", l("&f選取好數量後", "&a點擊確認補貨", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы пополнить запасы", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a feltöltéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para realizar un restock", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMLORE = n("MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMLORE",
			"en_US", l("&7Click to restock", "&7Exit GUI to cancel"),
			"fr_FR", l("&7Clic pour restocker", "&7Sortir du GUI pour annuler"),
			"zh_CN", l("&7Click to restock", "&7Exit GUI to cancel"),
			"zh_TW", l("&f選取好數量後", "&a點擊確認補貨", "&7(關閉GUI 取消)"),
			"ru_RU", l("&7Нажмите, чтобы пополнить запасы", "&7Выйдите из меню, дабы отменить"),
			"hu_HU", l("&7Kattints a feltöltéshez", "&7Megszakításhoz lépj ki a GUI-ból"),
			"es_ES", l("&7Clic para hacer restock", "&7Sal de la GUI para cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITCLOSEDITEMNAME = n("MISC_SUPREMESHOPS_SHOPEDITCLOSEDITEMNAME",
			"en_US", l("&cClosed shop"),
			"fr_FR", l("&cShop fermé"),
			"zh_CN", l("&cClosed shop"),
			"zh_TW", l("&f商店目前狀態: &c打烊"),
			"ru_RU", l("&cЗакрытый магазин"),
			"hu_HU", l("&cZárt üzlet"),
			"es_ES", l("&cTienda cerrada")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITOPENEDITEMNAME = n("MISC_SUPREMESHOPS_SHOPEDITOPENEDITEMNAME",
			"en_US", l("&aOpened shop"),
			"fr_FR", l("&aShop ouvert"),
			"zh_CN", l("&aOpened shop"),
			"zh_TW", l("&f商店目前狀態: &a營業中"),
			"ru_RU", l("&aОткрытый магазин"),
			"hu_HU", l("&aNyitott üzlet"),
			"es_ES", l("&aTienda abierta")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITNOTRENTABLEITEMNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITNOTRENTABLEITEMNAME",
			"en_US", l("&cCurrently not rentable"),
			"fr_FR", l("&cActuellement non louable"),
			"zh_TW", l("&f是否可被租借: &c否"),
			"ru_RU", l("&cВ данный момент не подлежит аренде"),
			"hu_HU", l("&cJelenleg nem bérelhető"),
			"es_ES", l("&cActualmente no alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTABLEITEMNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTABLEITEMNAME",
			"en_US", l("&aCurrently rentable"),
			"fr_FR", l("&aActuellement louable"),
			"zh_TW", l("&f是否可被租借: &a是"),
			"ru_RU", l("&aПодлежит аренде"),
			"hu_HU", l("&aJelenleg bérelhető"),
			"es_ES", l("&aActualmente alquilable")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITCLOSEDITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITCLOSEDITEMNAME",
			"en_US", l("&cClosed merchant"),
			"fr_FR", l("&cMarchand fermé"),
			"zh_CN", l("&cClosed merchant"),
			"zh_TW", l("&f商人目前狀態: &c打烊"),
			"ru_RU", l("&cЗакрытый торговец"),
			"hu_HU", l("&cBezárt kereskedő"),
			"es_ES", l("&cMercader cerrado")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITOPENEDITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITOPENEDITEMNAME",
			"en_US", l("&aOpened merchant"),
			"fr_FR", l("&aMarchand ouvert"),
			"zh_CN", l("&aOpened merchant"),
			"zh_TW", l("&f商人目前狀態: &a營業中"),
			"ru_RU", l("&aОткрытый торговец"),
			"hu_HU", l("&aNyitott kereskedő"),
			"es_ES", l("&aMercader abierto")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITNONREMOTEITEMNAME = n("MISC_SUPREMESHOPS_SHOPEDITNONREMOTEITEMNAME",
			"en_US", l("&cRemote shop disabled"),
			"fr_FR", l("&cShop à distance désactivé"),
			"zh_CN", l("&cRemote shop disabled"),
			"zh_TW", l("&f遠程商店: &c關閉"),
			"ru_RU", l("&cУдалённый магазин отключен"),
			"hu_HU", l("&cTávoli bolt letiltva"),
			"es_ES", l("&cTienda remota deshabilitada")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITREMOTEITEMNAME = n("MISC_SUPREMESHOPS_SHOPEDITREMOTEITEMNAME",
			"en_US", l("&aRemote shop enabled"),
			"fr_FR", l("&aShop à distance activé"),
			"zh_CN", l("&aRemote shop enabled"),
			"zh_TW", l("&f遠程商店: &a開啟"),
			"ru_RU", l("&cУдалённый магазин включен"),
			"hu_HU", l("&aTávoli bolt engedélyezve"),
			"es_ES", l("&aTienda remota habilitada")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITNONREMOTEITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITNONREMOTEITEMNAME",
			"en_US", l("&cRemote merchant disabled"),
			"fr_FR", l("&cMarchand à distance désactivé"),
			"zh_CN", l("&cRemote merchant disabled"),
			"zh_TW", l("&f遠程商人: &c關閉"),
			"ru_RU", l("&cУдалённый торговец отключен"),
			"hu_HU", l("&cTávoli kereskedő letiltva"),
			"es_ES", l("&cMercader remoto deshabilitado")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITREMOTEITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITREMOTEITEMNAME",
			"en_US", l("&aRemote merchant enabled"),
			"fr_FR", l("&aMarchand à distance activé"),
			"zh_CN", l("&aRemote merchant enabled"),
			"zh_TW", l("&f遠程商人: &a開啟"),
			"ru_RU", l("&cУдалённый торговец включен"),
			"hu_HU", l("&aTávoli kereskedő engedélyezve"),
			"es_ES", l("&aMercader remoto habilitado")
			);

	public static final Text MISC_SUPREMESHOPS_EDITDISPLAYITEMSITEMNAME = n("MISC_SUPREMESHOPS_EDITDISPLAYITEMSITEMNAME",
			"en_US", l("&aDisplay items enabled"),
			"fr_FR", l("&aItems d'affichage activés"),
			"zh_CN", l("&aDisplay items enabled"),
			"zh_TW", l("&f展示物品: &a開啟"),
			"ru_RU", l("&aОтображение предметов включено"),
			"hu_HU", l("&aItemek megjelenítése engedélyezve"),
			"es_ES", l("&aMostrar objetos activado")
			);

	public static final Text MISC_SUPREMESHOPS_EDITNONDISPLAYITEMSITEMNAME = n("MISC_SUPREMESHOPS_EDITNONDISPLAYITEMSITEMNAME",
			"en_US", l("&cDisplay items disabled"),
			"fr_FR", l("&cItems d'affichage désactivés"),
			"zh_CN", l("&cDisplay items disabled"),
			"zh_TW", l("&f展示物品: &c關閉"),
			"ru_RU", l("&aОтображение предметов выключено"),
			"hu_HU", l("&cItemek megjelenítése letiltva"),
			"es_ES", l("&cMostrar objetos desactivado")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADMINSTOCKITEMNAME = n("MISC_SUPREMESHOPS_EDITADMINSTOCKITEMNAME",
			"en_US", l("&aStock management enabled"),
			"fr_FR", l("&aManagement de stock activé"),
			"zh_TW", l("&f庫存管理功能: &a開啟"),
			"ru_RU", l("&aУправление остатками включено"),
			"hu_HU", l("&aRaktárkészlet kezelés engedélyezve"),
			"es_ES", l("&aGestor de stock activado")
			);

	public static final Text MISC_SUPREMESHOPS_EDITNONADMINSTOCKITEMNAME = n("MISC_SUPREMESHOPS_EDITNONADMINSTOCKITEMNAME",
			"en_US", l("&cStock management disabled"),
			"fr_FR", l("&cManagement de stock désactivé"),
			"zh_TW", l("&f庫存管理功能: &c關閉"),
			"ru_RU", l("&aУправление остатками выключено"),
			"hu_HU", l("&cRaktárkészlet kezelés letiltva"),
			"es_ES", l("&cGestor de stock desactivado")
			);

	public static final Text MISC_SUPREMESHOPS_EDITMANAGERSNAME = n("MISC_SUPREMESHOPS_EDITMANAGERSNAME",
			"en_US", l("&aShop managers"),
			"fr_FR", l("&aManagers de shop"),
			"zh_TW", l("&a商店管理者"),
			"ru_RU", l("&aМенеджеры магазина"),
			"hu_HU", l("&aBolt kezelők"),
			"es_ES", l("&aManagers de la tienda")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPRICENAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPRICENAME",
			"en_US", l("&aRent price"),
			"fr_FR", l("&aPrix de location"),
			"zh_TW", l("&a出租價格"),
			"ru_RU", l("&aСтоимость аренды"),
			"hu_HU", l("&aBérleti ár"),
			"es_ES", l("&aPrecio del alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODNAME",
			"en_US", l("&aRent payment period"),
			"fr_FR", l("&aFréquence de paiement de location"),
			"zh_TW", l("&a租金付款週期"),
			"ru_RU", l("&aОплачиваемый период аренды"),
			"hu_HU", l("&aBérleti díj fizetési periódus"),
			"es_ES", l("&aPeríodo de pago de alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODLORE = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODLORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenleg: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITNAME",
			"en_US", l("&aRent period streak limit"),
			"fr_FR", l("&aLimite de périodes de location consécutives"),
			"zh_TW", l("&a最大租借週期限制"),
			"ru_RU", l("&aНепрерывных оплачиваемых периодов аренды"),
			"hu_HU", l("&aBérleti periódusok közötti időkorlát"),
			"es_ES", l("&aLímite de períodos de alquiler consecutivos")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITLORE = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITLORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenleg: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYNAME",
			"en_US", l("&aDelay between rent period streak limit"),
			"fr_FR", l("&aDélai entre la limite de périodes de location consécutives"),
			"zh_TW", l("&a最大租借週期限制之間的延遲"),
			"ru_RU", l("&aЗадержка между непрерывными оплачиваемыми периодами аренды"),
			"hu_HU", l("&aKésés a bérleti periódusok kötötti időcsíknál"),
			"es_ES", l("&aEspera entre períodos de alquiler consecutivos")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYLORE = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYLORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenlegi: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSNAME",
			"en_US", l("&aMerchant managers"),
			"fr_FR", l("&aManagers de marchand"),
			"zh_CN", l("&aMerchant managers"),
			"zh_TW", l("&a商人管理者"),
			"ru_RU", l("&aМенеджеры торговца"),
			"hu_HU", l("&aKereskedő kezelők"),
			"es_ES", l("&aManagers de los mercaderes")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITTRADESLIMITNAME = n("MISC_SUPREMESHOPS_SHOPEDITTRADESLIMITNAME",
			"en_US", l("&aMaximum trades per player"),
			"fr_FR", l("&aNombre d'échanges maximum par joueur"),
			"zh_TW", l("&a每位玩家的最大交易次數"),
			"ru_RU", l("&aЛимит обменов на игрока"),
			"hu_HU", l("&aMaximális kereskedés per játékos"),
			"es_ES", l("&aCantidad máxima de comercios por jugador")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITTRADESLIMITNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITTRADESLIMITNAME",
			"en_US", l("&aMaximum trades per player"),
			"fr_FR", l("&aNombre d'échanges maximum par joueur"),
			"zh_TW", l("&a每位玩家的最大交易次數"),
			"ru_RU", l("&aЛимит обменов на игрока"),
			"hu_HU", l("&aMaximális kereskedés per játékos"),
			"es_ES", l("&aCantidad máxima de comercios por jugador")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSNAME = n("MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSNAME",
			"en_US", l("&aTrade conditions"),
			"fr_FR", l("&aConditions d'échange"),
			"zh_TW", l("&a交易條件"),
			"ru_RU", l("&aУсловия обмена"),
			"hu_HU", l("&aKereskedési feltételek"),
			"es_ES", l("&aCondiciones de comercio")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTONDITIONSNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTONDITIONSNAME",
			"en_US", l("&aRent conditions"),
			"fr_FR", l("&aConditions de location"),
			"zh_TW", l("&a出租條件"),
			"ru_RU", l("&aУсловия аренды"),
			"hu_HU", l("&aBérlési feltételek"),
			"es_ES", l("&aCondiciones de alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGNAME",
			"en_US", l("&cForce stop current renting"),
			"fr_FR", l("&cForcer la fin de la location actuelle"),
			"zh_TW", l("&a強制停止當前租借合約"),
			"ru_RU", l("&cПринудительно отменить нынешнюю аренду"),
			"hu_HU", l("&cA jelenlegi bérlés kényszerítése"),
			"es_ES", l("&cForzar la detención del alquiler actual")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGLORE = n("MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGLORE",
			"en_US", l("&7Prematurely stop the current renting", "&7The owner will receive future rent prices paid in advanced", "&7 if any and stocked benefits or it'll be dropped on", "&7 the ground. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"fr_FR", l("&7Arrêter prématurément la location actuelle", "&7Le vendeur actuel recevra les prix de locations payés d'avance", "&7 et les bénéfices stockés ou ils", "&7 seront droppés au sol. Cette action est irréversible.", "&7Un GUI de confirmation sera ouverte au clic"),
			"zh_TW", l("&7提早停止當前的租借合約", "&7將返還當前的租借者 預繳的租金", "&7如果有任何的商店庫存或是物品", "&7將會直接掉落於地上。", "&c[注意]這個動作不可復原", "&7(點擊後將會進行二次確認)"),
			"ru_RU", l("&7Преждевременный отказ от текущей аренды", "&7Уплаченная сумма будет зачислена в качестве депозита", "&7, а все остатки будут выброшены на землю.", "&7Это действие необратимо.", "&7Меню подтверждения будет открыто по нажатию."),
			"hu_HU", l("&7A jelenlegi bérleti díj idő előtti leállítása", "&7A tulajdonos meg fogja kapni az előre kifizetett", "&7 bérleti díjat, ha van ilyen, és a raktárkészlet", "&7 el lesz küldve, vagy a földre esik.", "&7Ez a művelet visszafordíthatatlan.", "&7A megerősítő GUI menü megnyilik egy kattintással."),
			"es_ES", l("&7Detén prematuramente el alquiler actual. El dueño recibirá", "&7el precio pagado del alquiler por adelantado si hay beneficios", "&7o se dropearán en el suelo. Esta acción es irreversible.", "&7Una GUI de confirmación se abrirá al hacer clic")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTNAME",
			"en_US", l("&aRent"),
			"fr_FR", l("&aLocation"),
			"zh_TW", l("&a出租"),
			"ru_RU", l("&aАренда"),
			"hu_HU", l("&aBérlés"),
			"es_ES", l("&aAlquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITRENTLORE = n("MISC_SUPREMESHOPS_RENTABLEEDITRENTLORE",
			"en_US", l("&7You've rented this for &a{rent_time}{rent_limit} &7periods", "&7You've paid &a{paid_rents} &7rents in advance", "&7Rent must be paid &a{rent_period}", "&dLeft-click to see rent price", "&dRight-click to pay rent (in advance ?)"),
			"fr_FR", l("&7Vous avez loué ceci pour &a{rent_time}{rent_limit} &7périodes", "&7Vous avez payé &a{paid_rents} &7loyers en avance", "&7Fréquence de paiement du loyer : &a{rent_period}", "&dClic-gauche pour voir le prix de location", "&dClic-droit pour payer la location (d'avance ?)"),
			"zh_TW", l("&7你已租借這個商店 &a{rent_time}{rent_limit} &7週期", "&7你已預先支付 &a{paid_rents} &7個週期的租金", "&6這間商店的租金必須 {rent_period} &6支付", "&f左鍵點擊 &a查看租金", "&f右鍵點擊 &a支付租金(或預繳)"),
			"ru_RU", l("&7Вы арендовали это на &a{rent_time}{rent_limit}", "&7Вы оплатили &a{paid_rents} &7сроков", "&7Аренда должна быть уплачена раз в  &a{rent_period}", "&dЛКМ, чтобы посмотреть цену", "&dПКМ, чтобы оплатить"),
			"hu_HU", l("&7Kibérelted &a{rent_time}{rent_limit} &7időszakra", "&7Kifizetett &a{paid_rents} &7díjak", "&7Bérleti díj fizetve lesz &a{rent_period}", "&dBal-klikk, hogy megnézd a bérlet árakat", "&dJobb-klikk a bérleti díj kifizetése (előre ?)"),
			"es_ES", l("&7Has alquilado esto por &a{rent_time}{rent_limit} &7período(s)", "&7Has pagado &a{paid_rents} &7alquileres por adelantado", "&7El alquiler debe ser pagado el &a{rent_period}", "&dClic izquierdo para ver el precio de alquiler", "&dClic derecho para pagar el alquiler (¿por adelantado?)")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDSHOPRENTITEMNAME = n("MISC_SUPREMESHOPS_UNRENTEDSHOPRENTITEMNAME",
			"en_US", l("&aRent info"),
			"fr_FR", l("&aInfos de location"),
			"zh_TW", l("&a出租資訊"),
			"ru_RU", l("&aИнформация об аренде"),
			"hu_HU", l("&aBérleti info"),
			"es_ES", l("&aInformación del alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDSHOPRENTITEMLORE = n("MISC_SUPREMESHOPS_UNRENTEDSHOPRENTITEMLORE",
			"en_US", l("&7You can rent this shop for &a{rent_limit} &7periods", "&7Rent must be paid &a{rent_period}", "&dRight-click to rent shop"),
			"fr_FR", l("&7Vous pouvez louer ce shop pour &a{rent_limit} &7périodes", "&7Fréquence de paiement du loyer : &a{rent_period}", "&dClic-droit pour louer le shop"),
			"zh_TW", l("&7你可以租借這間商店 &a{rent_limit} &7個週期", "&6這間商店的租金必須 {rent_period} &6支付", "&f右鍵點擊 &a租借這間商店"),
			"ru_RU", l("&7Вы можете арендовать этот магазин на &a{rent_limit}", "&7Аренда должна быть уплачена раз в &a{rent_period}", "&dПКМ, чтобы оплатить"),
			"hu_HU", l("&7Kibérelheted ezt  a boltot &a{rent_limit} &7időszakra", "&7Kifizetett bérleti díj &a{rent_period}", "&dJobb-klikk, hogy kibéreld a boltot"),
			"es_ES", l("&7Puedes alquilar esta tienda por &a{rent_limit} &7períodos", "&7El alquiler debe pagarse el &a{rent_period}", "&dClic derecho para alquilar")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDRENTITEMNAME = n("MISC_SUPREMESHOPS_UNRENTEDRENTITEMNAME",
			"en_US", l("&aRent info"),
			"fr_FR", l("&aInfos de location"),
			"zh_TW", l("&a出租資訊"),
			"ru_RU", l("&aИнформация об аренде"),
			"hu_HU", l("&aBérleti info"),
			"es_ES", l("&aInformación del alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_UNRENTEDRENTITEMLORE = n("MISC_SUPREMESHOPS_UNRENTEDRENTITEMLORE",
			"en_US", l("&7You can rent this for &a{rent_limit} &7periods", "&7Rent must be paid &a{rent_period}", "&dRight-click to rent"),
			"fr_FR", l("&7Vous pouvez louer ceci pour &a{rent_limit} &7périodes", "&7Fréquence de paiement du loyer : &a{rent_period}", "&dClic-droit pour louer"),
			"zh_TW", l("&7你可以租借這個商人 &a{rent_limit} &7個週期", "&6這個商人的租金必須 {rent_period} &6支付", "&f右鍵點擊 &a租借這個商人"),
			"ru_RU", l("&7Вы можете арендовать этот магазин на &a{rent_limit}", "&7Аренда должна быть уплачена раз в &a{rent_period}", "&dПКМ, чтобы оплатить"),
			"hu_HU", l("&7Kibérelheted ezt  a boltot &a{rent_limit} &7időszakra", "&7Kifizetett bérleti díj &a{rent_period}", "&dJobb-klikk, hogy kibéreld a boltot"),
			"es_ES", l("&7Puedes alquilar esto por &a{rent_limit} &7períodos", "&7El alquiler debe ser pagado el &a{rent_period}", "&dClic derecho para alquilar")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGNAME = n("MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGNAME",
			"en_US", l("&cStop renting"),
			"fr_FR", l("&cArrêter de louer"),
			"zh_TW", l("&c停止租借"),
			"ru_RU", l("&cЗакончить аренду"),
			"hu_HU", l("&cBérlés megállítása"),
			"es_ES", l("&cParar el alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGLORE = n("MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGLORE",
			"en_US", l("&7Prematurely stop the current renting", "&7You'll receive future rent prices paid in advanced", "&7 if any and stocked benefits or it'll be dropped on", "&7 the ground. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"fr_FR", l("&7Arrêter prématurément la location actuelle", "&7Vous recevrez les prix de locations payés d'avance", "&7 et les bénéfices stockés ou ils", "&7 seront droppés au sol. Cette action est irréversible.", "&7Un GUI de confirmation sera ouverte au clic"),
			"zh_TW", l("&7提早停止當前的租借合約", "&7將返還當前 預繳的租金", "&7如果有任何的商店庫存或是物品", "&7將會直接掉落於地上。", "&c[注意]這個動作不可復原", "&7(點擊後將會進行二次確認)"),
			"ru_RU", l("&7Преждевременный отказ от текущей аренды", "&7Уплаченная сумма будет зачислена в качестве депозита", "&7, а все остатки будут выброшены на землю.", "&7Это действие необратимо.", "&7Меню подтверждения будет открыто по нажатию."),
			"hu_HU", l("&7A jelenlegi bérleti díj idő előtti leállítása", "&7A tulajdonos meg fogja kapni az előre kifizetett", "&7 bérleti díjat, ha van ilyen, és a raktárkészlet", "&7 el lesz küldve, vagy a földre esik.", "&7Ez a művelet visszafordíthatatlan.", "&7A megerősítő GUI menü megnyilik egy kattintással."),
			"es_ES", l("&7Finaliza prematuramente el alquiler actual del mercader.", "&7Recibirás los futuros precios de alquiler pagados por adelantado", "&7si hay algún beneficio en stock o serán dropeados en el suelo.", "&7Las tiendas pertenecientes a este mercader serán eliminadas y", "&7las otras serán desenlazadas. &c¡Esta acción es irreversible!", "&7Una GUI de confirmación saldrá al hacer clic")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGNAME",
			"en_US", l("&cStop renting"),
			"fr_FR", l("&cArrêter de louer"),
			"zh_TW", l("&c停止租借"),
			"ru_RU", l("&cЗакончить аренду"),
			"hu_HU", l("&cBérlés megállítása"),
			"es_ES", l("&cParar el alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGLORE = n("MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGLORE",
			"en_US", l("&7Prematurely stop the current renting", "&7You'll receive future rent prices paid in advanced", "&7 if any and stocked benefits or it'll be dropped on", "&7 the ground. Shops belonging to this merchant will be removed", "&7 and others will be unlinked. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"fr_FR", l("&7Arrêter prématurément la location actuelle", "&7Vous recevrez les prix de locations payés d'avance", "&7 et les bénéfices stockés ou ils", "&7 seront droppés au sol. Les shops qui appartiennent à ce marchand seront supprimés", "&7 et les autres seront déliés. Cette action est irréversible.", "&7Un GUI de confirmation sera ouverte au clic"),
			"zh_TW", l("&7提早停止當前的租借合約", "&7將返還當前 預繳的租金", "&7如果有任何的商店庫存或是物品", "&7將會直接掉落於地上。", "&7屬於此商人的商店將會被刪除", "&7其他的會被解除連結。", "&c[注意]這個動作不可復原", "&7(點擊後將會進行二次確認)"),
			"ru_RU", l("&7Преждевременный отказ от текущей аренды", "&7Уплаченная сумма будет зачислена в качестве депозита", "&7, а все остатки будут выброшены на землю.", "&7Это действие необратимо.", "&7Меню подтверждения будет открыто по нажатию."),
			"hu_HU", l("&7A jelenlegi bérleti díj idő előtti leállítása", "&7A tulajdonos meg fogja kapni az előre kifizetett", "&7 bérleti díjat, ha van ilyen, és a raktárkészlet", "&7 el lesz küldve, vagy a földre esik.", "&7Ehhez a kerekedőhöz tartozó összes üzlet", "&7 el lesz távolítva és más meg fog szünni.", "&7Ez a művelet visszafordíthatatlan.", "&7A megerősítő GUI menü megnyilik egy kattintással."),
			"es_ES", l("&7Finaliza prematuramente el alquiler actual de la tienda.", "&7Recibirás los futuros precios de alquiler pagados por adelantado", "&7si hay algún beneficio en stock o serán dropeados en el suelo.", "&7Las tiendas pertenecientes a este mercader serán eliminadas y", "&7las otras serán desenlazadas. &c¡Esta acción es irreversible!", "&7Una GUI de confirmación saldrá al hacer clic")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITCONDITIONSNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITCONDITIONSNAME",
			"en_US", l("&aInteract conditions"),
			"fr_FR", l("&aConditions d'interaction"),
			"zh_CN", l("&aInteract conditions"),
			"zh_TW", l("&a交互條件"),
			"ru_RU", l("&aУсловия взаимодействия"),
			"hu_HU", l("&aInterakt feltételek"),
			"es_ES", l("&aCondiciones de interacción")
			);

	public static final Text MISC_SUPREMESHOPS_EDITSELLTOPLAYERNAME = n("MISC_SUPREMESHOPS_EDITSELLTOPLAYERNAME",
			"en_US", l("&4Sell shop"),
			"fr_FR", l("&4Vendre le shop"),
			"zh_CN", l("&4Sell shop"),
			"zh_TW", l("&4販賣商店"),
			"ru_RU", l("&4Продать магазин"),
			"hu_HU", l("&4Eladó bolt"),
			"es_ES", l("&4Tienda de ventas")
			);

	public static final Text MISC_SUPREMESHOPS_EDITDISPLAYNAMENAME = n("MISC_SUPREMESHOPS_EDITDISPLAYNAMENAME",
			"en_US", l("&aDisplay name"),
			"fr_FR", l("&aNom d'affichage"),
			"zh_TW", l("&a顯示名稱"),
			"ru_RU", l("&aОтображаемое имя"),
			"hu_HU", l("&aMegjelenítendő név"),
			"es_ES", l("&aNombre a mostrar")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITDISPLAYNAMENAME = n("MISC_SUPREMESHOPS_MERCHANTEDITDISPLAYNAMENAME",
			"en_US", l("&aDisplay name"),
			"fr_FR", l("&aNom d'affichage"),
			"zh_CN", l("&aDisplay name"),
			"zh_TW", l("&a顯示名稱"),
			"ru_RU", l("&aОтображаемое имя"),
			"hu_HU", l("&aMegjelenítendő név"),
			"es_ES", l("&aNombre a mostrar")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNNAME = n("MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNNAME",
			"en_US", l("&aParticle pattern"),
			"fr_FR", l("&aPattern de particules"),
			"zh_CN", l("&aParticle pattern"),
			"zh_TW", l("&a粒子圖案"),
			"ru_RU", l("&aПаттерн частиц"),
			"hu_HU", l("&aRészecske minta"),
			"es_ES", l("&aPatrón de partículas")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNNAME = n("MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNNAME",
			"en_US", l("&aParticle pattern (unrented)"),
			"fr_FR", l("&aPattern de particules (non loué)"),
			"zh_TW", l("&a粒子圖案 &c(待出租)"),
			"ru_RU", l("&aПаттерн частиц (не арендовано)"),
			"hu_HU", l("&aRészecske minta (nem kibérelt)"),
			"es_ES", l("&aPatrón de partículas (no alquilado)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITPARTICLEPATTERNNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITPARTICLEPATTERNNAME",
			"en_US", l("&aParticle pattern"),
			"fr_FR", l("&aPattern de particules"),
			"zh_CN", l("&aParticle pattern"),
			"zh_TW", l("&a粒子圖案"),
			"ru_RU", l("&aПаттерн частиц"),
			"hu_HU", l("&aRészecske minta"),
			"es_ES", l("&aPatrón de partículas")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSKINNNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSKINNNAME",
			"en_US", l("&aSkin"),
			"fr_FR", l("&aSkin"),
			"zh_CN", l("&aSkin"),
			"zh_TW", l("&a皮膚"),
			"ru_RU", l("&aСкин"),
			"hu_HU", l("&aKinézet"),
			"es_ES", l("&aSkin")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTNAME",
			"en_US", l("&aEquipment"),
			"fr_FR", l("&aÉquipement"),
			"zh_CN", l("&aEquipment"),
			"zh_TW", l("&a裝備"),
			"ru_RU", l("&aОборудование"),
			"hu_HU", l("&aFelszerelés"),
			"es_ES", l("&aEquipamiento")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSTATUSNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSTATUSNAME",
			"en_US", l("&aStatus"),
			"fr_FR", l("&aStatus"),
			"zh_CN", l("&aStatus"),
			"zh_TW", l("&a狀態"),
			"ru_RU", l("&aСтатус"),
			"hu_HU", l("&aStátusz"),
			"es_ES", l("&aEstado")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITSHOPSNAME = n("MISC_SUPREMESHOPS_MERCHANTEDITSHOPSNAME",
			"en_US", l("&aShops of this merchant"),
			"fr_FR", l("&aShops de ce marchand"),
			"zh_CN", l("&aLinked shops"),
			"zh_TW", l("&a這個商人的商店"),
			"ru_RU", l("&aМагазин этого торговца"),
			"hu_HU", l("&aA kereksedő üzletei"),
			"es_ES", l("&aTiendas de este mercader")
			);

	public static final Text MISC_SUPREMESHOPS_EDITPARTICLEPATTERNSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITPARTICLEPATTERNSELECTEDLORE",
			"en_US", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"fr_FR", l("&7&l(sélectionné)", "&dShift + clic-droit pour le supprimer"),
			"zh_CN", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"zh_TW", l("&a(已選定)", "&fSHIFT+右鍵點擊 &7移除選定"),
			"ru_RU", l("&7&l(выбрано)", "&dSHIFT + ПКМ, чтобы удалить"),
			"hu_HU", l("&7&l(kiválasztott)", "&dShift + jobb-klikk, hogy eltávolítsd ezt")
			);

	public static final Text MISC_SUPREMESHOPS_EDITPARTICLEPATTERNNOTSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITPARTICLEPATTERNNOTSELECTEDLORE",
			"en_US", l("&dRight-click to select it"),
			"fr_FR", l("&dClic-droit pour le sélectionner"),
			"zh_CN", l("&dRight-click to select it"),
			"zh_TW", l("&f右鍵點擊 &7選定"),
			"ru_RU", l("&dПКМ, чтобы выбрать"),
			"hu_HU", l("&dJobb-klikk, hogy kiválaszd"),
			"es_ES", l("&dClic derecho para seleccionarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITEQUIPMENTSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITEQUIPMENTSELECTEDLORE",
			"en_US", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"fr_FR", l("&7&l(sélectionné)", "&dShift + clic-droit pour le supprimer"),
			"zh_CN", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"zh_TW", l("&a(已選定)", "&fSHIFT+右鍵點擊 &7移除選定"),
			"ru_RU", l("&7&l(выбрано)", "&dSHIFT + ПКМ, чтобы удалить"),
			"hu_HU", l("&7&l(kiválasztott)", "&dShift + jobb-klikk, hogy eltávolítsd ezt"),
			"es_ES", l("&7&l(seleccionado)", "&dShift + clic derecho para eliminarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITEQUIPMENTNOTSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITEQUIPMENTNOTSELECTEDLORE",
			"en_US", l("&dRight-click to select it"),
			"fr_FR", l("&dClic-droit pour le sélectionner"),
			"zh_CN", l("&dRight-click to select it"),
			"zh_TW", l("&f右鍵點擊 &7選定"),
			"ru_RU", l("&dПКМ, чтобы выбрать"),
			"hu_HU", l("&dJobb-klikk, hogy kiválaszd"),
			"es_ES", l("&dClic derecho para seleccionarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITSKINSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITSKINSELECTEDLORE",
			"en_US", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"fr_FR", l("&7&l(sélectionné)", "&dShift + clic-droit pour le supprimer"),
			"zh_CN", l("&7&l(selected)", "&dShift + right-click to remove it"),
			"zh_TW", l("&a(已選定)", "&fSHIFT+右鍵點擊 &7移除選定"),
			"ru_RU", l("&7&l(выбрано)", "&dSHIFT + ПКМ, чтобы удалить"),
			"hu_HU", l("&7&l(kiválasztott)", "&dShift + jobb-klikk, hogy eltávolítsd ezt"),
			"es_ES", l("&7&l(seleccionado)", "&dShift + clic derecho para eliminarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITSKINNOTSELECTEDLORE = n("MISC_SUPREMESHOPS_EDITSKINNOTSELECTEDLORE",
			"en_US", l("&dRight-click to select it"),
			"fr_FR", l("&dClic-droit pour le sélectionner"),
			"zh_CN", l("&dRight-click to select it"),
			"zh_TW", l("&f右鍵點擊 &7選定"),
			"ru_RU", l("&dПКМ, чтобы выбрать"),
			"hu_HU", l("&dJobb-klikk, hogy kiválaszd"),
			"es_ES", l("&dClic derecho para seleccionarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANEDITLINELORE = n("MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANEDITLINELORE",
			"en_US", l("&a&l(belongs to this merchant)", "&dRight-click to edit it"),
			"fr_FR", l("&a&l(appartient à ce marchand)", "&dClic-droit pour l'éditer"),
			"zh_TW", l("&a(屬於這個商人)", "&f右鍵點擊 &7編輯"),
			"ru_RU", l("&a&l(относится к этому торговцу)", "&dПКМ, чтобы редактировать"),
			"hu_HU", l("&a&l(Ehhez a kereskedőhöz tartozik)", "&dJobb-klikk, hogy szerkeszd"),
			"es_ES", l("&a&l(pertenece a este mercader)", "&dClic derecho para editarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANTEDITLINELORE = n("MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANTEDITLINELORE",
			"en_US", l("&a&l(belongs to this merchant)"),
			"fr_FR", l("&a&l(appartient à ce marchand)"),
			"zh_TW", l("&a(屬於這個商人)"),
			"ru_RU", l("&a&l(относится к этому торговцу)"),
			"hu_HU", l("&a&l(ehhez a kereskedőhöz tartozik)"),
			"es_ES", l("&a&l(pertenece a este mercader)")
			);

	public static final Text MISC_SUPREMESHOPS_EDITMERCHANTSHOPLINKEDLINELORE = n("MISC_SUPREMESHOPS_EDITMERCHANTSHOPLINKEDLINELORE",
			"en_US", l("&a&l(linked to this merchant)", "&dRight-click to unlink it"),
			"fr_FR", l("&a&l(lié à ce marchand)", "&dClic-droit pour le délier"),
			"zh_TW", l("&a(連結至這個商人)", "&f右鍵點擊 &7取消連結"),
			"ru_RU", l("&a&l(привязано к этому торговцу)", "&dПКМ, чтобы отвязать"),
			"hu_HU", l("&a&l(ehhez a kereskedőhöz kapcsolódik)", "&dJobb-klikk a leválasztáshoz"),
			"es_ES", l("&a&l(linkeado a este mercader)", "&dClic derecho para desenlazarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITMERCHANTSHOPNOTLINKEDLINELORE = n("MISC_SUPREMESHOPS_EDITMERCHANTSHOPNOTLINKEDLINELORE",
			"en_US", l("&c&l(not linked to this merchant)", "&dRight-click to link it"),
			"fr_FR", l("&c&l(pas lié à ce marchand)", "&dClic-droit pour le lier"),
			"zh_TW", l("&c(商為連結至這個商人)", "&f右鍵點擊 &7連結"),
			"ru_RU", l("&a&l(не привязано к этому торговцу)", "&dПКМ, чтобы привязать"),
			"hu_HU", l("&c&l(Nincs csatlakozás ehhez a kereskedőhöz)", "&dJobb-klikk, hogy csatlakoztasd"),
			"es_ES", l("&c&l(no linkeado con este mercader)", "&dClic derecho para enlazarlo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE = n("MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE",
			"en_US", l("&7You don't have the permission for this"),
			"fr_FR", l("&7Vous n'avez pas la permission pour ceci"),
			"zh_CN", l("&7You don't have the permission for this"),
			"zh_TW", l("&7你沒有此權限"),
			"ru_RU", l("&7У вас недостаточно прав"),
			"hu_HU", l("&7Ehhez nincs jogod"),
			"es_ES", l("&7No tienes permisos para hacer esto")
			);

	public static final Text MISC_SUPREMESHOPS_EDITTAKEITEMSNAME = n("MISC_SUPREMESHOPS_EDITTAKEITEMSNAME",
			"en_US", l("&aTaken objects"),
			"fr_FR", l("&aObjets pris"),
			"zh_CN", l("&aTaken objects"),
			"zh_TW", l("&a(買家->你 的東西)"),
			"ru_RU", l("&aВыбранны предметы"),
			"hu_HU", l("&aElvett tárgyak"),
			"es_ES", l("&aObjetos recibidos")
			);

	public static final Text MISC_SUPREMESHOPS_EDITTAKEITEMSLORE = n("MISC_SUPREMESHOPS_EDITTAKEITEMSLORE",
			"en_US", l("&7Edit objects taken by the shop during", "&7 the trade, or withdraw benefits"),
			"fr_FR", l("&7Éditer les objets pris par le shop", "&7 durant l'échange, ou retirer les bénéfices"),
			"zh_CN", l("&7Edit objects taken by the shop during", "&7 the trade, or withdraw benefits"),
			"zh_TW", l("&7編輯在交易中商店從玩家那拿取的物件", "&7或是取出 (買家->你 的東西)"),
			"ru_RU", l("&7Изменить предметы, полученные магазином", "&7или вывести полученное"),
			"hu_HU", l("&7Szerkesztheti a bolt által", "&7a kereskedelem során elfoglalt tárgyakat,", "&7 vagy visszavonhatja az juttatásokat"),
			"es_ES", l("&7Editar objetos recibidos por la tienda durante el", "&7intercambio o retirar beneficios")
			);

	public static final Text MISC_SUPREMESHOPS_EDITGIVEITEMSNAME = n("MISC_SUPREMESHOPS_EDITGIVEITEMSNAME",
			"en_US", l("&aGiven objects"),
			"fr_FR", l("&aObjets donnés"),
			"zh_CN", l("&aGiven objects"),
			"zh_TW", l("&a(你->買家 的東西)"),
			"ru_RU", l("&aОтданны предметы"),
			"hu_HU", l("&aAdott tárgyak"),
			"es_ES", l("&aObjetos dados")
			);

	public static final Text MISC_SUPREMESHOPS_EDITGIVEITEMSLORE = n("MISC_SUPREMESHOPS_EDITGIVEITEMSLORE",
			"en_US", l("&7Edit objects given to the buyer during", "&7 the trade, or add/withdraw stock"),
			"fr_FR", l("&7Éditer les objets donnés à l'acheteur", "&7 durant l'échange, ou ajouter/retirer du stock"),
			"zh_CN", l("&7Edit objects given to the buyer during", "&7 the trade, or add/withdraw stock"),
			"zh_TW", l("&7編輯在交易中商店給玩家的物件", "&7或是 補貨/取回 庫存"),
			"ru_RU", l("&7Изменить отданные покупателю предметы", "&7или добавить\\убрать складские остатки"),
			"hu_HU", l("&7Szerkesztheti a vevőnek a kereskedelem során adott", "&7 tárgyakat, vagy hozzáadhat/vehet ki készleteket"),
			"es_ES", l("&7Editar los objetos dados al comprador durante el", "&7intercambio, o añadir/retirar stock")
			);

	public static final Text MISC_SUPREMESHOPS_EDITGIVEITEMLORE = n("MISC_SUPREMESHOPS_EDITGIVEITEMLORE",
			"en_US", l("&7Stock : &a{stock}"),
			"fr_FR", l("&7Stock : &a{stock}"),
			"zh_CN", l("&7Stock : &a{stock}"),
			"zh_TW", l("&7當前庫存數量: &a{stock}"),
			"ru_RU", l("&7Остаток : &a{stock}"),
			"hu_HU", l("&7Raktár: &a{stock}"),
			"es_ES", l("&7Stock : &a{stock}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONTROLRESTOCKLORE = n("MISC_SUPREMESHOPS_EDITCONTROLRESTOCKLORE",
			"en_US", l("&dLeft-click to add stock"),
			"fr_FR", l("&dClic-gauche pour ajouter du stock"),
			"zh_CN", l("&dLeft-click to add stock"),
			"zh_TW", l("&f左鍵點擊 &7增加庫存"),
			"ru_RU", l("&dЛКМ, чтобы добавить складские остатки"),
			"hu_HU", l("&dBal-klikk, hogy hozzáadd a raktárhoz"),
			"es_ES", l("&dClic izquierdo para añadir stock")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONTROLWITHDRAWLORE = n("MISC_SUPREMESHOPS_EDITCONTROLWITHDRAWLORE",
			"en_US", l("&dRight-click to withdraw stock"),
			"fr_FR", l("&dClic-droit pour récupérer du stock"),
			"zh_CN", l("&dRight-click to withdraw stock"),
			"zh_TW", l("&f右鍵點擊 &7取出所有的庫存"),
			"ru_RU", l("&dПКМ, чтобы забрать складские остатки"),
			"hu_HU", l("&dJobb-klikk, hogy kivedd a raktárkészletet"),
			"es_ES", l("&dClic derecho para retirar stock")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE = n("MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE",
			"en_US", l("&dShift + right-click to remove object"),
			"fr_FR", l("&dShift + clic-droit pour supprimer l'objet"),
			"zh_CN", l("&dShift + right-click to remove object"),
			"zh_TW", l("&fSHIFT+右鍵點擊 &c移除這個物件"),
			"ru_RU", l("&dShift + ПКМ, чтобы удалить предмет"),
			"hu_HU", l("&dShift + jobb-klikk, hogy eltávolítsd a tárgyat"),
			"es_ES", l("&dShift + Clic derecho para eliminar un objeto")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONTROLREMOVECONDITIONLORE = n("MISC_SUPREMESHOPS_EDITCONTROLREMOVECONDITIONLORE",
			"en_US", l("&dShift + right-click to remove condition"),
			"fr_FR", l("&dShift + clic-droit pour supprimer la condition"),
			"zh_CN", l("&dShift + right-click to remove condition"),
			"zh_TW", l("&fSHIFT+右鍵點擊 &c移除這個條件"),
			"ru_RU", l("&dShift + ПКМ, чтобы удалить условие"),
			"hu_HU", l("&dShift + jobb-klikk, hogy eltávolítsd a feltételeket"),
			"es_ES", l("&dShift + Clic derecho para eliminar una condición")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONTROLEDITCONDITIONERRORMESSAGELORE = n("MISC_SUPREMESHOPS_EDITCONTROLEDITCONDITIONERRORMESSAGELORE",
			"en_US", l("&dRight-click to change error message"),
			"fr_FR", l("&dClic-droit pour changer le message d'erreur"),
			"zh_CN", l("&dRight-click to change error message"),
			"zh_TW", l("&f右鍵點擊 &7更改錯誤訊息"),
			"ru_RU", l("&dПКМ, чтобы изменить сообщение об ошибке"),
			"hu_HU", l("&dJobb-klikk, hogy megváltoztasd a hibaüzenetet"),
			"es_ES", l("&dClic derecho para cambiar el mensaje de error")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPEDITMANAGERLORE = n("MISC_SUPREMESHOPS_SHOPEDITMANAGERLORE",
			"en_US", l("&dLeft-click to edit permissions", "&dShift + left-click to copy permissions", "&dRight-click to edit wage", "&dShift + right-click to remove manager"),
			"fr_FR", l("&dClic-gauche pour éditer les permissions", "&dShift + left-click pour copier les permissions", "&dClic-droit pour éditer le salaire", "&dShift + clic-droit pour supprimer le manager"),
			"zh_CN", l("&dLeft-click to edit permissions", "&dShift + left-click to copy permissions", "&dRight-click to edit wage", "&dShift + right-click to remove manager"),
			"zh_TW", l("&f左鍵點擊 &7編輯權限", "&fSHIFT+左鍵點擊 &7複製權限", "&f右鍵點擊 &7編輯薪資", "&fSHIFT+右鍵點擊 &c移除這位管理者"),
			"ru_RU", l("&dЛКМ, чтобы редактировать права", "&dShift + ЛКМ, чтобы копировать права", "&dПКМ, чтобы редактировать ЗП", "&dShift + ПКМ, чтобы уволить менеджера"),
			"hu_HU", l("&dBal-klikk, hogy szerkeszd a jogokat", "&dShift + bal-klikk, hogy másold a jogokat", "&dJobb-klikk, hogy szerkeszd a béreket", "&dShift + jobb-klikk, hogy eltávolítsd a kezelőt"),
			"es_ES", l("&dClic izquierdo para editar los permisos", "&dShift + clic izquierdo para copiar permisos", "&dClic derecho para editar el salario", "&dShift + clic derecho para eliminar managers")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITMANAGERLORE = n("MISC_SUPREMESHOPS_MERCHANTEDITMANAGERLORE",
			"en_US", l("&dLeft-click to edit permissions", "&dShift + left-click to copy permissions", "&dRight-click to edit wage", "&dShift + right-click to remove manager"),
			"fr_FR", l("&dClic-gauche pour éditer les permissions", "&dShift + left-click pour copier les permissions", "&dClic-droit pour éditer le salaire", "&dShift + clic-droit pour supprimer le manager"),
			"zh_CN", l("&dLeft-click to edit permissions", "&dShift + left-click to copy permissions", "&dRight-click to edit wage", "&dShift + right-click to remove manager"),
			"zh_TW", l("&f左鍵點擊 &7編輯權限", "&fSHIFT+左鍵點擊 &7複製權限", "&f右鍵點擊 &7編輯薪資", "&fSHIFT+右鍵點擊 &c移除這位管理者"),
			"ru_RU", l("&dЛКМ, чтобы редактировать права", "&dShift + ЛКМ, чтобы копировать права", "&dПКМ, чтобы редактировать ЗП", "&dShift + ПКМ, чтобы уволить менеджера"),
			"hu_HU", l("&dBal-klikk, hogy szerkeszd a jogokat", "&dShift + bal-klikk, hogy másold a jogokat", "&dJobb-klikk, hogy szerkeszd a béreket", "&dShift + jobb-klikk, hogy eltávolítsd a kezelőt"),
			"es_ES", l("&dClic izquierdo para editar los permisos", "&dShift + clic izquierdo para copiar permisos", "&dClic derecho para editar el salario", "&dShift + clic derecho para eliminar managers")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITCONTROLTOGGLEPERMISSIONLORE = n("MISC_SUPREMESHOPS_MANAGEABLEEDITCONTROLTOGGLEPERMISSIONLORE",
			"en_US", l("&dRight-click to toggle permission"),
			"fr_FR", l("&dClic-droit pour basculer la permission"),
			"zh_TW", l("&f右鍵點擊 &7切換權限"),
			"ru_RU", l("&dПКМ, чтобы вкл\\выкл права"),
			"hu_HU", l("&dJobb-klikk, hogy válts jogot"),
			"es_ES", l("&dClic derecho para cambiar el permiso")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTEDITCONTROLTOGGLESTATUSLORE = n("MISC_SUPREMESHOPS_MERCHANTEDITCONTROLTOGGLESTATUSLORE",
			"en_US", l("&dRight-click to toggle status"),
			"fr_FR", l("&dClic-droit pour basculer le status"),
			"zh_CN", l("&dRight-click to toggle status"),
			"zh_TW", l("&f右鍵點擊 &7切換狀態"),
			"ru_RU", l("&dПКМ, чтобы вкл\\выкл статус"),
			"hu_HU", l("&dJobb-klikk, hogy válts státuszt"),
			"es_ES", l("&dClic derecho para cambiar el estado")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPDESTROYNAME = n("MISC_SUPREMESHOPS_SHOPDESTROYNAME",
			"en_US", l("&4Destroy shop"),
			"fr_FR", l("&4Détruire le shop"),
			"zh_CN", l("&4Destroy shop"),
			"zh_TW", l("&4摧毀商店"),
			"ru_RU", l("&4Уничтожить магазин"),
			"hu_HU", l("&4Bolt lerombolása"),
			"es_ES", l("&4Destruir tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPDESTROYLORE = n("MISC_SUPREMESHOPS_SHOPDESTROYLORE",
			"en_US", l("&7Definitely destroy this shop. You'll", "&7 receive the stocked benefits and/or it'll", "&7 be dropped on the ground. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"fr_FR", l("&7Définitivement supprimer ce shop. Vous", "&7 recevrez les bénéfices stockés et/ou ils", "&7 seront droppés au sol. Cette action est irréversible.", "&7Un GUI de confirmation sera ouverte au clic"),
			"zh_CN", l("&7Definitely destroy this shop. You'll", "&7 receive the stocked benefits and/or it'll", "&7 be dropped on the ground. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"zh_TW", l("&7徹底的摧毀這間商店", "&7將會返還商店內的 庫存/物品", "&7到你的包包內或是直接掉落在地上", "&c[注意]這個動作不可復原", "&7(點擊後將會進行二次確認)"),
			"ru_RU", l("&7Уничтожить магазин. ", "&7Вы получите предметы со склада назад, но", "&7если у вас нет места, то они окажутся на земле.", "&7Данное действие необратимо.", "&7Меню подтверждения откроется по нажатию."),
			"hu_HU", l("&7Megsemmisíti a boltot. Megkapod a készletet", "&7 vagy a földre lesz dobva. Ez visszafordíthatatlan!", "&7Erre kattintva egy megerősítő GUI nyílik meg."),
			"es_ES", l("&7Destruir definitivamente esta tienda. Si lo haces", "&7recibirás los beneficios en stock y/o se dropearán", "&7en el suelo. Esta acción es irreversible.", "&7Una GUI de confirmación se abrirá al hacer clic")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTDESTROYNAME = n("MISC_SUPREMESHOPS_MERCHANTDESTROYNAME",
			"en_US", l("&4Destroy merchant"),
			"fr_FR", l("&4Détruire le marchand"),
			"zh_CN", l("&4Destroy merchant"),
			"zh_TW", l("&4摧毀商人"),
			"ru_RU", l("&4Уничтожить торговца"),
			"hu_HU", l("&4Kereskedő lerombolása"),
			"es_ES", l("&4Destruir mercader")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTDESTROYLORE = n("MISC_SUPREMESHOPS_MERCHANTDESTROYLORE",
			"en_US", l("&7Definitely destroy this merchant. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"fr_FR", l("&7Définitivement supprimer ce marchand. Cette action est irréversible.", "&7Un GUI de confirmation sera ouverte au clic"),
			"zh_CN", l("&7Definitely destroy this merchant. This action is irreversible.", "&7A confirmation GUI will be opened on click"),
			"zh_TW", l("&7徹底的摧毀這個商人", "&c[注意]這個動作不可復原", "&7(點擊後將會進行二次確認)"),
			"ru_RU", l("&7Уничтожает торговца. Это действие необратимо.", "&7Меню подтверждения откроется по нажатию."),
			"hu_HU", l("&7Megsemmisíti a kereskedőt. Ez visszafordíthatatlan!", "&7Egy megerősítő GUI nyílik meg, ha ide kattolsz"),
			"es_ES", l("&7Destruir definitivamente este mercader. Esta acción es irreversible.", "&7Una GUI de confirmación se abrirá después de darle clic")
			);

	public static final Text MISC_SUPREMESHOPS_EDITTAKEITEMLORE = n("MISC_SUPREMESHOPS_EDITTAKEITEMLORE",
			"en_US", l("&7Benefits in stock : &a{stock}"),
			"fr_FR", l("&7Bénéfices en stock : &a{stock}"),
			"zh_CN", l("&7Benefits in stock : &a{stock}"),
			"zh_TW", l("&7收益數量: &a{stock}"),
			"ru_RU", l("&7В наличии : &a{stock}"),
			"hu_HU", l("&7Haszon a raktáron: &a{stock}"),
			"es_ES", l("&7Beneficios en stock : &a{stock}")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEITEMLORE = n("MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEITEMLORE",
			"en_US", l("&7In stock : &a{stock}"),
			"fr_FR", l("&7En stock : &a{stock}"),
			"zh_TW", l("&7庫存數量: &a{stock}"),
			"ru_RU", l("&7В наличии : &a{stock}"),
			"hu_HU", l("&7Raktáron: &a{stock}"),
			"es_ES", l("&7En stock : &a{stock}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONDITIONLORE = n("MISC_SUPREMESHOPS_EDITCONDITIONLORE",
			"en_US", l("&7Condition : &a{description}"),
			"fr_FR", l("&7Condition : &a{description}"),
			"zh_TW", l("&7條件: &a{description}"),
			"ru_RU", l("&7Условие : &a{description}"),
			"hu_HU", l("&7Feltételek: &a{description}"),
			"es_ES", l("&7Condición : &a{description}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDSHOPNAME = n("MISC_SUPREMESHOPS_EDITADDSHOPNAME",
			"en_US", l("&6Add a shop"),
			"fr_FR", l("&6Ajouter un shop"),
			"zh_CN", l("&6Add a shop"),
			"zh_TW", l("&6新增一個商店"),
			"ru_RU", l("&6Добавить магазин"),
			"hu_HU", l("&6Adj hozzá egy üzletet"),
			"es_ES", l("&6Añadir una tienda")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPNAME = n("MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPNAME",
			"en_US", l("&6Create a shop with this merchant"),
			"fr_FR", l("&6Créer un shop avec ce marchand"),
			"zh_TW", l("&6在這個商人中創建一間商店"),
			"ru_RU", l("&6Создать магазин с этим торговцем"),
			"hu_HU", l("&6Készíts el egy üzletet ezzel a kereskedővel"),
			"es_ES", l("&6Crear una tienda con este mercader")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPLORE = n("MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPLORE",
			"en_US", l("&7Unlike linked shops, created merchants belong", "&7 specifically to this merchant and will", "&7 be destroyed if the merchant is destroyed"),
			"fr_FR", l("&7Contrairement aux shops liés, les marchands créés", "&7 appartiennent spécifiquement au marchand et seront", "&7 détruits si le marchand est détruit"),
			"zh_TW", l("&7與鏈接商店不同，創建的商人只屬於屬於這個商人", "&7如果摧毀該商人，也會將這個商人一起摧毀"),
			"ru_RU", l("&7Магазины, созданные с этим торговцем", "&7будут удалены при его уничтожении.", "&7Чего никогда не произойдёт с привязанными."),
			"hu_HU", l("&7Leválasztja a hozzákapcsolt üzleteket,", "&7A létrehozott üzletek ehhez a kereskedőhöz", "&7 tartoznak, így a kereskedő megsemmisítése", "&7 eltávolítja az összes üzletet is"),
			"es_ES", l("&7A diferencia de las tiendas linkeadas, los mercaderes creados", "&7pertenecen específicamente a este mercader y serán destruidos", "&7si el mercader se elimina o se destruye")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDOBJECTNAME = n("MISC_SUPREMESHOPS_EDITADDOBJECTNAME",
			"en_US", l("&6Add an object"),
			"fr_FR", l("&6Ajouter un object"),
			"zh_CN", l("&6Add an object"),
			"zh_TW", l("&6新增一個物件"),
			"ru_RU", l("&6Добавить предмет"),
			"hu_HU", l("&6Tárgy hozzáadása"),
			"es_ES", l("&6Añadir un objeto")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDCONDITIONNAME = n("MISC_SUPREMESHOPS_EDITADDCONDITIONNAME",
			"en_US", l("&6Add a condition"),
			"fr_FR", l("&6Ajouter une condition"),
			"zh_CN", l("&6Add a condition"),
			"zh_TW", l("&6新增一個條件"),
			"ru_RU", l("&6Добавить условие"),
			"hu_HU", l("&6Feltétel hozzáadása"),
			"es_ES", l("&6Añadir una condición")
			);

	public static final Text MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSNAME = n("MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSNAME",
			"en_US", l("&6Edit required valid conditions amount"),
			"fr_FR", l("&6Éditer le nombre de conditions valides requises"),
			"zh_CN", l("&6Edit required valid conditions amount"),
			"zh_TW", l("&6編輯要求的有效條件數量"),
			"ru_RU", l("&6Редактировать количество нужных условий"),
			"hu_HU", l("&6Szerkeszd a szükséges érvényes feltételek mennyiségét"),
			"es_ES", l("&6Editar el número de condiciones válidas requeridas")
			);

	public static final Text MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSLORE = n("MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSLORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_CN", l("&7Current : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenlegi: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSNAME = n("MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSNAME",
			"en_US", l("&6Edit required not valid conditions amount"),
			"fr_FR", l("&6Éditer le nombre de conditions non valides requises"),
			"zh_CN", l("&6Edit required not valid conditions amount"),
			"zh_TW", l("&6編輯要求的無效條件數量"),
			"ru_RU", l("&6Редактировать количество ненужных условий"),
			"hu_HU", l("&6Szerkeszd a nem-érvényes feltételek mennyiségét"),
			"es_ES", l("&6Editar la cantidad de condiciones requeridas no válidas")
			);

	public static final Text MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSLORE = n("MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSLORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_CN", l("&7Current : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenlegi: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGENAME = n("MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGENAME",
			"en_US", l("&6Edit conditions general error message"),
			"fr_FR", l("&6Éditer le message d'erreur général des conditions"),
			"zh_CN", l("&6Edit conditions general error message"),
			"zh_TW", l("&6編輯未達條件時顯示的錯誤訊息"),
			"ru_RU", l("&6Редактировать основное сообщение об ошибке"),
			"hu_HU", l("&6A feltételek általános hibaüzenetének szerkesztése"),
			"es_ES", l("&6Editar el mensaje de error de las condiciones generales")
			);

	public static final Text MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGELORE = n("MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGELORE",
			"en_US", l("&7Current : &a{current}"),
			"fr_FR", l("&7Actuellement : &a{current}"),
			"zh_CN", l("&7Current : &a{current}"),
			"zh_TW", l("&f目前: &a{current}"),
			"ru_RU", l("&7Сейчас : &a{current}"),
			"hu_HU", l("&7Jelenlegi: &a{current}"),
			"es_ES", l("&7Actualmente : &a{current}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITWITHDRAWALLOBJECTNAME = n("MISC_SUPREMESHOPS_EDITWITHDRAWALLOBJECTNAME",
			"en_US", l("&6Withdraw all stock"),
			"fr_FR", l("&6Retirer tout le stock"),
			"zh_CN", l("&6Withdraw all stock"),
			"zh_TW", l("&6一鍵取回"),
			"ru_RU", l("&6Забрать все остатки"),
			"hu_HU", l("&6Minden raktárkészlet kivétele"),
			"es_ES", l("&6Sacar todo el stock")
			);

	public static final Text MISC_SUPREMESHOPS_EDITWITHDRAWALLBENEFITSLORE = n("MISC_SUPREMESHOPS_EDITWITHDRAWALLBENEFITSLORE",
			"en_US", l("&7Take as much stock as possible in", "&7 all the available objects", "", "&dLeft-click to withdraw all for this shop (benefits)", "&dRight-click to withdraw all for this shop and others (benefits)"),
			"fr_FR", l("&7Prendre autant de stock que possible", "&7 dans tous les objets disponibles", "", "&dClic-gauche pour récupérer tout pour ce shop (bénéfices)", "&dClic-droit pour récupérer tout pour ce shop et d'autres (bénéfices)"),
			"zh_CN", l("&7Take as much stock as possible in", "&7 all the available objects", "", "&dLeft-click to withdraw all for this shop (benefits)", "&dRight-click to withdraw all for this shop and others (benefits)"),
			"zh_TW", l("&7取出所有現存物件中的物品", "", "&f左鍵點擊 &a取回這間商店所有 &6(買家 -> 你) &a的物品", "&f右鍵點擊 &a取回這間和其他間商店所有 &6(買家 -> 你) &a的物品"),
			"ru_RU", l("&7Забрать столько, сколько возможно", "&7во всех магазинах", "", "&dЛКМ, чтобы забрать всё из этого магазина", "&dПКМ, чтобы забрать всё из всех магазинов"),
			"hu_HU", l("&7Vedd ki az összes lehetséges készletet", "&7 minden elérhető tárgyból", "", "&dbal-klikk, hogy kivegyél mindent ebből az üzletből (haszon)", "&dJobb-klikk, hogy kivegyél mindent az összes üzletből (haszon)"),
			"es_ES", l("&7Llevar la cantidad máxima posible de stock de", "&7todos los objetos disponibles", "", "&dClic izquierdo para sacar todo el beneficio posible de esta tienda", "&dClic derecho para sacar todo el beneficio posible de esta y otras tiendas")
			);

	public static final Text MISC_SUPREMESHOPS_EDITWITHDRAWALLSTOCKLORE = n("MISC_SUPREMESHOPS_EDITWITHDRAWALLSTOCKLORE",
			"en_US", l("&7Take as much stock as possible in", "&7 all the available objects", "", "&dLeft-click to withdraw all for this shop (stock)", "&dRight-click to withdraw all for this shop and others (stock)"),
			"fr_FR", l("&7Prendre autant de stock que possible", "&7 dans tous les objets disponibles", "", "&dClic-gauche pour récupérer tout pour ce shop (stock)", "&dClic-droit pour récupérer tout pour ce shop et d'autres (stock)"),
			"zh_CN", l("&7Take as much stock as possible in", "&7 all the available objects", "", "&dLeft-click to withdraw all for this shop (stock)", "&dRight-click to withdraw all for this shop and others (stock)"),
			"zh_TW", l("&7取出所有現存物件中的物品", "", "&f左鍵點擊 &a取回這間商店所有 &6(你 -> 買家) &a的物品", "&f左鍵點擊 &a取回這間和其他間商店所有 &6(你  -> 買家) &a的物品"),
			"ru_RU", l("&7Забрать столько, сколько возможно", "&7во всех магазинах", "", "&dЛКМ, чтобы забрать всё из этого магазина", "&dПКМ, чтобы забрать всё из всех магазинов"),
			"hu_HU", l("&7Vedd ki az összes lehetséges készletet", "&7 minden elérhető tárgyból", "", "&dbal-klikk, hogy kivegyél mindent ebből az üzletből (készlet)", "&dJobb-klikk, hogy kivegyél mindent az összes üzletből (készlet)"),
			"es_ES", l("&7Coger todo el stock posible en todos los objetos", "&7disponibles", "", "&dClic izquierdo para sacarlo todo de esta tienda (stock)", "&dClic derecho para sacarlo todo de esta y otras tiendas (stock)")
			);

	public static final Text MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTNAME = n("MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTNAME",
			"en_US", l("&6Restock all"),
			"fr_FR", l("&6Restocker tout"),
			"zh_CN", l("&6Restock all"),
			"zh_TW", l("&6一鍵補貨"),
			"ru_RU", l("&6Пополнить запасы всех магазинов"),
			"hu_HU", l("&6Elraktároz mindent"),
			"es_ES", l("&6Restockear todo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTLORE = n("MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTLORE",
			"en_US", l("&7Restock as much as possible", "", "&dLeft-click to restock all for this shop", "&dRight-click to restock all for this shop and others"),
			"fr_FR", l("&7Restocker autant que possible", "", "&dClic-gauche pour restocker tout pour ce shop", "&dClic-droit pour restocker tout pour ce shop et d'autres"),
			"zh_CN", l("&7Restock as much as possible", "", "&dLeft-click to restock all for this shop", "&dRight-click to restock all for this shop and others"),
			"zh_TW", l("&7盡可能的補貨", "", "&f左鍵點擊 &a替這間商店補貨", "&f左鍵點擊 &a替這間和其他間商店補貨"),
			"ru_RU", l("&7Пополнить все, что можно", "", "&dЛКМ, чтобы пополнить всё в этом магазине", "&dПКМ, чтобы пополнить всё во всех ваших магазинах"),
			"hu_HU", l("&7Töltd fel az összes lehetséges mennyiséget", "", "&dBal-klikk, hogy feltölts mindent ebbe az üzletbe", "&dJobb-klikk, hogy feltölts mindent ehhez az üzlethez és máshoz")
			);

	public static final Text MISC_SUPREMESHOPS_MANAGEABLEEDITADDMANAGERLORE = n("MISC_SUPREMESHOPS_MANAGEABLEEDITADDMANAGERLORE",
			"en_US", l("&6Add a manager"),
			"fr_FR", l("&6Ajouter un manager"),
			"zh_TW", l("&6新增一個管理者"),
			"ru_RU", l("&6Добавить менеджера"),
			"hu_HU", l("&6Kezelő hozzáadása"),
			"es_ES", l("&6Añadir un manager")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDOBJECTSELECTTYPE = n("MISC_SUPREMESHOPS_EDITADDOBJECTSELECTTYPE",
			"en_US", l("Select type"),
			"fr_FR", l("Sélectionner type"),
			"zh_CN", l("Select type"),
			"zh_TW", l("選擇類型"),
			"ru_RU", l("Выбрать тип"),
			"hu_HU", l("Típus kiválasztása"),
			"es_ES", l("Seleccionar tipo")
			);

	public static final Text MISC_SUPREMESHOPS_EDITRENTPRICEPERIODSELECTTYPE = n("MISC_SUPREMESHOPS_EDITRENTPRICEPERIODSELECTTYPE",
			"en_US", l("Select period"),
			"fr_FR", l("Sélectionner fréquence"),
			"zh_TW", l("選擇週期"),
			"ru_RU", l("Выбрать период"),
			"hu_HU", l("Időszak kiválasztása"),
			"es_ES", l("Seleccionar período")
			);

	public static final Text MISC_SUPREMESHOPS_EDITADDCONDITIONSELECTTYPE = n("MISC_SUPREMESHOPS_EDITADDCONDITIONSELECTTYPE",
			"en_US", l("Select type"),
			"fr_FR", l("Sélectionner type"),
			"zh_CN", l("Select type"),
			"zh_TW", l("選擇類型"),
			"ru_RU", l("Выбрать тип"),
			"hu_HU", l("Típus kiválasztása"),
			"es_ES", l("Seleccionar tipo")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYNAME = n("MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYNAME",
			"en_US", l("&aBy name (shop)"),
			"fr_FR", l("&aPar nom (shop)"),
			"zh_CN", l("&aBy name (shop)"),
			"zh_TW", l("&a依照名子 &f(商店)"),
			"ru_RU", l("&aПо имени (магазины)"),
			"hu_HU", l("&aNév szerint (üzlet)"),
			"es_ES", l("&aPor nombre (tienda)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYRANKINGSERVER = n("MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYRANKINGSERVER",
			"en_US", l("&aBy ranking (shop) (server)"),
			"fr_FR", l("&aPar ranking (shop) (serveur)"),
			"zh_CN", l("&aBy ranking (shop) (server)"),
			"zh_TW", l("&a依照階級  &f(商店) (伺服器)"),
			"ru_RU", l("&aПо рангу (магазин) (сервер)"),
			"hu_HU", l("&aRangsorolás szerint (bolt) (szerver)"),
			"es_ES", l("&aPor ranking (tienda) (servidor)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYRANKINGSELLER = n("MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYRANKINGSELLER",
			"en_US", l("&aBy ranking (shop) (seller)"),
			"fr_FR", l("&aPar ranking (shop) (vendeur)"),
			"zh_CN", l("&aBy ranking (shop) (seller)"),
			"zh_TW", l("&a依照階級 &f(商店) (賣家)"),
			"ru_RU", l("&aПо рангу (магазин) (продавец)"),
			"hu_HU", l("&aRangsorolás szerint (bolt) (eladó)"),
			"es_ES", l("&aPor ranking (tienda) (vendedor)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYUNIQUEBUYERS = n("MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIABYUNIQUEBUYERS",
			"en_US", l("&aBy unique buyers (shop)"),
			"fr_FR", l("&aPar nombre d'acheteurs uniques (shop)"),
			"zh_CN", l("&aBy unique buyers (shop)"),
			"zh_TW", l("&a依照不重複買家 &f(商店)"),
			"ru_RU", l("&aПо уникальным покупателям (магазин)"),
			"hu_HU", l("&aVásárlók szerint (bolt)"),
			"es_ES", l("&aPor compradores únicos (tienda)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYNAME = n("MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYNAME",
			"en_US", l("&aBy name (merchant)"),
			"fr_FR", l("&aPar nom (marchand)"),
			"zh_CN", l("&aBy name (merchant)"),
			"zh_TW", l("&a依照名子 &f(商人)"),
			"ru_RU", l("&aПо имени (торговцы)"),
			"hu_HU", l("&aNév szerint (kereskedő)"),
			"es_ES", l("&aPor nombre (mercader)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSERVER = n("MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSERVER",
			"en_US", l("&aBy ranking (merchant) (server)"),
			"fr_FR", l("&aPar ranking (merchant) (serveur)"),
			"zh_CN", l("&aBy ranking (merchant) (server)"),
			"zh_TW", l("&a依照階級  &f(商人) (伺服器)"),
			"ru_RU", l("&aПо рангу (торговец) (сервер)"),
			"hu_HU", l("&aRangsorolás szerint (kereskedő) (szerver)"),
			"es_ES", l("&aPor ranking (mercader) (server)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSELLER = n("MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSELLER",
			"en_US", l("&aBy ranking (merchant) (seller)"),
			"fr_FR", l("&aPar ranking (merchant) (vendeur)"),
			"zh_CN", l("&aBy ranking (merchant) (seller)"),
			"zh_TW", l("&a依照階級 &f(商人) (賣家)"),
			"ru_RU", l("&aПо рангу (торговец) (продавец)"),
			"hu_HU", l("&aRangsorolás szerint (kereskedő) (eladó)"),
			"es_ES", l("&aPor ranking (mercader) (vendedor)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYUNIQUEBUYERS = n("MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYUNIQUEBUYERS",
			"en_US", l("&aBy unique buyers (merchant)"),
			"fr_FR", l("&aPar nombre d'acheteurs uniques (merchant)"),
			"zh_CN", l("&aBy unique buyers (merchant)"),
			"zh_TW", l("&a依照不重複買家 &f(商人)"),
			"ru_RU", l("&aПо уникальным покупателям (торговец)"),
			"hu_HU", l("&aVásárlók szerint (kereskedő)"),
			"es_ES", l("&aPor compradores únicos (mercader)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIALORE = n("MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIALORE",
			"en_US", l("&dLeft-click : sort by name (shop)", "&dRight-click : sort by unique buyers (shop)", "&dShift + left-click : sort by ranking (shop) (server)", "&dShift + right-click : sort by ranking (shop) (seller)"),
			"fr_FR", l("&dClic-gauche : trier par nom (shop)", "&dClic-droit : trier par acheteurs uniques (shop)", "&dShift + clic-gauche : trier par ranking (shop) (serveur)", "&dShift + clic-droit : trier par ranking (shop) (vendeur)"),
			"zh_CN", l("&dLeft-click : sort by name (shop)", "&dRight-click : sort by unique buyers (shop)", "&dShift + left-click : sort by ranking (shop) (server)", "&dShift + right-click : sort by ranking (shop) (seller)"),
			"zh_TW", l("&f左鍵點擊 &7依照名子排列 &f(商店)", "&f右鍵點擊 &7依照不重複買家排列 &f(商店)", "&fSHIFT+左鍵點擊 &7依照階級排列 &f(商店) (伺服器)", "&fSHIFT+右鍵點擊 &7依照階級排列 &f(商店) (賣家)"),
			"ru_RU", l("&dЛКМ : сортировка по имени (магазин)", "&dПКМ : по уникальным покупателям (магазин)", "&dShift + ЛКМ : по рангу (магазин) (сервер)", "&dShift + ПКМ : по рангу (магазин) (продавец)"),
			"hu_HU", l("&dBal-klikk : Szűrés névre (bolt)", "&dJobb-klikk : szűrés vásárlóra (bolt)", "&dShift + bal-klikk: szűrés értékelésre (bolt) (szerver)", "&dShift + jobb-klikk: szűrés értékelésre (bolt) (eladó)"),
			"es_ES", l("&dClic izquierdo : ordenar por nombre (tienda)", "&dClic derecho : ordenar por compradores únicos (tienda)", "&dShift + clic izquierdo : ordenar por ranking (tienda) (server)", "&dShift + clic derecho : ordenar por ranking (tienda) (vendedor)")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIALORE = n("MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIALORE",
			"en_US", l("&dLeft-click : sort by name (merchant)", "&dRight-click : sort by unique buyers (merchant)", "&dShift + left-click : sort by ranking (merchant) (server)", "&dShift + right-click : sort by ranking (merchant) (seller)"),
			"fr_FR", l("&dClic-gauche : trier par nom (marchand)", "&dClic-droit : trier par acheteurs uniques (marchand)", "&dShift + clic-gauche : trier par ranking (marchand) (serveur)", "&dShift + clic-droit : trier par ranking (marchand) (vendeur)"),
			"zh_CN", l("&dLeft-click : sort by name (merchant)", "&dRight-click : sort by unique buyers (merchant)", "&dShift + left-click : sort by ranking (merchant) (server)", "&dShift + right-click : sort by ranking (merchant) (seller)"),
			"zh_TW", l("&f左鍵點擊 &7依照名子排列 &f(商人)", "&f右鍵點擊 &7依照不重複買家排列 &f(商人)", "&fSHIFT+左鍵點擊 &7依照階級排列 &f(商人) (伺服器)", "&fSHIFT+右鍵點擊 &7依照階級排列 &f(商人) (賣家)"),
			"ru_RU", l("&dЛКМ : сортировка по имени (торговец)", "&dПКМ : по уникальным покупателям (магазин)", "&dShift + ЛКМ : по рангу (торговец) (сервер)", "&dShift + ПКМ : по рангу (торговец) (продавец)"),
			"hu_HU", l("&dBal-klikk : Szűrés névre (kereskedő)", "&dJobb-klikk : szűrés vásárlóra (kereskedő)", "&dShift + bal-klikk: szűrés értékelésre (kereskedő) (szerver)", "&dShift + jobb-klikk: szűrés értékelésre (kereskedő) (eladó)"),
			"es_ES", l("&dClic izquierdo : ordenar por nombre (mercader)", "&dClic derecho : ordenar por compradores únicos (mercader)", "&dShift + clic izquierdo : ordenar por ranking (mercader) (server)", "&dShift + clic derecho : ordenar por ranking (mercader) (vendedor)")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTLORECONTROLTELEPORT = n("MISC_SUPREMESHOPS_SHOPLISTLORECONTROLTELEPORT",
			"en_US", l("&dLeft-click to teleport to this shop"),
			"fr_FR", l("&dClic-gauche pour se téléporter à ce shop"),
			"zh_CN", l("&dLeft-click to teleport to this shop"),
			"zh_TW", l("&f左鍵點擊 &7傳送至這間商店"),
			"ru_RU", l("&dЛКМ для телепортации к этому магазину"),
			"hu_HU", l("&dBal-klikk, hogy teleportálj ehhez a bolthoz"),
			"es_ES", l("&dClic izquierdo para teletransportarte a esta tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDTRADE = n("MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDTRADE",
			"en_US", l("&dLeft-click to preview trade and trade with this shop"),
			"fr_FR", l("&dClic-gauche pour prévisualiser l'échanger et échanger avec ce shop"),
			"zh_CN", l("&dLeft-click to preview trade and trade with this shop"),
			"zh_TW", l("&f左鍵點擊 &a預覽與這間商店交易"),
			"ru_RU", l("&dЛКМ для превью и обмена"),
			"hu_HU", l("&dBal-klikk, hogy előnézd, és kereskedj ezzel a bolttal"),
			"es_ES", l("&dClic izquierdo para previsualizar el comercio y comerciar con esta tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDEDIT = n("MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDEDIT",
			"en_US", l("&dLeft-click to preview trade and edit this shop"),
			"fr_FR", l("&dClic-gauche pour prévisualiser l'échanger et éditer ce shop"),
			"zh_CN", l("&dLeft-click to preview trade and edit this shop"),
			"zh_TW", l("&f左鍵點擊 &a預覽交易和編輯商店"),
			"ru_RU", l("&dЛКМ для превью и редактирования"),
			"hu_HU", l("&dBal-klikk, hogy előnézd a kereskedelmet és szerkeszd a boltot"),
			"es_ES", l("&dClic izquierdo para previsualizar el comercio y editar esta tienda")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLTELEPORT = n("MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLTELEPORT",
			"en_US", l("&dLeft-click to teleport to this merchant"),
			"fr_FR", l("&dClic-gauche pour se téléporter à ce marchand"),
			"zh_CN", l("&dLeft-click to teleport to this merchant"),
			"zh_TW", l("&f左鍵點擊 &7傳送至這個商人"),
			"ru_RU", l("&dЛКМ для телепортации к этому торговцу"),
			"hu_HU", l("&dBal-klikk, hogy teleportálj ehhez a kereskedőhöz"),
			"es_ES", l("&dClic izquierdo para teletransportarse a este mercader")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLPREVIEWANDTRADE = n("MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLPREVIEWANDTRADE",
			"en_US", l("&dLeft-click to preview and trade with shops"),
			"fr_FR", l("&dClic-gauche pour prévisualiser et échanger avec les shops"),
			"zh_CN", l("&dLeft-click to preview and trade with shops"),
			"zh_TW", l("&f左鍵點擊 &a預覽與這間商店交易"),
			"ru_RU", l("&dЛКМ для превью и обмена"),
			"hu_HU", l("&dBal-klikk, hogy előnézd, és kereskedj a bolttal"),
			"es_ES", l("&dClic izquierdo para previsualizar y comerciar con tiendas")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLPREVIEWANDEDIT = n("MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLPREVIEWANDEDIT",
			"en_US", l("&dLeft-click to preview and edit shops and merchant"),
			"fr_FR", l("&dClic-gauche pour prévisualiser et éditer les shops et ce marchand"),
			"zh_CN", l("&dLeft-click to preview and edit shops and merchant"),
			"zh_TW", l("&f左鍵點擊 &a預覽交易和編輯商人"),
			"ru_RU", l("&dЛКМ для превью и редактирования"),
			"hu_HU", l("&dBal-klikk, hogy előnézd, és szerkeszd a boltot és a kereskedőt"),
			"es_ES", l("&dClic izquierdo para previsualizar y editar tiendas y mercaderes")
			);

	public static final Text MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC = n("MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC",
			"en_US", l("&7Object : &a{current}&7/&a{max}", "&7Total amount for this object : &a{amount}"),
			"fr_FR", l("&7Objet : &a{current}&7/&a{max}", "&7Montant total de cet objet : &a{amount}"),
			"zh_CN", l("&7Object : &a{current}&7/&a{max}", "&7Total amount for this object : &a{amount}"),
			"zh_TW", l("&7物品數量 : &a{current}&7/&a{max}", "&7這個商品的總數量  : &a{amount}"),
			"ru_RU", l("&7Предмет : &a{current}&7/&a{max}", "&7Общее количество этого предмета : &a{amount}"),
			"hu_HU", l("&7Tárgy : &a{current}&7/&a{max}", "&7A tárgy teljes összege: &a{amount}"),
			"es_ES", l("&7Objeto : &a{current}&7/&a{max}", "&7Cantidad total por este objeto : &a{amount}")
			);

	public static final Text MISC_SUPREMESHOPS_EDITITEMLISTLORE = n("MISC_SUPREMESHOPS_EDITITEMLISTLORE",
			"en_US", l("&7Match exact item : {match_exact_item} &7('false' will mean that", "&7 items with more enchants, effects, or item meta", "&7 parameters will be blacklisted as well)", "&7Durability check : {durability_check}", "", "&dLeft-click to set exact match", "&dRight-click to set durability check", "&dShift + right-click to remove item"),
			"fr_FR", l("&7Match l'item exact : {match_exact_item} &7('false' voudra dire que les", "&7 items avec + d'enchants, effets, ou paramètres", "&7 de meta seront aussi blacklistés)", "&7Check de durabilité : {durability_check}", "", "&dClic-gauche pour définir le match exact", "&dRight-click pour définir le check de durabilité", "&dShift + clic-droit pour supprimer l'item"),
			"zh_TW", l("&7符合確切物品: {match_exact_item}", "&7'false' 代表這個物品有更多的", "&7附魔、效果、子ID，也會被列入黑名單", "&7耐久度檢測: {durability_check}", "", "&f左鍵點擊 &7設置 符合確切項目", "&f右鍵點擊 &7設置 耐久度檢測", "&fSHIFT+右鍵點擊 &7移除物品"),
			"hu_HU", l("&7Pontos elem: {match_exact_item} &7(A false - azt jelenti,", "&7 hogy a több varázslattal, effektusokkal vagy", "&7 elemmeta-paraméterekkel rendelkező tételek is", "&7 feketelistára kerülnek)", "&7Tartóssági ellenőrzés: {durability_check}", "", "&dBal-klikk, hogy beállítsd a pontos egyezést", "&dJobb-klikk a tartóssági ellenőrzés", "&dShift + jobb-klikk, hogy eltávolítsd az itemet"),
			"es_ES", l("&7Coincidir ítem exacto : {match_exact_item} &7('false' significa que", "&7los ítems con más encantamientos, efectos o ítems con parámetros", "&7 meta serán añadidos a la lista negra también.", "&7Check de durabilidad : {durability_check}", "", "&dClic izquierdo para establecer coincidencia exacta de ítem", "&dClic derecho para establecer el check de durabilidad", "&dShift + clic derecho para eliminar el ítem")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMGUINAME = n("MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMGUINAME",
			"en_US", l("Destroy shop"),
			"fr_FR", l("Détruire le shop"),
			"zh_CN", l("Destroy shop"),
			"zh_TW", l("摧毀商店"),
			"ru_RU", l("Уничтожить магазин"),
			"hu_HU", l("Bolt lerombolása"),
			"es_ES", l("Destruir tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMGUINAME = n("MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMGUINAME",
			"en_US", l("Stop renting"),
			"fr_FR", l("Arrêter de louer"),
			"zh_TW", l("停止租借"),
			"ru_RU", l("Прекратить аренду"),
			"hu_HU", l("Bérlés megállítása"),
			"es_ES", l("Parar el alquiler")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMGUINAME = n("MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMGUINAME",
			"en_US", l("Force stop current renting"),
			"fr_FR", l("Forcer la fin de la location actuelle"),
			"zh_TW", l("強制停止當前租借合約"),
			"ru_RU", l("Принудительно остановить аренду"),
			"hu_HU", l("Jelenlegi bérlés megállításra kényszerítése"),
			"es_ES", l("Parar de forma forzosa el alquiler actual")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMGUINAME = n("MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMGUINAME",
			"en_US", l("Destroy merchant"),
			"fr_FR", l("Détruire le marchand"),
			"zh_CN", l("Destroy merchant"),
			"zh_TW", l("摧毀商人"),
			"ru_RU", l("Уничтожить торговца"),
			"hu_HU", l("Kereskedő lerombolása"),
			"es_ES", l("Destruir mercader")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMITEMNAME = n("MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMITEMNAME",
			"en_US", l("&cDestroy shop"),
			"fr_FR", l("&cDétruire le shop"),
			"zh_CN", l("&cDestroy shop"),
			"zh_TW", l("&c摧毀商店"),
			"ru_RU", l("&cУничтожить магазин"),
			"hu_HU", l("&cBolt lerombolása"),
			"es_ES", l("&cDestruir tienda")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMITEMNAME = n("MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMITEMNAME",
			"en_US", l("&cStop renting shop"),
			"fr_FR", l("&cArrêter de louer le shop"),
			"zh_TW", l("&c停止租借商店"),
			"ru_RU", l("&cПринудительно остановить аренду магазина"),
			"hu_HU", l("&cBolt bérlésének megállítása"),
			"es_ES", l("&cParar el alquiler de la tienda")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCONFIRMITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCONFIRMITEMNAME",
			"en_US", l("&cStop renting merchant"),
			"fr_FR", l("&cArrêter de louer le marchand"),
			"zh_TW", l("&c停止租借商人"),
			"ru_RU", l("&cОстановить торговца"),
			"hu_HU", l("&cKereskedő bérlésének megállítása"),
			"es_ES", l("&cParar el comercio del mercader")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMITEMNAME = n("MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMITEMNAME",
			"en_US", l("&cForce stop current renting"),
			"fr_FR", l("&cForcer la fin de location actuelle"),
			"zh_TW", l("&c強制停止當前租借合約"),
			"ru_RU", l("&cПринудительно остановить аренду"),
			"hu_HU", l("&cJelenlegi bérlés megállításának kényszerítése"),
			"es_ES", l("&cParar de forma forzosa el alquiler actual.")
			);

	public static final Text MISC_SUPREMESHOPS_SHOPSTOPRENTINGCANCELITEMNAME = n("MISC_SUPREMESHOPS_SHOPSTOPRENTINGCANCELITEMNAME",
			"en_US", l("&7Cancel"),
			"fr_FR", l("&7Annuler"),
			"zh_TW", l("&7取消"),
			"ru_RU", l("&7Отменить"),
			"hu_HU", l("&7Megszakít"),
			"es_ES", l("&7Cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCANCELITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCANCELITEMNAME",
			"en_US", l("&7Cancel"),
			"fr_FR", l("&7Annuler"),
			"zh_TW", l("&7取消"),
			"ru_RU", l("&7Отменить"),
			"hu_HU", l("&7Megszakít"),
			"es_ES", l("&7Cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCANCELITEMNAME = n("MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCANCELITEMNAME",
			"en_US", l("&7Cancel"),
			"fr_FR", l("&7Annuler"),
			"zh_TW", l("&7取消"),
			"ru_RU", l("&7Отменить"),
			"hu_HU", l("&7Megszakít"),
			"es_ES", l("&7Cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMITEMNAME = n("MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMITEMNAME",
			"en_US", l("&cDestroy merchant"),
			"fr_FR", l("&cDétruire le marchand"),
			"zh_CN", l("&cDestroy merchant"),
			"zh_TW", l("&c摧毀商人"),
			"ru_RU", l("&cУничтожить торговца"),
			"hu_HU", l("&cKereskedő lerombolása"),
			"es_ES", l("&cDestruir mercader")
			);

	public static final Text MISC_SUPREMESHOPS_DESTROYCANCELITEMNAME = n("MISC_SUPREMESHOPS_DESTROYCANCELITEMNAME",
			"en_US", l("&7Cancel"),
			"fr_FR", l("&7Annuler"),
			"zh_TW", l("&7取消"),
			"ru_RU", l("&7Отменить"),
			"hu_HU", l("&7Megszakít"),
			"es_ES", l("&7Cancelar")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEGUINAME = n("MISC_SUPREMESHOPS_PLAYERTRADEGUINAME",
			"en_US", l("Safe trade"),
			"fr_FR", l("Échange sécurisé"),
			"zh_CN", l("Safe trade"),
			"zh_TW", l("安全交易"),
			"ru_RU", l("Безопасный обмен"),
			"hu_HU", l("Biztonságos kereskedelem"),
			"es_ES", l("Comercio seguro")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEREADYNAME = n("MISC_SUPREMESHOPS_PLAYERTRADEREADYNAME",
			"en_US", l("&a{player} &a: ready"),
			"fr_FR", l("&a{player} &a: prêt"),
			"zh_CN", l("&a{player} &a: ready"),
			"zh_TW", l("&f{player}: &a已準備交易"),
			"ru_RU", l("&a{player} &a: готов"),
			"hu_HU", l("&a{player}&a: készen áll"),
			"es_ES", l("&a{player}: &alisto")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADENOTREADYNAME = n("MISC_SUPREMESHOPS_PLAYERTRADENOTREADYNAME",
			"en_US", l("&c{player} &c: not ready"),
			"fr_FR", l("&c{player} &c: pas prêt"),
			"zh_CN", l("&c{player} &c: not ready"),
			"zh_TW", l("&c{player}: &c還沒準備好"),
			"ru_RU", l("&a{player} &a: не готов"),
			"hu_HU", l("&c{player}&c: Nem áll készen"),
			"es_ES", l("&c{player} &c: no preparado")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGSELFHEADERNAME = n("MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGSELFHEADERNAME",
			"en_US", l("&aYour offer"),
			"fr_FR", l("&aVotre offre"),
			"zh_CN", l("&aYour offer"),
			"zh_TW", l("&a你提出的交易物件"),
			"ru_RU", l("&aВы предлагаете"),
			"hu_HU", l("&aAjánlataid"),
			"es_ES", l("&aTu oferta")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGHEADERNAME = n("MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGHEADERNAME",
			"en_US", l("&a{player}&a's offer"),
			"fr_FR", l("&aOffre de {player}"),
			"zh_CN", l("&a{player}&a's offer"),
			"zh_TW", l("&f{player} &a提出的交易物件"),
			"ru_RU", l("&a{player}&a предлагает"),
			"hu_HU", l("&a{player}&a játékos ajánlata"),
			"es_ES", l("&aOferta de {player}")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTNAME = n("MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTNAME",
			"en_US", l("&6Add an object"),
			"fr_FR", l("&6Ajouter un object"),
			"zh_CN", l("&6Add an object"),
			"zh_TW", l("&6新增一個物件"),
			"ru_RU", l("&6Добавить предмет"),
			"hu_HU", l("&6Adj hozzá tárgyat"),
			"es_ES", l("&6Añadir un objeto")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADECHATNAME = n("MISC_SUPREMESHOPS_PLAYERTRADECHATNAME",
			"en_US", l("&6Message {player}"),
			"fr_FR", l("&6Envoyer un message à {player}"),
			"zh_CN", l("&6Message {player}"),
			"zh_TW", l("&6發送訊息給 {player}"),
			"ru_RU", l("&6Сообщение {player}"),
			"hu_HU", l("&6{player} üzenet"),
			"es_ES", l("&6Enviar un mensaje a {player}")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADECANCELNAME = n("MISC_SUPREMESHOPS_PLAYERTRADECANCELNAME",
			"en_US", l("&cCancel trade"),
			"fr_FR", l("&cAnnuler l'échange"),
			"zh_CN", l("&cCancel trade"),
			"zh_TW", l("&c取消交易"),
			"ru_RU", l("&cОтменить обмен"),
			"hu_HU", l("&cKereskedelem megszakítása"),
			"es_ES", l("&cCancelar comercio")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTSELECTTYPEGUINAME = n("MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTSELECTTYPEGUINAME",
			"en_US", l("Select type"),
			"fr_FR", l("Sélectionner type"),
			"zh_CN", l("Select type"),
			"zh_TW", l("選擇類型"),
			"ru_RU", l("Выбрать тип"),
			"hu_HU", l("Válassz típust"),
			"es_ES", l("Seleccionar tipo")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADEOBJECTITEMLORE = n("MISC_SUPREMESHOPS_PLAYERTRADEOBJECTITEMLORE",
			"en_US", l("", "&dShift + right-click to remove item"),
			"fr_FR", l("", "&dShift + clic-droit pour supprimer l'item"),
			"zh_CN", l("", "&dShift + right-click to remove item"),
			"zh_TW", l("", "&fSHIFT+右鍵點擊 &7移除物品"),
			"ru_RU", l("", "&dShift + ПКМ, чтобы удалить предмет"),
			"hu_HU", l("", "&dShift + jobb-klikk, hogy eltávolítsd az itemet"),
			"es_ES", l("", "&dShift + clic derecho para eliminar un objeto")
			);

	public static final Text MISC_SUPREMESHOPS_PLAYERTRADESELECTPLAYERGUINAME = n("MISC_SUPREMESHOPS_PLAYERTRADESELECTPLAYERGUINAME",
			"en_US", l("Select player"),
			"fr_FR", l("Sélectionner joueur"),
			"zh_CN", l("Select player"),
			"zh_TW", l("選擇玩家"),
			"ru_RU", l("Выбрать игрока"),
			"hu_HU", l("Játékos kiválasztása"),
			"es_ES", l("Seleccionar jugador")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFWEEK = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFWEEK",
			"en_US", l("from {start) to {end}"),
			"fr_FR", l("de {start} à {end}"),
			"zh_CN", l("from {start) to {end}"),
			"zh_TW", l("從 {start} 到 {end}"),
			"ru_RU", l("с {start) до {end}"),
			"hu_HU", l("{start} kezdettől {end} végéig"),
			"es_ES", l("de {start} a {end}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFMONTH = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFMONTH",
			"en_US", l("from {start) to {end}"),
			"fr_FR", l("de {start} à {end}"),
			"zh_CN", l("from {start) to {end}"),
			"zh_TW", l("從 {start} 到 {end}"),
			"hu_HU", l("{start} kezdettől {end} végéig"),
			"es_ES", l("de {start} a {end}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_GAMETIME = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_GAMETIME",
			"en_US", l("from {start) ticks to {end} ticks"),
			"fr_FR", l("de {start} ticks à {end} ticks"),
			"zh_CN", l("from {start) ticks to {end} ticks"),
			"zh_TW", l("從 {start} 遊戲刻到 {end} 遊戲刻"),
			"ru_RU", l("с {start) тиков до {end} тиков"),
			"hu_HU", l("{start} tick kezdettől {end} tick végéig"),
			"es_ES", l("de {start) ticks a {end} ticks")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLD = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLD",
			"en_US", l("in world {world}"),
			"fr_FR", l("dans le monde {world}"),
			"zh_CN", l("in world {world}"),
			"zh_TW", l("在世界 {world}"),
			"ru_RU", l("в мире {world}"),
			"hu_HU", l("{world} világban"),
			"es_ES", l("en el mundo {world}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLDGUARDREGION = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLDGUARDREGION",
			"en_US", l("in region {region} of world {world}"),
			"fr_FR", l("dans la région {region} du monde {world}"),
			"zh_CN", l("in region {region} of world {world}"),
			"zh_TW", l("在世界 {world} 的區域 {region}"),
			"ru_RU", l("в регионе {region} мира {world}"),
			"hu_HU", l("{world} világ {region} régiójában"),
			"es_ES", l("en la región {region} del mundo {world}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERMONEY = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERMONEY",
			"en_US", l("{operation} {money}$"),
			"fr_FR", l("{operation} {money}$"),
			"zh_CN", l("{operation} {money}$"),
			"zh_TW", l("{operation} {money}$"),
			"ru_RU", l("{operation} {money}$"),
			"hu_HU", l("{operation} {money}$"),
			"es_ES", l("{operation} {money}$")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPERMISSION = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPERMISSION",
			"en_US", l("{description}"),
			"fr_FR", l("{description}"),
			"zh_CN", l("{description}"),
			"zh_TW", l("{description}"),
			"ru_RU", l("{description}"),
			"hu_HU", l("{description}"),
			"es_ES", l("{description}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPLACEHOLDERAPIVARIABLE = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPLACEHOLDERAPIVARIABLE",
			"en_US", l("{description}"),
			"fr_FR", l("{description}"),
			"zh_CN", l("{description}"),
			"zh_TW", l("{description}"),
			"ru_RU", l("{description}"),
			"hu_HU", l("{description}"),
			"es_ES", l("{description}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSONE = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSONE",
			"en_US", l("{operation} item {item}"),
			"fr_FR", l("{operation} l'item {item}"),
			"zh_CN", l("{operation} item {item}"),
			"zh_TW", l("{operation} 物品 {item}"),
			"ru_RU", l("{operation} предмет {item}"),
			"hu_HU", l("{operation} {item} item"),
			"es_ES", l("{operation} ítem {item}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSMULTIPLE = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSMULTIPLE",
			"en_US", l("{operation} {amount} of those items : {items}"),
			"fr_FR", l("{operation} {amount} de ces items : {items}"),
			"zh_CN", l("{operation} {amount} of those items : {items}"),
			"zh_TW", l("{operation} {items} 中的 {amount}"),
			"ru_RU", l("{operation} {amount} тех предметов : {items}"),
			"hu_HU", l("{operation} {amount} e tétel közül : {items}"),
			"es_ES", l("{operation} {amount} de estos ítems: {items}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERXPLEVEL = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERXPLEVEL",
			"en_US", l("{operation} {level} xp levels"),
			"fr_FR", l("{operation} {level} niveaux d'xp"),
			"zh_CN", l("{operation} {level} xp levels"),
			"zh_TW", l("{operation} {level} 經驗等級"),
			"ru_RU", l("{operation} {level} уровней"),
			"hu_HU", l("{operation} {level} xp szint"),
			"es_ES", l("{operation} {level} niveles de xp")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERHUNGER = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERHUNGER",
			"en_US", l("{operation} {amount} food level"),
			"fr_FR", l("{operation} {amount} niveaux de faim"),
			"zh_CN", l("{operation} {amount} food level"),
			"zh_TW", l("{operation} {amount} 飢餓度"),
			"ru_RU", l("{operation} {amount} голода"),
			"hu_HU", l("{operation} {amount} kaja szint"),
			"es_ES", l("{operation} {amount} nivel de comida")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPOINTSPOINTS = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPOINTSPOINTS",
			"en_US", l("{operation} {amount} PlayerPoints points"),
			"fr_FR", l("{operation} {amount} points PlayerPoints"),
			"zh_CN", l("{operation} {amount} PlayerPoints points"),
			"zh_TW", l("{operation} {amount} PlayerPoints 點數"),
			"ru_RU", l("{operation} {amount} очков PlayerPoints"),
			"hu_HU", l("{operation} {amount} PlayerPoints pont"),
			"es_ES", l("{operation} {amount} PlayerPoints puntos")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_TOKENENCHANTTOKENS = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_TOKENENCHANTTOKENS",
			"en_US", l("{operation} {amount} TokenEnchant tokens"),
			"fr_FR", l("{operation} {amount} tokens TokenEnchant"),
			"zh_TW", l("{operation} {amount} TokenEnchant 代幣"),
			"ru_RU", l("{operation} {amount} TokenEnchant токенов"),
			"hu_HU", l("{operation} {amount} TokenEnchant token"),
			"es_ES", l("{operation} {amount} TokenEnchant token(s)")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERHEALTH = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERHEALTH",
			"en_US", l("{operation} {amount} health"),
			"fr_FR", l("{operation} {amount} vie"),
			"zh_CN", l("{operation} {amount} health"),
			"zh_TW", l("{operation} {amount} 血量"),
			"ru_RU", l("{operation} {amount} здоровья"),
			"hu_HU", l("{operation} {amount} élet"),
			"es_ES", l("{operation} {amount} vida")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPBLOCKTYPE = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPBLOCKTYPE",
			"en_US", l("shop block must be of type {type}"),
			"fr_FR", l("le bloc de shop doit être de type {type}"),
			"zh_CN", l("shop block must be of type {type}"),
			"zh_TW", l("商店必須是類型 {type}"),
			"ru_RU", l("блок магазина должен быть - {type}"),
			"hu_HU", l("az üzletnek {type} típusnak kell lennie"),
			"es_ES", l("el bloque de la tienda debe ser del tipo {type}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPISADMIN = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPISADMIN",
			"en_US", l("shop must be admin"),
			"fr_FR", l("le shop doit être admin"),
			"zh_CN", l("shop must be admin"),
			"zh_TW", l("商店必須是管理員商店'"),
			"ru_RU", l("магазин должен быть Админ-шопом"),
			"hu_HU", l("a boltnak adminnak kell lennie"),
			"es_ES", l("la tienda debe ser una de tipo admin")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPOBJECTCOUNT = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPOBJECTCOUNT",
			"en_US", l("shop must have {operation} {amount}{side}{type} object{plural}"),
			"fr_FR", l("le shop doit avoir {operation} {amount}{side}{type} objet{plural}"),
			"zh_CN", l("shop must have {amount}{side}{type} object{plural}"),
			"zh_TW", l("商店必須有 {operation} {amount}{side}{type} 物件"),
			"ru_RU", l("магазин должен иметь {operation} {amount}{side}{type} предмет{plural}"),
			"hu_HU", l("boltnak lennie kell {operation} {amount}{side}{type} tárgynak"),
			"es_ES", l("la tienda debe tener {operation} {amount}{side}{type} objeto{plural}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCOUNT = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCOUNT",
			"en_US", l("must have {operation} {amount}{type} shop{plural}"),
			"fr_FR", l("doit avoir {operation} {amount}{type} shop{plural}"),
			"zh_TW", l("商店必須有 {operation} {amount} {type} 商店"),
			"ru_RU", l("должно быть {operation} {amount}{type} магазин{plural}"),
			"hu_HU", l("kell {operation} {amount}{type} bolt"),
			"es_ES", l("debe haber {operation} {amount}{type} tienda{plural}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCONDITIONCOUNT = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCONDITIONCOUNT",
			"en_US", l("shop must have {amount}{type} condition{plural}"),
			"fr_FR", l("le shop doit avoir {amount}{type} condition{plural}"),
			"zh_CN", l("shop must have {amount}{type} condition{plural}"),
			"zh_TW", l("商店必須有 {amount}{type} 條件"),
			"ru_RU", l("магазин должен иметь {amount}{type} условие{plural}"),
			"hu_HU", l("boltnak lennie kell {amount}{type} feltételnek"),
			"es_ES", l("la tienda debe tener {amount}{type} condición(es)")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPTYPE = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPTYPE",
			"en_US", l("shop must be of type {type}"),
			"fr_FR", l("le shop doit être de type {type}"),
			"zh_CN", l("shop must be of type {type}"),
			"zh_TW", l("商店必須是類型 {type}"),
			"ru_RU", l("допустимый тип магазина {type}"),
			"hu_HU", l("üzletnek {type} típusnak kell lennie"),
			"es_ES", l("la tienda debe ser del tipo {type}")
			);

	public static final Text MISC_SUPREMESHOPS_CONDITIONDESCRIBE_TIMEOFDAY = n("MISC_SUPREMESHOPS_CONDITIONDESCRIBE_TIMEOFDAY",
			"en_US", l("from {start_hour}:{start_minute} to {end_hour}:{end_minute}"),
			"fr_FR", l("de {start_hour}:{start_minute} à {end_hour}:{end_minute}"),
			"zh_CN", l("from {start_hour}:{start_minute} to {end_hour}:{end_minute}"),
			"zh_TW", l("從 {start_hour}:{start_minute} 到 {end_hour}:{end_minute}"),
			"ru_RU", l("с {start_hour}:{start_minute} до {end_hour}:{end_minute}"),
			"hu_HU", l("{start_hour}:{start_minute} kezdettől {end_hour}:{end_minute} végéig"),
			"es_ES", l("de {start_hour}:{start_minute} a {end_hour}:{end_minute}")
			);

}
