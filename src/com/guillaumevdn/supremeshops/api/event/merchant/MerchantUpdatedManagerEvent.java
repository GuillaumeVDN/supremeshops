package com.guillaumevdn.supremeshops.api.event.merchant;

import java.util.List;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public class MerchantUpdatedManagerEvent extends MerchantEvent {

	// base
	private UserInfo manager;
	private Operation operation;
	private TradeObject wageObject;
	private List<MerchantManagementPermission> affectedPermissions;

	public MerchantUpdatedManagerEvent(Merchant merchant, UserInfo manager, TradeObject wageObject, List<MerchantManagementPermission> affectedPermissions, Operation operation) {
		super(merchant);
		this.manager = manager;
		this.operation = operation;
		this.wageObject = wageObject;
		this.affectedPermissions = affectedPermissions;
	}

	// get
	public UserInfo getManager() {
		return manager;
	}

	public Operation getOperation() {
		return operation;
	}

	public TradeObject getWageObject() {
		return wageObject;
	}

	public List<MerchantManagementPermission> getAffectedPermissions() {
		return affectedPermissions;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_MANAGER,
		REMOVE_MANAGER,
		ADD_PERMISSION,
		REMOVE_PERMISSION,
		UPDATE_PERMISSIONS,
		ADD_WAGE,
		REMOVE_WAGE
	}

}
