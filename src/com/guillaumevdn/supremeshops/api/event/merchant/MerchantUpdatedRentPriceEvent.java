package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public class MerchantUpdatedRentPriceEvent extends MerchantEvent {

	// base
	private Operation operation;
	private TradeObject priceObject;

	public MerchantUpdatedRentPriceEvent(Merchant merchant, TradeObject priceObject, Operation operation) {
		super(merchant);
		this.operation = operation;
		this.priceObject = priceObject;
	}

	// get
	public Operation getOperation() {
		return operation;
	}

	public TradeObject getPriceObject() {
		return priceObject;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_OBJECT,
		REMOVE_OBJECT
	}

}
