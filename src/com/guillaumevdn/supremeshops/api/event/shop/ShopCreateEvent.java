package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopCreateEvent extends ShopEvent implements Cancellable {

	// base
	private Player whoCreated;

	public ShopCreateEvent(Shop shop, Player whoCreated) {
		super(shop);
		this.whoCreated = whoCreated;
	}

	// get
	public Player getWhoCreated() {
		return whoCreated;
	}

	// cancellable
	private boolean cancelled;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
