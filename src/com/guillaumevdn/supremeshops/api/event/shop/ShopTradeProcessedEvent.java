package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopTradeProcessedEvent extends ShopEvent {

	// base
	private Player player;
	private int trades;
	private Merchant merchant;

	public ShopTradeProcessedEvent(Shop shop, Player player, int trades, Merchant merchant) {
		super(shop);
		this.player = player;
		this.trades = trades;
		this.merchant = merchant;
	}

	// get
	public Player getPlayer() {
		return player;
	}

	public int getTrades() {
		return trades;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
