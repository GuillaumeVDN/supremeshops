package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopUpdatedEvent extends ShopEvent {

	// base
	private Operation operation;

	public ShopUpdatedEvent(Shop shop, Operation operation) {
		super(shop);
		this.operation = operation;
	}

	// get
	public Operation getOperation() {
		return operation;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		SET_ACTUAL_OWNER,
		SET_DISPLAY_NAME,
		SET_PARTICLE_PATTERN,
		TOGGLE_OPEN,
		TOGGLE_REMOTE,
		TOGGLE_DISPLAY_ITEMS,
		TOGGLE_ADMIN_STOCK,
		SET_TRADES_LIMIT,
		TOGGLE_RENTABLE,
		SET_RENT_PERIOD,
		SET_RENT_PERIOD_STREAK_LIMIT,
		SET_RENT_PERIOD_STREAK_LIMIT_DELAY,
		SET_UNRENTED_PARTICLE_PATTERN,
		SET_PAID_RENTS,
		SET_CURRENT_OWNER,
		UPDATED_RENT
	}

}
