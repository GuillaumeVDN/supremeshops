package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopUpdatedObjectEvent extends ShopEvent {

	// base
	private TradeObject object;
	private Operation operation;

	public ShopUpdatedObjectEvent(Shop shop, TradeObject object, Operation operation) {
		super(shop);
		this.object = object;
		this.operation = operation;
	}

	// get
	public TradeObject getObject() {
		return object;
	}

	public Operation getOperation() {
		return operation;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_OBJECT,
		REMOVE_OBJECT,
		SET_STOCK
	}

}
