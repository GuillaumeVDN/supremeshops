package com.guillaumevdn.supremeshops.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.gui.SSGui;

public class ArgMenu extends CommandArgument {

	private static final Param paramPlayer = new Param(Utils.asList("player", "p"), "name", SSPerm.SUPREMESHOPS_COMMAND_MENU_OTHERS, false, false);
	private static final Param paramMenu = new Param(Utils.asList("menu", "m"), "name", SSPerm.SUPREMESHOPS_COMMAND_MENU_SPECIFIC, false, false);

	public ArgMenu() {
		super(SupremeShops.inst(), Utils.asList("menu", "see"), "open a specific menu", SSPerm.SUPREMESHOPS_COMMAND_MENU, false, paramPlayer, paramMenu);
	}

	@Override
	public void perform(CommandCall call) {
		CommandSender sender = call.getSender();
		Player target = paramPlayer.getPlayer(call, true);
		if (target != null) {
			boolean self = sender.getName().equalsIgnoreCase(target.getName());
			String id = paramMenu.has(call) ? paramMenu.getString(call) : SupremeShops.inst().getModuleManager().getMainGuiId();
			if (id != null) {
				// open
				SSGui gui = SupremeShops.inst().getModuleManager().getGui(id);
				if (gui == null) {
					Messenger.send(sender, Messenger.Level.SEVERE_ERROR, "SupremeShops", "Could not find any gui with id '" + id + "'.");
					SupremeShops.inst().warning("Trying to open GUI " + id + " but doesn't exist (from command, sender " + sender.getName() + " " + (!self ? ", target " + target.getName() : "") + ")");
				} else {
					gui.build(target).open(target);
				}
			}
		}
	}

}
