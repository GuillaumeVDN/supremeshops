package com.guillaumevdn.supremeshops.command.playertrade;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.CommandRoot;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.PlayerSelectionGUI;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;

public class CommandRootPlayertrade extends CommandRoot {

	// base
	private static Param paramPlayer = new Param(Utils.asList("player", "p"), "name", null, false, false);

	public CommandRootPlayertrade() {
		super(SupremeShops.inst(), Utils.asList("trade", "exchange", "safetrade"), "trade with a player", SSPerm.SUPREMESHOPS_COMMAND_TRADE, true, paramPlayer);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		// has an active trade
		Player sender = call.getSenderAsPlayer();
		PlayerTrade trade = SupremeShops.inst().getGeneralManager().getPlayerTrade(sender);
		if (trade != null) {
			trade.openGUI(sender);
		}
		// new trade
		else {
			// has player param
			if (paramPlayer.has(call)) {
				Player selected = paramPlayer.getPlayer(call, false);
				if (selected != null) {
					// self
					if (selected.equals(sender)) {
						SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEREQUESTCANTSELF.send(sender);
						return;
					}
					// start
					onSelect(sender, selected);
				}
			}
			// no specified player
			else {
				new PlayerSelectionGUI(sender) {
					@Override
					protected void onSelect(final Player player, Player selected) {
						CommandRootPlayertrade.this.onSelect(player, selected);
					}
				}.open(sender);
			}
		}
	}

	private void onSelect(final Player player, Player selected) {
		// selected player had asked first
		List<Player> asked = SupremeShops.inst().getGeneralManager().getAskedPlayerTrades(player);
		if (asked.contains(selected)) {
			asked.remove(selected);
			SupremeShops.inst().getGeneralManager().startTrade(selected, player);
		}
		// ask
		else {
			final List<Player> list = SupremeShops.inst().getGeneralManager().getAskedPlayerTrades(selected);
			list.add(player);
			SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEREQUEST.send(selected, "{player}", player.getName());
			SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEREQUESTSELF.send(player, "{player}", selected.getName());
			new BukkitRunnable() {
				@Override
				public void run() {
					list.remove(player);
				}
			}.runTaskLaterAsynchronously(SupremeShops.inst(), 20L * 60L);
		}
	}

}
