package com.guillaumevdn.supremeshops.command.shop;

import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.CommandRoot;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.gui.SSGui;

public class CommandRootShop extends CommandRoot {

	// base
	public CommandRootShop() {
		super(SupremeShops.inst(), Utils.asList("supremeshops", "sshops", "shops", "shop"), "main command for SupremeShops", null, false);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		// attempt to open GUI
		if (SSPerm.SUPREMESHOPS_COMMAND_SHOP_MENU.has(call.getSender()) && call.senderIsPlayer()) {
			SSGui esGUI = SupremeShops.inst().getModuleManager().getGui(SupremeShops.inst().getModuleManager().getMainGuiId());
			FilledGUI gui = esGUI == null ? null : esGUI.build(call.getSenderAsPlayer());
			if (gui == null) {
				SupremeShops.inst().error("Couldn't " + (esGUI == null ? "find" : "build") + " main gui with id " + SupremeShops.inst().getModuleManager().getMainGuiId());
			} else {
				gui.open(call.getSenderAsPlayer());
				return;
			}
		}
		// show help
		super.perform(call);
	}

}
