package com.guillaumevdn.supremeshops.data;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataBoard;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.ShopType;
import com.guillaumevdn.supremeshops.module.shop.SignShop;

public class ShopBoard extends DataBoard<Shop> {

	// fields
	private Map<String, Shop> shops = new ConcurrentHashMap<String, Shop>();
	private Map<BlockCoords, BlockShop> blockShopsByBlock = new ConcurrentHashMap<BlockCoords, BlockShop>();
	private Map<BlockCoords, BlockShop> blockShopsBySign = new ConcurrentHashMap<BlockCoords, BlockShop>();
	private Map<BlockCoords, SignShop> signShopsBySign = new ConcurrentHashMap<BlockCoords, SignShop>();
	private Map<UserInfo, List<Shop>> shopsByOwners = new HashMap<>();// null key = admin ; concurrent hashmaps can't have a null key ? :confusedwat: so whatever

	// methods
	public Map<String, Shop> getAll() {
		return Collections.unmodifiableMap(shops);
	}

	public List<Shop> getAll(Collection<? extends Class<?>> types, boolean mustBeOpen, ElementRemotePolicy remotePolicy) {
		// get shops
		List<Shop> result = new ArrayList<Shop>();
		// get matching shops
		for (Shop shop : shops.values()) {
			// invalid type
			if (types != null && !types.isEmpty() && !Utils.hasOneSuper(shop.getClass(), types)) {
				continue;
			}
			// must be open but isn't
			if (mustBeOpen && !shop.isOpen()) {
				continue;
			}
			// must be remote but isn't
			if (remotePolicy.equals(ElementRemotePolicy.MUST_BE)) {
				if (!shop.isRemote()) {
					continue;
				}
			}
			// is remote but musn't be
			else if (remotePolicy.equals(ElementRemotePolicy.CANT_BE)) {
				if (shop.isRemote()) {
					continue;
				}
			}
			// we good
			result.add(shop);
		}
		// done
		return Collections.unmodifiableList(result);
	}

	public Map<UserInfo, List<Shop>> getAllByOwners(boolean withAdmin) {
		if (withAdmin) {
			return Collections.unmodifiableMap(shopsByOwners);
		} else {
			Map<UserInfo, List<Shop>> copy = Utils.asMapCopy(shopsByOwners);
			copy.remove(null);
			return Collections.unmodifiableMap(copy);
		}
	}

	/**
	 * Register and save a shop
	 */
	public void add(Shop shop) {
		if (!shops.containsKey(shop.getId().toLowerCase())) {
			shops.put(shop.getId().toLowerCase(), shop);
			shop.pushAsync();
			// update by player
			updateByOwner(shop, null);
			// update by block/sign
			if (shop instanceof BlockShop) {
				blockShopsByBlock.put(((BlockShop) shop).getBlock(), (BlockShop) shop);
				blockShopsBySign.put(((BlockShop) shop).getSign(), (BlockShop) shop);
			}
			// update by sign
			if (shop instanceof SignShop) {
				signShopsBySign.put(((SignShop) shop).getSign(), (SignShop) shop);
			}
		}
	}

	/**
	 * Delete and unregister a shop
	 */
	public void delete(Shop shop) {
		if (shops.containsKey(shop.getId().toLowerCase())) {
			shops.remove(shop.getId().toLowerCase());
			shop.deleteAsync();
			// update by owner
			List<Shop> byOwner = shopsByOwners.get(shop.getCurrentOwner());
			if (byOwner != null) {
				byOwner.remove(shop);
				if (byOwner.isEmpty()) {
					shopsByOwners.remove(shop.getCurrentOwner());
				}
			}
			// update by block/sign
			if (shop instanceof BlockShop) {
				blockShopsByBlock.remove(((BlockShop) shop).getBlock());
				blockShopsBySign.remove(((BlockShop) shop).getSign());
			}
			// update by sign
			if (shop instanceof SignShop) {
				signShopsBySign.remove(((SignShop) shop).getSign());
			}
		}
	}

	public void addAll(Collection<? extends Shop> shops, final Callback pushCallback) {
		// add shops
		for (Shop shop : shops) {
			this.shops.put(shop.getId().toLowerCase(), shop);
		}
		// push all
		pushAsync(shops, pushCallback);
		// update block/sign shops
		for (Shop shop : shops) {
			if (shop instanceof BlockShop) {
				blockShopsByBlock.put(((BlockShop) shop).getBlock(), (BlockShop) shop);
				blockShopsBySign.put(((BlockShop) shop).getSign(), (BlockShop) shop);
			}
			if (shop instanceof SignShop) {
				signShopsBySign.put(((SignShop) shop).getSign(), (SignShop) shop);
			}
		}
		// update by owner
		shopsByOwners.clear();
		for (Shop shop : shops) {
			List<Shop> byOwner = shopsByOwners.get(shop.getCurrentOwner());
			if (byOwner != null) {
				byOwner.remove(shop);
				if (byOwner.isEmpty()) {
					shopsByOwners.remove(shop.getCurrentOwner());
				}
			}
		}
	}

	public void deleteAll(Collection<? extends Shop> shops) {
		// add shops
		for (Shop shop : shops) {
			this.shops.remove(shop.getId().toLowerCase());
		}
		// push all
		deleteAsync(shops);
		// update block/sign shops
		for (Shop shop : shops) {
			if (shop instanceof BlockShop) {
				blockShopsByBlock.remove(((BlockShop) shop).getBlock());
				blockShopsBySign.remove(((BlockShop) shop).getSign());
			}
			if (shop instanceof SignShop) {
				signShopsBySign.remove(((SignShop) shop).getSign());
			}
		}
		// update by owner
		shopsByOwners.clear();
		for (Shop shop : shops) {
			updateByOwner(shop, null);
		}
	}

	public void updateByOwner(Shop shop, UserInfo oldOwner) {
		// remove old
		if (oldOwner != null) {
			List<Shop> byOwner = shopsByOwners.get(oldOwner);
			if (byOwner != null && byOwner.remove(shop) && byOwner.isEmpty()) {
				shopsByOwners.remove(oldOwner);
			}
		}
		// add new
		List<Shop> byPlayer = shopsByOwners.get(shop.getCurrentOwner());
		if (byPlayer == null) shopsByOwners.put(shop.getCurrentOwner(), byPlayer = new ArrayList<Shop>());
		byPlayer.add(shop);
	}

	/**
	 * Get a shop by its coordinates
	 * @param param the shop id
	 */
	@Override
	public Shop getElement(Object param) {
		if (param instanceof String) {
			return shops.get(((String) param).toLowerCase());
		}
		throw new IllegalArgumentException("param type " + param.getClass() + " must be a shop id");
	}

	public PlayerShop getPhysicalElementLinkedToBlock(BlockCoords block) {
		// sign shop
		PlayerShop shop = signShopsBySign.get(block);
		if (shop != null) {
			return shop;
		}
		// block shop
		shop = blockShopsBySign.get(block);
		return shop != null ? shop : blockShopsByBlock.get(block);
	}

	public List<Shop> getElementsByOwner(UserInfo owner) {
		return getElementsByOwner(owner, null);
	}

	public List<Shop> getElementsByOwner(UserInfo owner, Collection<? extends Class<?>> types) {
		List<Shop> shops = new ArrayList<Shop>();
		if (shopsByOwners.containsKey(owner)) {
			for (Shop shop : shopsByOwners.get(owner)) {
				if ((types == null || types.isEmpty()) || Utils.hasOneSuper(shop.getClass(), types)) {
					shops.add(shop);
				}
			}
		}
		return shops;
	}

	public Map<UserInfo, List<Shop>> getShopsByOwners() {
		return Collections.unmodifiableMap(shopsByOwners);
	}

	public List<Shop> getElements(UserInfo player, Collection<? extends Class<?>> types, boolean mustBeOpen, ElementRemotePolicy remotePolicy) {
		// get shops
		List<Shop> result = new ArrayList<Shop>();
		List<Shop> byOwner = shopsByOwners.get(player);
		if (byOwner != null) {
			// get matching shops
			for (Shop shop : byOwner) {
				// type don't match
				if (types != null && !types.isEmpty() && !Utils.hasOneSuper(shop.getClass(), types)) {
					continue;
				}
				// must be open but isn't
				if (mustBeOpen && !shop.isOpen()) {
					continue;
				}
				// must be remote but isn't
				if (remotePolicy.equals(ElementRemotePolicy.MUST_BE)) {
					if (!shop.isRemote()) {
						continue;
					}
				}
				// is remote but musn't be
				else if (remotePolicy.equals(ElementRemotePolicy.CANT_BE)) {
					if (shop.isRemote()) {
						continue;
					}
				}
				// we good
				result.add(shop);
			}
		}
		// done
		return Collections.unmodifiableList(result);
	}

	public static enum ElementRemotePolicy {
		MUST_BE,
		MIGHT_BE,
		CANT_BE
	}

	// data
	@Override
	public SSDataManager getDataManager() {
		return SupremeShops.inst().getData();
	}

	@Override
	public final File getJsonFile(Shop element) {
		return new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_shops/" + element.getType().getDiskDataFolder() + "/" + element.getDataId() + ".json");
	}

	@Override
	protected final void jsonPull() {
		shops.clear();
		blockShopsByBlock.clear();
		blockShopsBySign.clear();
		// load shops by type
		for (ShopType type : ShopType.values()) {
			if (type.mustSaveData()) {
				File root = new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_shops/" + type.getDiskDataFolder() + "/");
				if (root.exists() && root.isDirectory()) {
					// load shops in this
					for (File file : root.listFiles()) {
						if (file.getName().toLowerCase().endsWith(".json")) {
							String id = file.getName().substring(0, file.getName().length() - 5);
							try {
								Constructor<? extends Shop> constructor = type.getShopClass().getDeclaredConstructor(String.class);
								constructor.setAccessible(true);
								Shop shop = constructor.newInstance(id);
								shop.jsonPull();
								if (!shop.hasLoadError()) {
									shops.put(shop.getId().toLowerCase(), shop);
								}
							} catch (Throwable exception) {
								exception.printStackTrace();
								SupremeShops.inst().error("Couldn't load shop " + id + " from file " + file.getPath());
							}
						}
					}
				}
			}
		}
		// update block/sign shops
		for (Shop shop : shops.values()) {
			if (shop instanceof BlockShop) {
				blockShopsByBlock.put(((BlockShop) shop).getBlock(), (BlockShop) shop);
				blockShopsBySign.put(((BlockShop) shop).getSign(), (BlockShop) shop);
			}
			if (shop instanceof SignShop) {
				signShopsBySign.put(((SignShop) shop).getSign(), (SignShop) shop);
			}
		}
		// update by player
		shopsByOwners.clear();
		for (Shop shop : shops.values()) {
			updateByOwner(shop, null);
		}
	}

	@Override
	protected final void jsonDelete() {
		File root = new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_shops/");
		if (root.exists() && root.isDirectory()) {
			root.delete();
		}
	}

	// MySQL
	@Override
	public final String getMySQLTable() {
		return "supremeshops_shops";
	}

	@Override
	protected final Query getMySQLInitQuery() {
		return new Query("CREATE TABLE IF NOT EXISTS `" + getMySQLTable() + "`("
				+ "`id` VARCHAR(100) NOT NULL,"
				+ "`type` VARCHAR(100) NOT NULL,"
				+ "`owner` VARCHAR(100) NOT NULL,"
				+ "`data` LONGTEXT NOT NULL,"
				+ "PRIMARY KEY(`id`)"
				+ ") ENGINE=`InnoDB` DEFAULT CHARSET=?;", "utf8");
	}

	@Override
	protected final void mysqlPull() {
		getDataManager().performMySQLGetQuery(new Query("SELECT * FROM `" + getMySQLTable() + "`;"), set -> {
			// load
			shops.clear();
			blockShopsByBlock.clear();
			blockShopsBySign.clear();
			while (set.next()) {
				String id = set.getString("id");
				try {
					ShopType type = Utils.valueOfOrNull(ShopType.class, set.getString("type"));
					Constructor<? extends Shop> constructor = type.getShopClass().getDeclaredConstructor(String.class);
					constructor.setAccessible(true);
					Shop shop = constructor.newInstance(id);
					shop.mysqlPull(set);
					if (!shop.hasLoadError()) {
						shops.put(shop.getId().toLowerCase(), shop);
					}
				} catch (Throwable exception) {
					exception.printStackTrace();
					SupremeShops.inst().error("Couldn't load shop " + id + " from database");
				}
			}
			// update block/sign shops
			for (Shop shop : shops.values()) {
				if (shop instanceof BlockShop) {
					blockShopsByBlock.put(((BlockShop) shop).getBlock(), (BlockShop) shop);
					blockShopsBySign.put(((BlockShop) shop).getSign(), (BlockShop) shop);
				}
				if (shop instanceof SignShop) {
					signShopsBySign.put(((SignShop) shop).getSign(), (SignShop) shop);
				}
			}
			// update by player
			shopsByOwners.clear();
			for (Shop shop : shops.values()) {
				updateByOwner(shop, null);
			}
		});
	}

	@Override
	protected final void mysqlDelete() {
		getDataManager().performMySQLUpdateQuery(new Query("DELETE FROM `" + getMySQLTable() + "`;"));
	}

}
