package com.guillaumevdn.supremeshops.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ItemInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.util.ItemListsUtils;
import com.guillaumevdn.supremeshops.util.ListItem;

public class ItemWhitelistEditionGUI extends FilledGUI {

	// amount
	public ItemWhitelistEditionGUI() {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_ITEMWHITELISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_44);
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		for (final ListItem whitelisted : SupremeShops.inst().getModuleManager().getItemWhitelist()) {
			// add stock and controls in lore
			ItemStack stack = whitelisted.getItem().getItemStack();
			ItemMeta meta = stack.getItemMeta();
			List<String> lore;
			if (meta.getLore() == null || meta.getLore().isEmpty()) {
				lore = new ArrayList<String>();
			} else {
				lore = Utils.asList(meta.getLore());
				lore.add(" ");
			}
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITITEMLISTLORE.getLines("{match_exact_item}", whitelisted.getExactMatch() ? "§atrue" : "§cfalse", "{durability_chec,}", whitelisted.getDurabilityCheck() ? "§atrue" : "§cfalse"));
			meta.setLore(lore);
			stack.setItemMeta(meta);
			// set item
			setRegularItem(new ClickeableItem(new ItemData("item_" + stack.hashCode(), -1, stack)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// left-click : set exact match
					if (clickType.equals(ClickType.LEFT)) {
						whitelisted.setExactMatch(!whitelisted.getExactMatch());
						ItemListsUtils.updateItemWhitelist(whitelisted);
						ItemWhitelistEditionGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// right-click : set durability check
					else if (clickType.equals(ClickType.LEFT)) {
						whitelisted.setDurabilityCheck(!whitelisted.getDurabilityCheck());
						ItemListsUtils.updateItemWhitelist(whitelisted);
						ItemWhitelistEditionGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// shift + right-click : remove item
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						ItemListsUtils.removeItemWhitelist(whitelisted.getItem(), true);
						ItemWhitelistEditionGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// add object item
		setPersistentItem(new ClickeableItem(new ItemData("add_object", 47, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				SSLocale.MSG_SUPREMESHOPS_WHITELISTITEMINPUT.send(player);
				GCoreLegacy.inst().getItemInputs().put(player, new ItemInput() {
					@Override
					public void onChoose(Player player, ItemStack value) {
						// invalid item
						if (value == null || Mat.fromItem(value).isAir()) {
							SSLocale.MSG_SUPREMESHOPS_WHITELISTITEMINVALID.send(player);
							ItemWhitelistEditionGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
						// already whitelisted
						if (ItemListsUtils.isItemWhitelisted(value)) {
							SSLocale.MSG_SUPREMESHOPS_WHITELISTITEMINVALID.send(player);
							ItemWhitelistEditionGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						// whitelist
						ItemListsUtils.updateItemWhitelist(new ListItem(new ItemData("whitelisted_" + value.hashCode(), value)));
						SSLocale.MSG_SUPREMESHOPS_WHITELISTITEMADDED.send(player);
						ItemWhitelistEditionGUI.this.open(player);
					}
				});
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
