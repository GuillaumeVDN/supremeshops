package com.guillaumevdn.supremeshops.gui.merchant;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public interface MerchantGUI {

	Merchant getMerchant();

}
