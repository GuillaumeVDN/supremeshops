package com.guillaumevdn.supremeshops.gui.rentable;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class UnrentedGUI extends FilledGUI implements RentableGUI {

	// amount
	private Rentable rentable;
	private Merchant fromMerchant;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public UnrentedGUI(Rentable rentable, Merchant fromMerchant, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEUNRENTEDGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.rentable = rentable;
		this.fromMerchant = fromMerchant;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}

	public Merchant getFromMerchant() {
		return fromMerchant;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add rent price items
		for (final TradeObject object : rentable.getRentPrice()) {
			setRegularItem(new ClickeableItem(new ItemData("object_" + object.hashCode(), -1, object.getPreviewItem(object.getCustomAmount(null), null).getItemStack())));
		}
		// add rent item
		setPersistentItem(new ClickeableItem(new ItemData("rent", 49, Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_UNRENTEDRENTITEMNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_UNRENTEDRENTITEMLORE.getLines(rentable.getMessageReplacers(true, false, player)))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// right-click, rent shop
				if (clickType.equals(ClickType.RIGHT)) {
					// someone else was faster
					if (rentable.isRented()) {
						player.closeInventory();
						return;
					}
					// not rentable anymore
					if (!rentable.isRentable()) {
						player.closeInventory();
						return;
					}
					// can't rent (conditions)
					if (!rentable.areRentConditionsValid(player, true)) {
						player.closeInventory();
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
						return;
					}
					// too many rented shops
					if (rentable instanceof Shop) {
						if (SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), Utils.asList(Rentable.class)).size() > SupremeShops.inst().getModuleManager().getMaxRentedShops(player)) {
							SSLocale.MSG_SUPREMESHOPS_MAXRENTEDSHOPS.send(player);
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
					}
					// too many rented merchants
					else if (rentable instanceof Merchant) {
						if (SupremeShops.inst().getData().getMerchants().getElementsByOwner(new UserInfo(player), Utils.asList(Rentable.class)).size() > SupremeShops.inst().getModuleManager().getMaxRentedMerchants(player)) {
							SSLocale.MSG_SUPREMESHOPS_MAXRENTEDMERCHANTS.send(player);
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
					}
					// ensure he still has everything needed
					for (TradeObject object : rentable.getRentPrice()) {
						double objectAm = object.getCustomAmount(player);
						if (object.getTradesForStock(object.getPlayerStock(player), objectAm) < 1) {
							object.sendHasntMessage(player, objectAm);
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
					}
					// pay
					for (TradeObject object : rentable.getRentPrice()) {
						object.take(player, object.getCustomAmount(player));
					}
					// start renting
					rentable.changeRent(new UserInfo(player), 1, true);
					// done
					rentable.openGUI(fromMerchant, player, fromGUI, fromGUIPageIndex);// don't use this parent GUI of course
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			}
		});
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
