package com.guillaumevdn.supremeshops.gui.rentable.management;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.rentable.RentableGUI;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;

public class RentableEditRentConditionsGUI extends FilledGUI implements RentableGUI {

	// amount
	private Rentable rentable;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public RentableEditRentConditionsGUI(Rentable rentable, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTCONDITIONSGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.rentable = rentable;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		for (final Condition condition : rentable.getRentConditions().getConditions().getElements().values()) {
			// add stock in lore
			List<String> lore = SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONDITIONLORE.getLines("{description}", condition.describe(player));
			// add controls in lore
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLEDITCONDITIONERRORMESSAGELORE.getLines());
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLREMOVECONDITIONLORE.getLines());
			// set item
			setRegularItem(new ClickeableItem(new ItemData("condition_" + condition.getId(), -1, condition.getType().getIcon(), 1, "§6" + condition.getId(), lore)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : edit error message
					if (clickType.equals(ClickType.RIGHT)) {
						player.closeInventory();
						SSLocale.MSG_SUPREMESHOPS_CONDITIONERRORMESSAGEINPUT.send(player);
						GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
							@Override
							public void onChat(Player player, String value) {
								// cancel
								if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
									RentableEditRentConditionsGUI.this.open(player);
									return;
								}
								// forbidden words
								List<String> forbidden = SupremeShops.inst().getConfiguration().getListFormatted("player_input_blacklist", Utils.emptyList());
								String[] words = Utils.format(value).toLowerCase().split(" ");
								for (String word : words) {
									for (String forb : forbidden) {
										int similarity = Utils.getLevenshteinSimilarity(word, forb);
										if (similarity < 3) {
											SSLocale.MSG_SUPREMESHOPS_BLACKLISTEDPLAYERINPUT.send(player, "{word}", word, "{similar}", forb);
											RentableEditRentConditionsGUI.this.open(player);
											// sound
											if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
												SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
											}
											return;
										}
									}
								}
								// set error message
								condition.getErrorMessage().setValue(Utils.asList(value));
								rentable.pushAsync();
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// send message and open from GUI
								SSLocale.MSG_SUPREMESHOPS_CONDITIONERRORMESSAGECHANGED.send(player, "{message}", value);
								RentableEditRentConditionsGUI.this.open(player);
								// log
								SupremeShops.inst().pluginLog(rentable, player, null, "Set error message for rent condition " + condition.getId() + " to " + value);
							}
						});
					}
					// shift + right-click : remove condition
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						rentable.removeRentCondition(condition);
						RentableEditRentConditionsGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						// log
						SupremeShops.inst().pluginLog(rentable, player, null, "Removed rent condition " + condition.describe(player));
					}
				}
			});
		}
		// add condition item
		setPersistentItem(new ClickeableItem(new ItemData("add_condition", 46, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDCONDITIONNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// select type
				int size = Utils.getInventorySize(ConditionType.values().size());
				GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDCONDITIONSELECTTYPE.getLine(), size, GUI.getRegularItemSlots(0, size - 3));
				for (final ConditionType type : ConditionType.values()) {
					// can't add
					if (!type.getLogic().canAddToRentableRentConditions(player, rentable)) continue;
					// attempt to add
					typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.getId(), -1, type.getIcon(), 1, "§a" + Utils.capitalizeFirstLetter(type.getName()), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							type.getLogic().attemptToAddToRentableRentConditions(player, rentable, RentableEditRentConditionsGUI.this);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
				// add back item
				if (fromGUI != null) {
					typeGui.setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(typeGui.getSize() - 2)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							RentableEditRentConditionsGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
				// open gui
				typeGui.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// required valid item
		setPersistentItem(new ClickeableItem(new ItemData("required_valid", 48, EditorGUI.ICON_NUMBER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITREQUIREDVALIDCONDITIONSLORE.getLines("{current}", rentable.getRentConditions().getRequiredValid().getParsedValue(player)))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
				// enter amount in chat
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_CONDITIONREQUIREDVALIDINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// not a number
						final Integer amount = Utils.integerOrNull(value);
						if (amount == null || amount <= 0) {
							GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// set value and push
						rentable.getRentConditions().getRequiredValid().setValue(Utils.asList("" + amount));
						rentable.pushAsync();
						// sound
						if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
							SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
						}
						// send message and open from GUI
						SSLocale.MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDVALID.send(player);
						if (fromGUI != null) fromGUI.open(player);
						// log
						SupremeShops.inst().pluginLog(rentable, player, null, "Set rent conditions required valid to " + amount);
					}
				});
			}
		});
		// required not valid item
		setPersistentItem(new ClickeableItem(new ItemData("required_not_valid", 49, EditorGUI.ICON_NUMBER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITREQUIREDNOTVALIDCONDITIONSLORE.getLines("{current}", rentable.getRentConditions().getRequiredNotValid().getParsedValue(player)))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
				// enter amount in chat
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_CONDITIONREQUIREDNOTVALIDINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// not a number
						final Integer amount = Utils.integerOrNull(value);
						if (amount == null || amount <= 0) {
							GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// set value and push
						rentable.getRentConditions().getRequiredNotValid().setValue(Utils.asList("" + amount));
						rentable.pushAsync();
						// sound
						if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
							SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
						}
						// send message and open from GUI
						SSLocale.MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDNOTVALID.send(player);
						if (fromGUI != null) fromGUI.open(player);
						// log
						SupremeShops.inst().pluginLog(rentable, player, null, "Set rent conditions required not valid to " + amount);
					}
				});
			}
		});
		// general error message item
		setPersistentItem(new ClickeableItem(new ItemData("general_error_message", 50, EditorGUI.ICON_STRING, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGENAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONDITIONSGENERALERRORMESSAGELORE.getLines("{current}", rentable.getRentConditions().getRequiredNotValid().getParsedValue(player)))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
				// enter amount in chat
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_CONDITIONGENERALERRORMESSAGEINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// set value and push
						rentable.getRentConditions().getGeneralErrorMessage().setValue(Utils.asList(value));
						rentable.pushAsync();
						// sound
						if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
							SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
						}
						// send message and open from GUI
						SSLocale.MSG_SUPREMESHOPS_SETCONDITIONSGENERALERRORMESSAGE.send(player);
						if (fromGUI != null) fromGUI.open(player);
						// log
						SupremeShops.inst().pluginLog(rentable, player, null, "Set rent conditions general error message to " + value);
					}
				});
			}
		});
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
