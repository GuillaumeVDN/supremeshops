package com.guillaumevdn.supremeshops.gui.shop;

import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class AdminShopListGUI extends GUI {

	// base
	public AdminShopListGUI(Collection<Shop> shops, OfflinePlayer owner, Player player) {
		super(SupremeShops.inst(), owner != null ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTOWNERGUINAME.getLine("{owner}", owner.getName()) : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_53);
		// initialize
		for (final Shop shop : shops) {
			// build lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			if (shop instanceof BlockShop) {
				lore.add("");
				lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTLORECONTROLTELEPORT.getLines());
			}
			Replacer replacer = new Replacer(shop.getMessageReplacers(true, false, player));
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(replacer.apply(infoItem.getName()));
			icon.setLore(replacer.apply(infoItem.getLore()));
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// teleport
					if (shop instanceof BlockShop && clickType.equals(ClickType.LEFT)) {
						player.teleport(((BlockShop) shop).getBlock().toLocation().add(0.5d, 1d, 0.5d));
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
	}

}
