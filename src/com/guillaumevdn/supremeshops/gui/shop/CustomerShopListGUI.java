package com.guillaumevdn.supremeshops.gui.shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.ShopsSorter;
import com.guillaumevdn.supremeshops.util.ShopsSorter.SortCriteria;

public class CustomerShopListGUI extends FilledGUI {

	// base
	private UserInfo owner;
	private boolean adminOnly;
	private GUI fromGUI;
	private int fromGUIPageIndex;
	private Player player;
	private long lastChangedCriteria = System.currentTimeMillis();
	private SortCriteria sortCriteria = SortCriteria.BY_NAME;

	public CustomerShopListGUI(UserInfo owner, boolean adminOnly, GUI fromGUI, int fromGUIPageIndex, Player player) {
		super(SupremeShops.inst(), owner != null ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTOWNERGUINAME.getLine("{owner}", owner.toOfflinePlayer().getName()) : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_44);
		this.owner = owner;
		this.adminOnly = adminOnly;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
		this.player = player;
	}

	// get
	public UserInfo getOwner() {
		return owner;
	}

	public boolean isAllowAdmin() {
		return adminOnly;
	}

	public long getLastChangedCriteria() {
		return lastChangedCriteria;
	}

	public SortCriteria getSortCriteria() {
		return sortCriteria;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	public Player getPlayer() {
		return player;
	}

	// fill
	@Override
	protected void fill() {
		// calculate content
		List<Shop> content = new ArrayList<Shop>();
		if (owner == null) {
			if (adminOnly) {
				content.addAll(SupremeShops.inst().getData().getShops().getElements(null, null, true, ElementRemotePolicy.MUST_BE));
			} else {
				content.addAll(SupremeShops.inst().getData().getShops().getAll(null, true, ElementRemotePolicy.MUST_BE));
			}
		} else {
			content.addAll(SupremeShops.inst().getData().getShops().getElements(owner, null, true, ElementRemotePolicy.MUST_BE));
		}
		// sort content
		content = new ShopsSorter(content, sortCriteria).getSortedList();
		// initialize content
		for (final Shop shop : content) {
			// can't trade
			if (!shop.areTradeConditionsValid(player, false)) {
				continue;
			}
			// didn't discover items
			if (SupremeShops.inst().getForceItemDiscover() && !shop.hasDiscoveredAllItems(player, false)) {
				continue;
			}
			// build lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDTRADE.getLines());
			Replacer replacer = new Replacer(shop.getMessageReplacers(true, false, player));
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(replacer.apply(infoItem.getName()));
			icon.setLore(replacer.apply(infoItem.getLore()));
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// preview trade
					if (clickType.equals(ClickType.LEFT)) {
						new TradePreviewGUI(shop, null, 1, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// add sort item
		setPersistentItem(new ClickeableItem(new ItemData("sort", 50, Mat.COMMAND_BLOCK, 1, sortCriteria.getName().getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIALORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// too often
				if (System.currentTimeMillis() - lastChangedCriteria < 500L) {
					return;
				}
				// left-click : by name
				if (clickType.equals(ClickType.LEFT)) {
					sortCriteria = SortCriteria.BY_NAME;
					open(player, pageIndex);
				}
				// right-click : by unique buyers
				else if (clickType.equals(ClickType.RIGHT)) {
					sortCriteria = SortCriteria.BY_UNIQUE_BUYERS;
					open(player, pageIndex);
				}
				// shift + left-click : by ranking (server)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SERVER;
					open(player, pageIndex);
				}
				// shift + right-click : by ranking (seller)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SELLER;
					open(player, pageIndex);
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
