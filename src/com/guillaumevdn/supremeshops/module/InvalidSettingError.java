package com.guillaumevdn.supremeshops.module;

public class InvalidSettingError extends Error {

	private static final long serialVersionUID = -7248691607208646620L;

	public InvalidSettingError() {
		super();
	}

	public InvalidSettingError(String message) {
		super(message);
	}

}
