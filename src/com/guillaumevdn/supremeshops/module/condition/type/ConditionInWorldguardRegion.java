package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.gcorelegacy.lib.util.ServerVersion;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;
import com.guillaumevdn.supremeshops.util.WorldGuardUtils113;
import com.guillaumevdn.supremeshops.util.WorldGuardUtilsLegacy;

public class ConditionInWorldguardRegion extends Condition {

	// base
	private PPString world = addComponent(new PPString("world", this, null, true, 9, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDNAMELORE.getLines()));
	private PPString region = addComponent(new PPString("region", this, null, true, 10, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDGUARDREGIONNAMELORE.getLines()));

	public ConditionInWorldguardRegion(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.IN_WORLDGUARD_REGION, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getWorld() {
		return world;
	}

	public String getWorld(Player parser) {
		return world.getParsedValue(parser);
	}

	public PPString getRegion() {
		return region;
	}

	public String getRegion(Player parser) {
		return region.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLDGUARDREGION.getLine("{world}", getWorld(parser), "{region}", getRegion(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		String world = getWorld(player);
		String region = getRegion(player);
		if (world == null || region == null) return false;
		Location location = player.getLocation();
		return ServerVersion.IS_1_13 ? WorldGuardUtils113.isLocInRegion(location, Bukkit.getWorld(world), region) : WorldGuardUtilsLegacy.isLocInRegion(location, Bukkit.getWorld(world), region);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		if (!(shop instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		String world = getWorld(player);
		String region = getRegion(player);
		if (world == null || region == null) return false;
		Location location = ((Locatable) shop).getBlock().toLocation();
		return ServerVersion.IS_1_13 ? WorldGuardUtils113.isLocInRegion(location, Bukkit.getWorld(world), region) : WorldGuardUtilsLegacy.isLocInRegion(location, Bukkit.getWorld(world), region);
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		if (!(merchant instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		String world = getWorld(parser);
		String region = getRegion(parser);
		if (world == null || region == null) return false;
		Location location = ((Locatable) merchant).getLocation();
		return ServerVersion.IS_1_13 ? WorldGuardUtils113.isLocInRegion(location, Bukkit.getWorld(world), region) : WorldGuardUtilsLegacy.isLocInRegion(location, Bukkit.getWorld(world), region);
	}

	// clone
	protected ConditionInWorldguardRegion() {
		super();
	}

}
