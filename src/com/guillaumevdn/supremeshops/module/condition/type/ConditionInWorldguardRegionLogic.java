package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionInWorldguardRegionLogic extends ConditionLogic<ConditionInWorldguardRegion> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionInWorldguardRegion value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "world", value.getWorld().getValue());
		JsonUtils.writeJsonValue(out, "region", value.getRegion().getValue());
	}

	@Override
	public ConditionInWorldguardRegion readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionInWorldguardRegion condition = new ConditionInWorldguardRegion(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getWorld().setValue(JsonUtils.readJsonValue(in));
		condition.getRegion().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_IN_WORLDGUARD_REGION.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_IN_WORLDGUARD_REGION.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionInWorldguardRegion>.DoneCallback callback) {
		askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONWORLDINPUT, fromGUI, new StringSelectionCallback() {
			@Override
			public void onSelect(final String world) {
				askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONWORLDINPUT, fromGUI, new StringSelectionCallback() {
					@Override
					public void onSelect(final String region) {
						// create condition
						ConditionInWorldguardRegion condition = new ConditionInWorldguardRegion(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getWorld().setValue(Utils.asList(world));
						condition.getRegion().setValue(Utils.asList(region));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

}
