package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ConditionIsAdmin extends Condition {

	// base
	public ConditionIsAdmin(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.IS_ADMIN, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPISADMIN.getLine();
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		return shop.isCurrentOwnerAdmin();
	}
	
	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		return merchant.isCurrentOwnerAdmin();
	}

	// clone
	protected ConditionIsAdmin() {
		super();
	}

}
