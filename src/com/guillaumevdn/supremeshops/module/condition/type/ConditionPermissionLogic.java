package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionPermissionLogic extends ConditionLogic<ConditionPermission> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionPermission value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "permission", value.getPermission().getValue());
		JsonUtils.writeJsonValue(out, "description", value.getDescription().getValue());
	}

	@Override
	public ConditionPermission readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionPermission condition = new ConditionPermission(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getPermission().setValue(JsonUtils.readJsonValue(in));
		condition.getDescription().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PERMISSION.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PERMISSION.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionPermission>.DoneCallback callback) {
		askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONPERMISSIONINPUT, fromGUI, permission -> {
			askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONPERMISSIONDESCRIPTIONINPUT, fromGUI, description -> {
				// create condition
				ConditionPermission condition = new ConditionPermission(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
				condition.getPermission().setValue(Utils.asList(permission));
				condition.getDescription().setValue(Utils.asList(description));
				// done
				callback.callback(condition);
			});
		});
	}

}
