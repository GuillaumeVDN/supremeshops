package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.ShopType;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionShopCountLogic extends ConditionLogic<ConditionShopCount> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionShopCount value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "shop_type", value.getShopType().getValue());
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		JsonUtils.writeJsonValue(out, "amount", value.getAmount().getValue());
	}

	@Override
	public ConditionShopCount readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionShopCount condition = new ConditionShopCount(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getShopType().setValue(JsonUtils.readJsonValue(in));
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		condition.getAmount().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_SHOP_COUNT.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return false;
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionShopCount>.DoneCallback callback) {
		askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONSHOPTYPEINPUT, fromGUI, ShopType.class, new EnumSelectionCallback<ShopType>() {
			@Override
			public void onSelect(final ShopType value) {
				askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONSHOPAMOUNTINPUT, 0, Integer.MAX_VALUE, fromGUI, new NumberSelectionCallback() {
					@Override
					public void onSelect(final int amount) {
						askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT, fromGUI, ConditionShopCount.Operation.class, new EnumSelectionCallback<ConditionShopCount.Operation>() {
							@Override
							public void onSelect(final ConditionShopCount.Operation operation) {
								// create condition
								ConditionShopCount condition = new ConditionShopCount(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
								condition.getShopType().setValue(Utils.asList(value.name()));
								condition.getAmount().setValue(Utils.asList("" + amount));
								condition.getOperation().setValue(Utils.asList(operation.name()));
								// done
								callback.callback(condition);
							}
						});
					}
				});
			}
		});
	}

}
