package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionShopObjectCountLogic extends ConditionLogic<ConditionShopObjectCount> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionShopObjectCount value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "object_side", value.getObjectSide().getValue());
		JsonUtils.writeJsonValue(out, "object_type", value.getObjectType().getValue());
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		JsonUtils.writeJsonValue(out, "amount", value.getAmount().getValue());
	}

	@Override
	public ConditionShopObjectCount readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionShopObjectCount condition = new ConditionShopObjectCount(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getObjectSide().setValue(JsonUtils.readJsonValue(in));
		condition.getObjectType().setValue(JsonUtils.readJsonValue(in));
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		condition.getAmount().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return false;
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return false;
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionShopObjectCount>.DoneCallback callback) {
		throw new UnsupportedOperationException();
	}

}
