package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.ShopType;

public class ConditionShopType extends Condition {

	// base
	private PPEnum<ShopType> shopType = addComponent(new PPEnum<ShopType>("shop_type", this, ShopType.BLOCK.name(), ShopType.class, "shop type", true, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_SHOPTYPELORE.getLines()));

	public ConditionShopType(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.SHOP_TYPE, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<ShopType> getShopType() {
		return shopType;
	}

	public ShopType getShopType(Player parser) {
		return shopType.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPTYPE.getLine("{type}", getShopType(parser).toString().replace("_", " ").toLowerCase());
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		ShopType type = getShopType(player);
		if (type == null) return false;
		return type.equals(shop.getType());
	}

	// clone
	protected ConditionShopType() {
		super();
	}

}
