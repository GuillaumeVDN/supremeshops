package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionTokenEnchantTokensLogic extends ConditionLogic<ConditionTokenEnchantTokens> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionTokenEnchantTokens value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		JsonUtils.writeJsonValue(out, "points", value.getPoints().getValue());
	}

	@Override
	public ConditionTokenEnchantTokens readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionTokenEnchantTokens condition = new ConditionTokenEnchantTokens(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		condition.getPoints().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return Utils.isPluginEnabled("TokenEnchant") && SSPerm.SUPREMESHOPS_CREATE_CONDITION_TOKENENCHANT_TOKENS.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return Utils.isPluginEnabled("TokenEnchant") && SSPerm.SUPREMESHOPS_CREATE_CONDITION_TOKENENCHANT_TOKENS.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return Utils.isPluginEnabled("TokenEnchant");
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionTokenEnchantTokens>.DoneCallback callback) {
		askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONTOKENSINPUT, 0, Integer.MAX_VALUE, fromGUI, new NumberSelectionCallback() {
			@Override
			public void onSelect(final int points) {
				askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT, fromGUI, ConditionTokenEnchantTokens.Operation.class, new EnumSelectionCallback<ConditionTokenEnchantTokens.Operation>() {
					@Override
					public void onSelect(final ConditionTokenEnchantTokens.Operation operation) {
						// create condition
						ConditionTokenEnchantTokens condition = new ConditionTokenEnchantTokens(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getPoints().setValue(Utils.asList("" + points));
						condition.getOperation().setValue(Utils.asList(operation.name()));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

}
