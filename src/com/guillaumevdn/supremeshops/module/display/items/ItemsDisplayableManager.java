package com.guillaumevdn.supremeshops.module.display.items;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Item;

import com.guillaumevdn.supremeshops.SupremeShops;

public class ItemsDisplayableManager {

	// base
	public static final String ITEM_MARK_BEGIN = "SSDISPLAY-";
	private CalculateTask calculateTask = null;
	private ProcessTask processTask = null;
	private ConcurrentHashMap<ItemsDisplayable, ItemsDisplayableData> data = new ConcurrentHashMap<>();

	// get
	public CalculateTask getCalculateTask() {
		return calculateTask;
	}

	public Map<ItemsDisplayable, ItemsDisplayableData> getData() {
		return data;
	}

	public boolean isDisplayItem(Item item) {
		for (ItemsDisplayableData data : data.values()) {
			if (data.getItems().contains(item)) {
				return true;
			}
		}
		return false;
	}

	public ItemsDisplayableData getData(ItemsDisplayable displayable) {
		ItemsDisplayableData displayableData = data.get(displayable);
		if (displayableData == null) data.put(displayable, displayableData = new ItemsDisplayableData(displayable));
		return displayableData;
	}

	// methods
	public void restart() {
		// stop previous
		stop();
		// task and events
		(calculateTask = new CalculateTask(this, SupremeShops.inst().getConfiguration().getDouble("display_items_distance_tolerance", 20.0))).runTaskTimer(SupremeShops.inst(), 0L, 5L);
		//(calculateTask = new CalculateTask(this, SupremeShops.inst().getConfiguration().getDouble("display_items_distance_tolerance", 20.0))).runTaskTimerAsynchronously(SupremeShops.inst(), 0L, 5L);
		(processTask = new ProcessTask(this, SupremeShops.inst().getConfiguration().getInt("display_items_process_tick", 5))).runTaskTimer(SupremeShops.inst(), 0L, 1L);
	}

	public void stop() {
		// clear data
		data.clear();
		// task and events
		if (calculateTask != null) {
			calculateTask.cancel();
			calculateTask = null;
		}
		if (processTask != null) {
			processTask.cancel();
			processTask = null;
		}
	}

	public void stop(ItemsDisplayable displayable) {
		ItemsDisplayableData displayableData = data.get(displayable);
		if (displayableData != null) {
			displayableData.setAwaitingOperation(true);
			displayableData.getAwaitingItems().clear();
		}
	}

}
