package com.guillaumevdn.supremeshops.module.display.particles;

import com.guillaumevdn.supremeshops.SupremeShops;

public class DisplayParticlesManager {

	// base
	private SingleParticlesDisplayablesTask singleParticlesDisplayablesTask = null;
	private MultipleParticlesDisplayableTask multipleParticlesDisplayablesTask = null;

	// get
	public SingleParticlesDisplayablesTask getSingleParticleDisplayablesTask() {
		return singleParticlesDisplayablesTask;
	}

	public MultipleParticlesDisplayableTask getMultipleParticleDisplayablesTask() {
		return multipleParticlesDisplayablesTask;
	}

	// methods
	public void restartTasks() {
		// stop previous tasks
		stopTasks();
		// start new tasks
		(singleParticlesDisplayablesTask = new SingleParticlesDisplayablesTask()).runTaskTimerAsynchronously(SupremeShops.inst(), 0L, 1L);
		(multipleParticlesDisplayablesTask = new MultipleParticlesDisplayableTask()).runTaskTimerAsynchronously(SupremeShops.inst(), 0L, 1L);
	}

	public void stopTasks() {
		if (singleParticlesDisplayablesTask != null) {
			singleParticlesDisplayablesTask.stop();
			singleParticlesDisplayablesTask = null;
		}
		if (multipleParticlesDisplayablesTask != null) {
			multipleParticlesDisplayablesTask.stop();
			multipleParticlesDisplayablesTask = null;
		}
	}

}
