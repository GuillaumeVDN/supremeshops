package com.guillaumevdn.supremeshops.module.display.sign;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;

public interface SignDisplayable {

	BlockCoords getSign();
	List<String> getSignLines();

}
