package com.guillaumevdn.supremeshops.module.display.sign;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SignDisplayableManager {

	// methods
	public void refreshSign(SignDisplayable displayable) {
		BlockCoords sign = displayable.getSign();
		Block block = sign.getWorld().getBlockAt(sign.getX(), sign.getY(), sign.getZ());
		if (Utils.instanceOf(block.getState(), Sign.class)) {
			// update sign
			Sign signState = (Sign) block.getState();
			List<String> lines = displayable.getSignLines();
			for (int i = 0; i < 4; ++i) {
				signState.setLine(i, i < lines.size() ? lines.get(i) : "");
			}
			signState.update(true);
		}
	}

}
