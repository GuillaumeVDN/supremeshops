package com.guillaumevdn.supremeshops.module.gui;

import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;

public interface PostFillCallback {

	boolean postFill(FilledGUI gui);

}
