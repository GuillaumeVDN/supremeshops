package com.guillaumevdn.supremeshops.module.itemvalue;

public class InvalidSettingError extends Error {

	private static final long serialVersionUID = -8244191662755628865L;
	
	public InvalidSettingError(String message) {
		super(message);
	}

}
