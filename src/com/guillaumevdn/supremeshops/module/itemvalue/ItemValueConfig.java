package com.guillaumevdn.supremeshops.module.itemvalue;

import java.util.HashMap;
import java.util.Map;

import com.guillaumevdn.supremeshops.module.object.ObjectSide;

public class ItemValueConfig {

	// base
	private String key;
	private Map<ObjectSide, Double> defaultValues = new HashMap<>();
	private Map<ObjectSide, Double> minValues = new HashMap<>();
	private Map<ObjectSide, Double> maxValues = new HashMap<>();
	private Map<ObjectSide, String> postTradeModifiers = new HashMap<>();
	private Map<ObjectSide, String> postOppositeTradeModifiers = new HashMap<>();
	private Map<ObjectSide, String> perMinuteModifiers = new HashMap<>();

	public ItemValueConfig(String key) {
		this.key = key;
	}

	// get
	public String getKey() {
		return key;
	}

	public Map<ObjectSide, Double> getDefaultValues() {
		return defaultValues;
	}

	public Map<ObjectSide, Double> getMinValues() {
		return minValues;
	}

	public Map<ObjectSide, Double> getMaxValues() {
		return maxValues;
	}

	public Map<ObjectSide, String> getPostTradeModifiers() {
		return postTradeModifiers;
	}

	public Map<ObjectSide, String> getPostOppositeTradeModifiers() {
		return postOppositeTradeModifiers;
	}

	public Map<ObjectSide, String> getPerMinuteModifiers() {
		return perMinuteModifiers;
	}

	public boolean isEmpty() {
		return defaultValues.isEmpty() && minValues.isEmpty() && maxValues.isEmpty() && postTradeModifiers.isEmpty() && perMinuteModifiers.isEmpty();
	}

}
