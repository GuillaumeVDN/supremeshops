package com.guillaumevdn.supremeshops.module.itemvalue;

public enum ItemValueSetting {

	DISABLED,
	RECOMMENDED,
	ENABLED

}
