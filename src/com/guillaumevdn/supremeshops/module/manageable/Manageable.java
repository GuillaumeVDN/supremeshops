package com.guillaumevdn.supremeshops.module.manageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public interface Manageable<P extends ManagementPermission> {

	UserInfo getCurrentOwner();
	boolean isCurrentOwnerAdmin();
	List<P> getAllPermissions();
	Map<UserInfo, Set<P>> getManagers();
	Map<UserInfo, Map<TradeObject, Double>> getManagersWages();
	Map<UserInfo, Long> getLastPaidManagersWages();
	Map<UserInfo, Integer> getLateManagersWages();
	Set<P> getManagementPermissions(OfflinePlayer player);
	Set<P> getManagementPermissions(UserInfo player);
	boolean hasManagementPermission(Player player, P permission);
	Long getLastPaidManagerWage(UserInfo manager);
	Integer getLateManagerWages(UserInfo manager);
	Map<TradeObject, Double> getManagerWage(UserInfo manager);
	void changeModifyManagerWageStock(UserInfo manager, TradeObject object, double delta);
	double getTotalObjectStockInManagersWages(List<ObjectType> types);
	Object[] getMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser);

	void addManager(UserInfo manager);
	void addToManagerWage(UserInfo manager, TradeObject wageObject, Double stock);
	void removeManager(UserInfo manager);
	void removeManagerWage(UserInfo manager, TradeObject wageObject);
	void changeManagementPermission(Player player, P permission, boolean has);
	void changeManagementPermission(UserInfo manager, P permission, boolean has);
	void changeManagementPermissions(Player player, Set<P> permissions);
	void changeManagementPermissions(UserInfo manager, Set<P> permissions);
	void changeLateManagerWages(UserInfo manager, int late, boolean push);
	int checkToPayWages(boolean push);
	boolean checkToPayWage(UserInfo manager, boolean push);
	int checkToPayLateWages(boolean push);
	boolean checkToPayLateWages(UserInfo manager, boolean push);
	boolean payWage(UserInfo manager, boolean push);

}
