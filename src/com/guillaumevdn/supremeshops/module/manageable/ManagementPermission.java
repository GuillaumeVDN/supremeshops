package com.guillaumevdn.supremeshops.module.manageable;

import com.guillaumevdn.gcorelegacy.lib.Perm;

public interface ManagementPermission {

	boolean canEditInGui();
	Perm getPermission();
	String name();

}
