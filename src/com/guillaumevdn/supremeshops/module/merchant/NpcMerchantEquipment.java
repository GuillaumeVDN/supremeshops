package com.guillaumevdn.supremeshops.module.merchant;

import org.bukkit.OfflinePlayer;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;

public class NpcMerchantEquipment {

	// base
	private String id;
	private Perm permission;
	private ItemData helmet, chestplate, leggings, boots, mainHand, offHand;
	private ItemData icon;

	public NpcMerchantEquipment(String id, ItemData icon, Perm permission, ItemData helmet, ItemData chestplate, ItemData leggings, ItemData boots, ItemData offHand, ItemData mainHand) {
		this.id = id;
		this.permission = permission;
		this.icon = icon;
		this.helmet = helmet;
		this.chestplate =chestplate;
		this.leggings = leggings;
		this.boots = boots;
		this.offHand = offHand;
		this.mainHand = mainHand;
	}

	// get
	public String getId() {
		return id;
	}

	public Perm getPermission() {
		return permission;
	}

	public boolean hasPermission(OfflinePlayer player) {
		return permission == null || permission.has(player);
	}
	
	public ItemData getIcon() {
		return icon;
	}

	public ItemData getHelmet() {
		return helmet;
	}

	public ItemData getChestplate() {
		return chestplate;
	}

	public ItemData getLeggings() {
		return leggings;
	}

	public ItemData getBoots() {
		return boots;
	}

	public ItemData getMainHand() {
		return mainHand;
	}

	public ItemData getOffHand() {
		return offHand;
	}

}
