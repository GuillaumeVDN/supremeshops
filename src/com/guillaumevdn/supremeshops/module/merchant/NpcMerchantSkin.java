package com.guillaumevdn.supremeshops.module.merchant;

import org.bukkit.OfflinePlayer;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;

public class NpcMerchantSkin {

	// base
	private String id, data, signature;
	private Perm permission;
	private ItemData icon;

	public NpcMerchantSkin(String id, ItemData icon, Perm permission, String data, String signature) {
		this.id = id;
		this.permission = permission;
		this.icon = icon;
		this.data = data;
		this.signature = signature;
	}

	// get
	public String getId() {
		return id;
	}

	public Perm getPermission() {
		return permission;
	}

	public boolean hasPermission(OfflinePlayer player) {
		return permission == null || permission.has(player);
	}

	public ItemData getIcon() {
		return icon;
	}

	public String getData() {
		return data;
	}

	public String getSignature() {
		return signature;
	}

}
