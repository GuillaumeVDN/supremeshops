package com.guillaumevdn.supremeshops.module.merchant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.WeekDay;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedRentConditionsEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedRentPriceEvent;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.fake.FakeRentableBlockMerchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.MerchantShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.RentPeriod;
import com.guillaumevdn.supremeshops.util.UserOperator;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;

public class RentableBlockMerchant extends BlockMerchant implements Rentable {

	// base
	private UserInfo currentOwner = null;
	private int pastPaidRents = 0;
	private int paidRents = 0;
	private long lastCheckRent = 0L;
	private boolean awaitingForceUnrent = false;
	private boolean rentable = false;
	private List<TradeObject> rentPrice = new ArrayList<TradeObject>();
	private RentPeriod rentPeriod = RentPeriod.MONTHLY;
	private int rentPeriodStreakLimit = -1;
	private int rentPeriodStreakLimitDelay = -1;
	private CPConditions rentConditions = new CPConditions("rentConditions", null, false, -1, null, null);
	private String unrentedParticlePatternId = null;

	private RentableBlockMerchant(String id) {
		super(id, MerchantType.RENTABLE_BLOCK);
	}

	public RentableBlockMerchant(String id, BlockCoords block, BlockCoords sign) {
		super(id, MerchantType.RENTABLE_BLOCK, null, block, sign);
	}

	// rentable
	@Override
	public UserInfo getCurrentOwner() {
		return currentOwner;
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return false;
	}

	@Override
	public boolean isRented() {
		return currentOwner != null;
	}

	@Override
	public int getPastPaidRents() {
		return pastPaidRents;
	}

	@Override
	public int getPaidRents() {
		return paidRents;
	}

	@Override
	public boolean isRentable() {
		return rentable;
	}

	@Override
	public long getLastCheckRent() {
		return lastCheckRent;
	}

	@Override
	public boolean isAwaitingForceUnrent() {
		return awaitingForceUnrent;
	}

	@Override
	public List<TradeObject> getRentPrice() {
		return rentPrice;
	}

	@Override
	public RentPeriod getRentPeriod() {
		return rentPeriod;
	}

	@Override
	public int getRentPeriodStreakLimit() {
		return rentPeriodStreakLimit;
	}

	@Override
	public int getRentPeriodStreakLimitDelay() {
		return rentPeriodStreakLimitDelay;
	}

	@Override
	public CPConditions getRentConditions() {
		return rentConditions;
	}

	@Override
	public boolean areRentConditionsValid(Player player, boolean errorMessage) {
		return rentConditions.isValid(player, this, errorMessage);
	}

	@Override
	public String getUnrentedParticlePatternId() {
		return unrentedParticlePatternId;
	}

	@Override
	public ParticlePattern getUnrentedParticlePattern() {
		return SupremeShops.inst().getModuleManager().getParticlePattern(unrentedParticlePatternId);
	}

	@Override
	public List<String> getSignLines() {
		return SSLocaleMisc.MISC_SUPREMESHOPS_UNRENTEDBLOCKMERCHANTSIGNLINES.getLines(getMessageReplacers(true, false, null));
	}

	@Override
	public ParticlePatternAvailability getRentableParticlePatternAvailability() {
		return ParticlePatternAvailability.BLOCK;
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isRentable();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return isRented() ? getParticlePattern() : getUnrentedParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return isRented() ? areInteractConditionsValid(player, false) : areRentConditionsValid(player, false);
	}

	// set
	protected void setCurrentOwner(UserInfo currentOwner) {
		this.currentOwner = currentOwner;
	}

	protected void setPastPaidRents(int pastPaidRents) {
		this.pastPaidRents = pastPaidRents;
	}

	protected void setPaidRents(int paidRents) {
		this.paidRents = paidRents;
	}

	protected void setRentable(boolean rentable) {
		this.rentable = rentable;
	}

	protected void setRentPrice(List<TradeObject> rentPrice) {
		this.rentPrice = rentPrice;
	}

	protected void setRentPeriod(RentPeriod rentPeriod) {
		this.rentPeriod = rentPeriod;
	}

	protected void setRentPeriodStreakLimit(int rentPeriodStreakLimit) {
		this.rentPeriodStreakLimit = rentPeriodStreakLimit;
	}

	protected void setRentPeriodStreakLimitDelay(int rentPeriodStreakLimitDelay) {
		this.rentPeriodStreakLimitDelay = rentPeriodStreakLimitDelay;
	}

	protected void setRentConditions(CPConditions rentConditions) {
		this.rentConditions = rentConditions;
	}

	protected void setUnrentedParticlePatternId(String unrentedParticlePatternId) {
		this.unrentedParticlePatternId = unrentedParticlePatternId;
	}

	protected void setLastCheckRent(long lastCheckRent) {
		this.lastCheckRent = lastCheckRent;
	}

	protected void setAwaitingForceUnrent(boolean awaitingForceUnrent) {
		this.awaitingForceUnrent = awaitingForceUnrent;
	}

	// methods
	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// current owner
		replacers.add("{current_owner}");
		replacers.add(currentOwner == null ? "/" : currentOwner.toOfflinePlayer().getName());
		// rent time
		replacers.add("{rent_time}");
		replacers.add(currentOwner == null ? "/" : pastPaidRents);
		// rent limit
		replacers.add("{rent_limit}");
		replacers.add(currentOwner == null || rentPeriodStreakLimit <= 0 ? "∞" : rentPeriodStreakLimit);
		// paid rents in advance
		replacers.add("{paid_rents}");
		replacers.add(currentOwner == null ? "/" : paidRents);
		// rent period
		replacers.add("{rent_period}");
		replacers.add(rentPeriod.getName().getLine());
		// rented for
		replacers.add("{rented_for}");
		replacers.add(currentOwner == null ? "/" : paidRents);
		// done
		return replacers;
	}

	// methods : change
	@Override
	public void addRentPrice(TradeObject priceObject) {
		if (!rentPrice.contains(priceObject)) {
			rentPrice.add(priceObject);
			pushAsync();
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedRentPriceEvent(this, priceObject, MerchantUpdatedRentPriceEvent.Operation.ADD_OBJECT));
		}
	}

	@Override
	public void removeRentPrice(TradeObject priceObject) {
		if (rentPrice.remove(priceObject)) {
			pushAsync();
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedRentPriceEvent(this, priceObject, MerchantUpdatedRentPriceEvent.Operation.REMOVE_OBJECT));
		}
	}

	@Override
	public void addRentCondition(Condition condition) {
		if (rentConditions.getConditions().getElement(condition.getId()) == null) {
			rentConditions.getConditions().addElement(condition);
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedRentConditionsEvent(this, condition, MerchantUpdatedRentConditionsEvent.Operation.ADD_CONDITION));
			pushAsync();
		}
	}

	@Override
	public void removeRentCondition(Condition condition) {
		if (rentConditions.getConditions().removeElement(condition.getId()) != null) {
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedRentConditionsEvent(this, condition, MerchantUpdatedRentConditionsEvent.Operation.REMOVE_CONDITION));
			pushAsync();
		}
	}

	@Override
	public void changeRentable(boolean rentable) {
		// set
		setRentable(rentable);
		pushAsync();
		// close GUIs
		if (!rentable) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, false);
		}
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.TOGGLE_RENTABLE));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, (rentable ? "Set rentable" : "Set not rentable"));
	}

	@Override
	public void changeRentPeriod(RentPeriod period) {
		// set
		setRentPeriod(period);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_RENT_PERIOD));
		// message owner
		Player onlineOwner = currentOwner == null ? null : currentOwner.toPlayer();
		if (onlineOwner != null) {
			SSLocale.MSG_SUPREMESHOPS_MERCHANTCHANGEDRENTPERIOD.send(onlineOwner, "{merchant}", getDisplayName(), "{rent_period}", period.getName());
		}
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set rent period to " + period.name());
	}

	@Override
	public void changeRentPeriodStreakLimit(int limit) {
		// set
		setRentPeriodStreakLimit(limit);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_RENT_PERIOD_STREAK_LIMIT));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set rent period streak limit to " + limit);
	}

	@Override
	public void changeRentPeriodStreakLimitDelay(int delay) {
		// set
		setRentPeriodStreakLimitDelay(delay);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_RENT_PERIOD_STREAK_LIMIT_DELAY));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set rent period streak limit delay to " + delay);
	}

	@Override
	public void changeUnrentedParticlePattern(String particlePatternId) {
		// set particle pattern id
		setUnrentedParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_UNRENTED_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, particlePatternId == null ? "Removed unrented particle pattern" : "Set unrented particle pattern to " + particlePatternId);
	}

	@Override
	public void changePaidRents(int paidRents) {
		// set
		if (this.paidRents > 0 && pastPaidRents <= rentPeriodStreakLimit) {
			awaitingForceUnrent = false;
		}
		setPaidRents(paidRents);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_PAID_RENTS));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set paid rents to " + paidRents);
	}

	@Override
	public void changeRent(UserInfo newOwner, int alreadyPaidForRents, boolean push) {
		// there's currently someone renting the merchant
		if (currentOwner != null) {
			// refund paid rent prices
			OfflinePlayer owner = currentOwner.toOfflinePlayer();
			Player onlineOwner = owner.getPlayer();
			if (paidRents > 0) {
				for (TradeObject object : rentPrice) {
					object.forceWithdraw(owner, paidRents * object.getCustomAmount(onlineOwner));
				}
			}
			// delete all merchant shops
			for (Shop shop : getShops(Utils.asList(MerchantShop.class), false)) {
				// withdraw stock
				for (TradeObject object : shop.getObjects()) {
					object.forceWithdrawAll(owner, shop);
				}
				// delete shop
				shop.destroy(DestroyCause.MERCHANT_UNRENTED, true);
			}
			// notify owner
			if (onlineOwner != null) {
				SSLocale.MSG_SUPREMESHOPS_UNRENTEDMERCHANT.send(onlineOwner, "{merchant}", getDisplayName());
			}
		}
		UserInfo oldOwner = currentOwner;
		// reset rent settings
		setCurrentOwner(newOwner);
		setPaidRents(0);
		setPastPaidRents(alreadyPaidForRents);
		// reset merchant settings
		setParticlePatternId(null);
		clearAllManagers();
		setDisplayName(getId());
		clearShopsIds();
		setOpen(false);
		setRemote(false);
		setTradesLimit(-1);
		setAwaitingForceUnrent(false);
		setLastCheckRent(System.currentTimeMillis());
		// update in merchant board
		SupremeShops.inst().getData().getMerchants().updateByOwner(this, oldOwner);
		// update last rent end
		if (oldOwner != null) {
			new UserOperator(oldOwner) {
				@Override
				protected void process(SSUser user) {
					user.setLastRentEnd(RentableBlockMerchant.this, System.currentTimeMillis());
				}
			}.operate();
		}
		// close all GUIs for this merchant
		SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, true);
		// push
		if (push) {
			pushAsync();
		}
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_CURRENT_OWNER));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set current owner to " + (newOwner == null ? "/" : newOwner.toStringName()));
	}

	// methods : pay rent
	public static final long SECOND_MILLIS = 1000L;
	public static final long MINUTE_MILLIS = 60L * SECOND_MILLIS;
	public static final long HOUR_MILLIS = 60L * MINUTE_MILLIS;
	public static final long DAY_MILLIS = 24L * HOUR_MILLIS;
	public static final long WEEK_MILLIS = 7L * DAY_MILLIS;

	@Override
	public void checkToUpdateRent(boolean push) {
		// not rented
		if (!isRented()) {
			return;
		}
		// is already awaiting something
		if (awaitingForceUnrent) {
			return;
		}
		// daily
		if (RentPeriod.DAILY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't today, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// weekly
		else if (RentPeriod.WEEKLY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = WeekDay.getCurrent().ordinal();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this week, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// monthly
		if (RentPeriod.MONTHLY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = calendar.get(Calendar.DAY_OF_MONTH);
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this month, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// nothing to pay
		return;
	}

	@Override
	public void updateRent(boolean push) {
		// couldn't pay already
		if (awaitingForceUnrent) {
			return;
		}
		// can't pay
		if (paidRents <= 0) {
			lastCheckRent = System.currentTimeMillis();
			awaitingForceUnrent = true;
			// event
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.UPDATED_RENT));
			return;
		}
		// pay
		++pastPaidRents;
		--paidRents;
		lastCheckRent = System.currentTimeMillis();
		// is above paid rents streak limit
		if (pastPaidRents > rentPeriodStreakLimit) {
			awaitingForceUnrent = true;
			// event
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.UPDATED_RENT));
			return;
		}
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.UPDATED_RENT));
		// push
		if (push) pushAsync();
		return;
	}

	// methods : misc
	@Override
	public void openGUI(Merchant fromMerchant, Player player, GUI fromGUI, int fromGUIPageIndex) {
		buildGui(player, fromGUI, fromGUIPageIndex).open(player);
	}

	// fake
	@Override
	public RentableBlockMerchant asFake() {
		return new FakeRentableBlockMerchant(this);
	}

	// data
	public static class RentableBlockMerchantJsonData {

		private final UserInfo owner;
		private final BlockCoords block;
		private final BlockCoords sign;
		private final String displayName;
		private final List<String> shopsIds;
		private final CPConditions interactConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final String particlePatternId;
		private final Map<UserInfo, Set<MerchantManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final UserInfo currentOwner;
		private final int pastPaidRents;
		private final int paidRents;
		private final long lastCheckRent;
		private final boolean awaitingForceUnrent;
		private final boolean rentable;
		private final List<TradeObject> rentPrice;
		private final RentPeriod rentPeriod;
		private final int rentPeriodStreakLimit;
		private final int rentPeriodStreakLimitDelay;
		private final CPConditions rentConditions;
		private final String unrentedParticlePatternId;

		public RentableBlockMerchantJsonData(RentableBlockMerchant merchant) {
			this.owner = merchant.getActualOwner();
			this.block = merchant.getBlock();
			this.sign = merchant.getSign();
			this.displayName = merchant.getDisplayName();
			this.shopsIds = merchant.getShopsIds();
			this.interactConditions = merchant.getInteractConditions();
			this.open = merchant.isOpen();
			this.remote = merchant.isRemote();
			this.tradesLimit = merchant.getTradesLimit();
			this.particlePatternId = merchant.getParticlePatternId();
			this.managers = merchant.getManagers();
			this.managersWages = merchant.getManagersWages();
			this.lastPaidManagersWages = merchant.getLastPaidManagersWages();
			this.lateManagersWages = merchant.getLateManagersWages();
			this.buyers = merchant.getBuyers();
			this.currentOwner = merchant.getCurrentOwner();
			this.pastPaidRents = merchant.getPastPaidRents();
			this.paidRents = merchant.getPaidRents();
			this.lastCheckRent = merchant.getLastCheckRent();
			this.awaitingForceUnrent = merchant.isAwaitingForceUnrent();
			this.rentable = merchant.isRentable();
			this.rentPrice = merchant.getRentPrice();
			this.rentPeriod = merchant.getRentPeriod();
			this.rentPeriodStreakLimit = merchant.getRentPeriodStreakLimit();
			this.rentPeriodStreakLimitDelay = merchant.getRentPeriodStreakLimitDelay();
			this.rentConditions = merchant.getRentConditions();
			this.unrentedParticlePatternId = merchant.getUnrentedParticlePatternId();
		}

	}

	@Override
	public Class<?> getJsonDataClass() {
		return RentableBlockMerchantJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		RentableBlockMerchantJsonData data = (RentableBlockMerchantJsonData) jsonData;
		setActualOwner(data.owner);
		setBlock(data.block);
		setSign(data.sign);
		setDisplayName(data.displayName);
		setShopsIds(data.shopsIds);
		setInteractConditions(data.interactConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setParticlePatternId(data.particlePatternId);
		setCurrentOwner(data.currentOwner);
		setPastPaidRents(data.pastPaidRents);
		setPaidRents(data.paidRents);
		setLastCheckRent(data.lastCheckRent);
		setAwaitingForceUnrent(data.awaitingForceUnrent);
		setRentable(data.rentable);
		setRentPrice(data.rentPrice);
		setRentPeriod(data.rentPeriod);
		setRentPeriodStreakLimit(data.rentPeriodStreakLimit);
		setRentPeriodStreakLimitDelay(data.rentPeriodStreakLimitDelay);
		setRentConditions(data.rentConditions);
		setUnrentedParticlePatternId(data.unrentedParticlePatternId);
	}

	@Override
	public Object writeJsonData() {
		return new RentableBlockMerchantJsonData(this);
	}

}
