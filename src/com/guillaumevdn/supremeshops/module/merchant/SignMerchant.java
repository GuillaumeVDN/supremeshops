package com.guillaumevdn.supremeshops.module.merchant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedEvent;
import com.guillaumevdn.supremeshops.module.display.particles.SingleParticlesDisplayable;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayable;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.fake.FakeSignMerchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.Locatable;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;

public class SignMerchant extends Merchant implements Locatable, SignDisplayable, SingleParticlesDisplayable, ParticlePatternAssignable {

	// base
	private BlockCoords sign;
	private String particlePatternId = null;

	private SignMerchant(String id) {
		super(id, MerchantType.SIGN);
	}

	protected SignMerchant(String id, MerchantType type) {
		super(id, type);
	}

	public SignMerchant(String id, UserInfo owner, BlockCoords sign) {
		this(id, MerchantType.SIGN, owner, sign);
	}

	protected SignMerchant(String id, MerchantType type, UserInfo owner, BlockCoords sign) {
		super(id, type, owner);
		this.sign = sign;
	}

	// get
	@Override
	public World getWorld() {
		return sign.getWorld();
	}

	@Override
	public BlockCoords getBlock() {
		return getSign();
	}

	@Override
	public Location getLocation() {
		return sign.toLocation();
	}

	// sign displayable
	@Override
	public BlockCoords getSign() {
		return sign;
	}

	@Override
	public List<String> getSignLines() {
		return SSLocaleMisc.MISC_SUPREMESHOPS_SIGNMERCHANTSIGNLINES.getLines(getMessageReplacers(true, false, null));
	}

	// particle pattern
	@Override
	public String getParticlePatternId() {
		return particlePatternId;
	}

	@Override
	public ParticlePattern getParticlePattern() {
		return particlePatternId == null ? null : SupremeShops.inst().getModuleManager().getParticlePattern(particlePatternId);
	}

	@Override
	public List<ParticlePattern> getAvailableParticlePatterns(Player player) {
		List<ParticlePattern> patterns = new ArrayList<ParticlePattern>();
		for (ParticlePattern pattern : SupremeShops.inst().getModuleManager().getParticlePatterns().values()) {
			if (pattern.getAvailability().equals(ParticlePatternAvailability.SIGN) && (isCurrentOwnerAdmin() || (pattern.getPermission() == null || pattern.getPermission().has(getCurrentOwner().toOfflinePlayer())))) {
				patterns.add(pattern);
			}
		}
		return patterns;
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isOpen();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return getParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return areInteractConditionsValid(player, false);
	}

	@Override
	public Location getParticlesBase() {
		return getBlock().toLocation();
	}

	// set
	protected void setSign(BlockCoords sign) {
		this.sign = sign;
	}

	protected void setParticlePatternId(String particlePatternId) {
		this.particlePatternId = particlePatternId;
	}

	// methods
	@Override
	public List<Shop> destroy(DestroyCause cause, boolean push) {
		// remove sign
		try {
			sign.toBlock().getWorld().dropItem(sign.toLocation(), new ItemStack(Mat.fromBlock(sign.toBlock()).getCurrentMaterial()));
			Mat.AIR.setBlock(sign.toBlock());
		} catch (Throwable ignored) {
			try {
				sign.toBlock().breakNaturally();
			} catch (Throwable ignored2) {}
		}
		// destroy
		return super.destroy(cause, push);
	}

	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// location
		replacers.add("{location}");
		replacers.add(sign.toString());
		// return
		return replacers;
	}

	// methods : change
	@Override
	public void changeDisplayName(String displayName) {
		super.changeDisplayName(displayName);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeOpen(boolean open) {
		super.changeOpen(open);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeActualOwner(UserInfo owner) {
		super.changeActualOwner(owner);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	// methods : shops
	@Override
	public void changeParticlePattern(String particlePatternId) {
		// set id
		setParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set particle pattern to" + particlePatternId);
	}

	// fake
	@Override
	public SignMerchant asFake() {
		return new FakeSignMerchant(this);
	}

	// data class
	public static final class SignMerchantJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final List<String> shopsIds;
		private final CPConditions interactConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final Map<UserInfo, Set<MerchantManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final BlockCoords sign;
		private final String particlePatternId;

		private SignMerchantJsonData(SignMerchant merchant) {
			this.owner = merchant.getActualOwner();
			this.displayName = merchant.getDisplayName();
			this.shopsIds = merchant.getShopsIds();
			this.interactConditions = merchant.getInteractConditions();
			this.open = merchant.isOpen();
			this.remote = merchant.isRemote();
			this.tradesLimit = merchant.getTradesLimit();
			this.managers = merchant.getManagers();
			this.managersWages = merchant.getManagersWages();
			this.lastPaidManagersWages = merchant.getLastPaidManagersWages();
			this.lateManagersWages = merchant.getLateManagersWages();
			this.buyers = merchant.getBuyers();
			this.sign = merchant.getSign();
			this.particlePatternId = merchant.getParticlePatternId();
		}

	}

	public Class<?> getJsonDataClass() {
		return SignMerchantJsonData.class;
	}

	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		SignMerchantJsonData data = (SignMerchantJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setShopsIds(data.shopsIds);
		setInteractConditions(data.interactConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setSign(data.sign);
		setParticlePatternId(data.particlePatternId);
	}

	public Object writeJsonData() {
		return new SignMerchantJsonData(this);
	}

}
