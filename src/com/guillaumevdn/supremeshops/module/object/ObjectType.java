package com.guillaumevdn.supremeshops.module.object;

import java.lang.reflect.Constructor;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.object.type.ObjectCommands;
import com.guillaumevdn.supremeshops.module.object.type.ObjectCommandsLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItemLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectMerchant;
import com.guillaumevdn.supremeshops.module.object.type.ObjectMerchantLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectPlayerPointsPoints;
import com.guillaumevdn.supremeshops.module.object.type.ObjectPlayerPointsPointsLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectShop;
import com.guillaumevdn.supremeshops.module.object.type.ObjectShopLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectTokenEnchantTokens;
import com.guillaumevdn.supremeshops.module.object.type.ObjectTokenEnchantTokensLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoneyLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectXpLevel;
import com.guillaumevdn.supremeshops.module.object.type.ObjectXpLevelLogic;

public final class ObjectType implements Comparable<ObjectType> {

	// special
	public static final ObjectType COMMANDS = registerType("COMMANDS", ObjectCommands.class, Mat.COMMAND_BLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_COMMANDS.getLine(), new ObjectCommandsLogic());
	public static final ObjectType ITEM = registerType("ITEM", ObjectItem.class, Mat.DIAMOND_PICKAXE, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_ITEM.getLine(), new ObjectItemLogic());
	public static final ObjectType MERCHANT = registerType("MERCHANT", ObjectMerchant.class, Mat.EMERALD, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_MERCHANT.getLine(), new ObjectMerchantLogic());
	public static final ObjectType PLAYERPOINTS_POINTS = registerType("PLAYERPOINTS_POINTS", ObjectPlayerPointsPoints.class, Mat.GOLD_INGOT, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_PLAYERPOINTSPOINTS.getLine(), new ObjectPlayerPointsPointsLogic(), "PlayerPoints");
	public static final ObjectType SHOP = registerType("SHOP", ObjectShop.class, Mat.CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_SHOP.getLine(), new ObjectShopLogic());
	public static final ObjectType TOKENENCHANT_TOKENS = registerType("TOKENENCHANT_TOKENS", ObjectTokenEnchantTokens.class, Mat.GOLD_INGOT, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_TOKENENCHANT_TOKENS.getLine(), new ObjectTokenEnchantTokensLogic(), "TokenEnchant");
	public static final ObjectType VAULT_MONEY = registerType("VAULT_MONEY", ObjectVaultMoney.class, Mat.EMERALD, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_VAULTMONEY.getLine(), new ObjectVaultMoneyLogic(), "Vault");
	public static final ObjectType XP_LEVEL = registerType("XP_LEVEL", ObjectXpLevel.class, Mat.EXPERIENCE_BOTTLE, SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTTYPE_XPLEVEL.getLine(), new ObjectXpLevelLogic());

	// registration
	public static ObjectType registerType(String id, Class<? extends TradeObject> objectClass, Mat icon, String name, ObjectLogic logic, String... requiredPlugins) {
		id = id.toUpperCase();
		ObjectType type = new ObjectType(id, objectClass, icon, name, logic, requiredPlugins);
		SupremeShops.inst().getModuleManager().getObjectTypes().put(id.toLowerCase(), type);
		return type;
	}

	public static ObjectType valueOf(String id) {
		return id != null ? SupremeShops.inst().getModuleManager().getObjectTypes().get(id.toLowerCase()) : null;
	}

	public static List<ObjectType> values() {
		return Utils.asList(SupremeShops.inst().getModuleManager().getObjectTypes().values());
	}

	// base
	private String id;
	private Class<? extends TradeObject> objectClass;
	private Mat icon;
	private String name;
	private ObjectLogic logic;
	private List<String> requiredPlugins;

	private ObjectType(String id, Class<? extends TradeObject> objectClass, Mat icon, String name, ObjectLogic logic, String... requiredPlugins) {
		this.id = id;
		this.objectClass = objectClass;
		this.icon = icon;
		this.name = name;
		this.logic = logic;
		this.requiredPlugins = requiredPlugins == null ? Utils.emptyList() : Utils.asList(requiredPlugins);
	}

	// overriden methods
	@Override
	public boolean equals(Object obj) {
		return Utils.instanceOf(obj, ObjectType.class) && getId().equalsIgnoreCase(((ObjectType) obj).getId());
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public int compareTo(ObjectType o) {
		return String.CASE_INSENSITIVE_ORDER.compare(getId(), o.getId());
	}

	// get
	public String getId() {
		return id;
	}

	public Class<? extends TradeObject> getObjectClass() {
		return objectClass;
	}

	public Mat getIcon() {
		return icon;
	}

	public String getName() {
		return name;
	}

	public ObjectLogic getLogic() {
		return logic;
	}

	public List<String> getRequiredPlugins() {
		return requiredPlugins;
	}

	// methods
	public boolean hasAllRequiredPlugins() {
		for (String plugin : requiredPlugins) {
			if (!Utils.isPluginEnabled(plugin)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Unregister the shop type
	 * @return null
	 */
	public ObjectType unregister() {
		SupremeShops.inst().getModuleManager().getObjectTypes().remove(id.toLowerCase());
		return null;
	}

	public TradeObject createNew(String id, ObjectSide side, boolean canEditSide, Parseable parent, ConfigData data, boolean loadOrSave, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// invalid plugins
		for (String requiredPlugin : this.getRequiredPlugins()) {
			if (!Utils.isPluginEnabled(requiredPlugin)) {
				(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("trade object type " + getId() + " requires plugin " + requiredPlugin + " to be enabled");
				return null;
			}
		}
		// create
		try {
			// create instance
			Constructor<? extends TradeObject> constructor = getObjectClass().getDeclaredConstructor(String.class, ObjectSide.class, boolean.class, Parseable.class, boolean.class, int.class, Mat.class, List.class);
			TradeObject component = constructor.newInstance(id, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
			// load or save data
			if (loadOrSave) {
				component.load(data);
			} else {
				component.save(data);
			}
			// return
			return component;
		}
		// couldn't create
		catch (Throwable exception) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("unknown error when creating trade object " + id + " (" + getObjectClass().getName() + ") with type " + getId());
			exception.printStackTrace();
			return null;
		}
	}

}
