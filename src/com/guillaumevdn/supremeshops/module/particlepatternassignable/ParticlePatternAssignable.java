package com.guillaumevdn.supremeshops.module.particlepatternassignable;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

/**
 * @author GuillaumeVDN
 */
public interface ParticlePatternAssignable {

	String getParticlePatternId();
	ParticlePattern getParticlePattern();
	List<ParticlePattern> getAvailableParticlePatterns(Player player);
	Object[] getMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser);

	void changeParticlePattern(String particlePatternId);

}
