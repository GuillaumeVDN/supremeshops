package com.guillaumevdn.supremeshops.module.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedEvent;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayable;
import com.guillaumevdn.supremeshops.module.display.particles.SingleParticlesDisplayable;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayable;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeBlockShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.Locatable;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScript;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScriptExecution;

public class BlockShop extends PlayerShop implements Locatable, SignDisplayable, ItemsDisplayable, SingleParticlesDisplayable, ParticlePatternAssignable {

	// base
	private BlockCoords block;
	private BlockCoords sign;
	private boolean displayItems = true;
	private String particlePatternId;

	private BlockShop(String id) {
		super(id, ShopType.BLOCK);
	}

	protected BlockShop(String id, ShopType type) {
		super(id, type);
	}

	public BlockShop(String id, UserInfo owner, BlockCoords block, BlockCoords sign) {
		this(id, ShopType.BLOCK, owner, block, sign);
	}

	protected BlockShop(String id, ShopType type, UserInfo owner, BlockCoords block, BlockCoords sign) {
		super(id, type, owner, false, false);
		this.block = block;
		this.sign = sign;
	}

	// get
	@Override
	public World getWorld() {
		return block.getWorld();
	}

	@Override
	public BlockCoords getBlock() {
		return block;
	}

	@Override
	public Location getLocation() {
		return block.toLocation();
	}

	@Override
	public BlockCoords getSign() {
		return sign;
	}

	@Override
	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	// particle pattern
	@Override
	public String getParticlePatternId() {
		return particlePatternId;
	}

	@Override
	public ParticlePattern getParticlePattern() {
		return particlePatternId == null ? null : SupremeShops.inst().getModuleManager().getParticlePattern(particlePatternId);
	}

	@Override
	public List<ParticlePattern> getAvailableParticlePatterns(Player player) {
		List<ParticlePattern> patterns = new ArrayList<ParticlePattern>();
		for (ParticlePattern pattern : SupremeShops.inst().getModuleManager().getParticlePatterns().values()) {
			if (pattern.getAvailability().equals(ParticlePatternAvailability.BLOCK) && (isCurrentOwnerAdmin() || (pattern.getPermission() == null || pattern.getPermission().has(getCurrentOwner().toOfflinePlayer())))) {
				patterns.add(pattern);
			}
		}
		return patterns;
	}

	// sign displayable
	@Override
	public List<String> getSignLines() {
		return SSLocaleMisc.MISC_SUPREMESHOPS_BLOCKSHOPSIGNLINES.getLines(getMessageReplacers(true, false, null));
	}

	// items displayable
	@Override
	public BlockCoords getItemsDisplayBase() {
		return getBlock();
	}

	@Override
	public BlockCoords getItemsDisplayFacingBase() {
		return getSign();
	}

	@Override
	public boolean getDisplayItems() {
		return displayItems;
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isOpen();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return getParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return areTradeConditionsValid(player, false);
	}

	@Override
	public Location getParticlesBase() {
		return getBlock().toLocation();
	}

	// set
	protected void setBlock(BlockCoords block) {
		this.block = block;
	}

	protected void setSign(BlockCoords sign) {
		this.sign = sign;
	}

	protected void setDisplayItems(boolean displayItems) {
		this.displayItems = displayItems;
	}

	protected void setParticlePatternId(String particlePatternId) {
		this.particlePatternId = particlePatternId;
	}

	// methods
	@Override
	public List<Merchant> destroy(DestroyCause cause, boolean push) {
		// remove block and sign
		try {
			block.toBlock().getWorld().dropItem(block.toLocation(), new ItemStack(Mat.fromBlock(block.toBlock()).getCurrentMaterial()));
			Mat.AIR.setBlock(block.toBlock());
		} catch (Throwable ignored) {
			try {
				block.toBlock().breakNaturally();
			} catch (Throwable ignored2) {}
		}
		try {
			sign.toBlock().getWorld().dropItem(sign.toLocation(), new ItemStack(Mat.fromBlock(sign.toBlock()).getCurrentMaterial()));
			Mat.AIR.setBlock(sign.toBlock());
		} catch (Throwable ignored) {
			try {
				sign.toBlock().breakNaturally();
			} catch (Throwable ignored2) {}
		}
		// remove display
		try {
			SupremeShops.inst().getGeneralManager().cleanDisplayItems(this);
		} catch (Throwable ignored) {}
		// destroy
		return super.destroy(cause, push);
	}

	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// location
		replacers.add("{location}");
		replacers.add(block.toString());
		// return
		return replacers;
	}

	// methods : change
	@Override
	public void changeDisplayName(String displayName) {
		super.changeDisplayName(displayName);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeOpen(boolean open) {
		super.changeOpen(open);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeActualOwner(UserInfo owner) {
		super.changeActualOwner(owner);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	public void changeDisplayItems(boolean displayItems) {
		// set display items
		setDisplayItems(displayItems);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.TOGGLE_DISPLAY_ITEMS));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, (displayItems ? "Enabled display items" : "Disabled display items"));
		// remove display items (will be re-added soon anyway)
		if (SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null) {
			SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().stop(this);
		}
	}

	@Override
	public void changeParticlePattern(String particlePatternId) {
		// set particle pattern id
		setParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, particlePatternId == null ? "Removed particle pattern" : "Set particle pattern to" + particlePatternId);
	}

	// methods : trade
	@Override
	public boolean attemptTradesProcessing(Player player, int trades, Merchant merchant) {
		// trade
		if (!super.attemptTradesProcessing(player, trades, merchant)) {
			return false;
		}
		// display on buy particle scripts
		ParticleScript script = SupremeShops.inst().getModuleManager().getOnBuyParticleScript(player);
		if (script != null) {
			final ParticleScriptExecution execution = new ParticleScriptExecution(script, block.toLocation());
			new BukkitRunnable() {
				@Override
				public void run() {
					if (execution.update(null, true)) {
						cancel();
					}
				}
			}.runTaskTimerAsynchronously(SupremeShops.inst(), 0L, 1L);
		}
		// refresh display
		if (SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null) {
			SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().getData(this).removeItems();
		}
		// ok
		return true;
	}

	// fake
	@Override
	public BlockShop asFake() {
		return new FakeBlockShop(this);
	}

	// data
	public static class BlockShopJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final Map<TradeObject, Double> objects;
		private final CPConditions tradeConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final boolean adminStock;
		private final Map<UserInfo, Set<ShopManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final BlockCoords block;
		private final BlockCoords sign;
		private final boolean displayItems;
		private final String particlePatternId;

		public BlockShopJsonData(BlockShop shop) {
			this.owner = shop.getActualOwner();
			this.displayName = shop.getDisplayName();
			this.objects = shop.getObjectsMap();
			this.tradeConditions = shop.getTradeConditions();
			this.open = shop.isOpen();
			this.remote = shop.isRemote();
			this.tradesLimit = shop.getTradesLimit();
			this.adminStock = shop.getAdminStock();
			this.managers = shop.getManagers();
			this.managersWages = shop.getManagersWages();
			this.lastPaidManagersWages = shop.getLastPaidManagersWages();
			this.lateManagersWages = shop.getLateManagersWages();
			this.buyers = shop.getBuyers();
			this.block = shop.getBlock();
			this.sign = shop.getSign();
			this.displayItems = shop.displayItems;
			this.particlePatternId = shop.getParticlePatternId();
		}

	}

	@Override
	public Class<?> getJsonDataClass() {
		return BlockShopJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		BlockShopJsonData data = (BlockShopJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setObjects(data.objects);
		setTradeConditions(data.tradeConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setAdminStock(data.adminStock);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setBlock(data.block);
		setSign(data.sign);
		setDisplayItems(data.displayItems);
		setParticlePatternId(data.particlePatternId);
	}

	@Override
	public Object writeJsonData() {
		return new BlockShopJsonData(this);
	}

}
