package com.guillaumevdn.supremeshops.module.shop;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeMerchantShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public class MerchantShop extends PlayerShop {

	// base
	private String merchantId;

	private MerchantShop(String id) {
		super(id, ShopType.MERCHANT);
	}

	public MerchantShop(String id, UserInfo owner, String merchantId) {
		super(id, ShopType.MERCHANT, owner, false, true);
		this.merchantId = merchantId;
	}

	// get
	public String getMerchantId() {
		return merchantId;
	}

	public Merchant getMerchant() {
		return SupremeShops.inst().getData().getMerchants().getElement(merchantId);
	}

	@Override
	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	// set
	protected void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	// methods
	@Override
	public List<Merchant> destroy(DestroyCause cause, boolean push) {
		// unlink from merchant
		Merchant merchant = getMerchant();
		if (merchant != null) {
			merchant.removeShop(this, false);
		}
		// destroy
		return super.destroy(cause, push);
	}

	// fake
	@Override
	public MerchantShop asFake() {
		return new FakeMerchantShop(this);
	}

	// data
	public static class MerchantShopJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final Map<TradeObject, Double> objects;
		private final CPConditions tradeConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final boolean adminStock;
		private final Map<UserInfo, Set<ShopManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final String merchantId;

		public MerchantShopJsonData(MerchantShop shop) {
			this.owner = shop.getActualOwner();
			this.displayName = shop.getDisplayName();
			this.objects = shop.getObjectsMap();
			this.tradeConditions = shop.getTradeConditions();
			this.open = shop.isOpen();
			this.remote = shop.isRemote();
			this.tradesLimit = shop.getTradesLimit();
			this.adminStock = shop.getAdminStock();
			this.managers = shop.getManagers();
			this.managersWages = shop.getManagersWages();
			this.lastPaidManagersWages = shop.getLastPaidManagersWages();
			this.lateManagersWages = shop.getLateManagersWages();
			this.buyers = shop.getBuyers();
			this.merchantId = shop.getMerchantId();
		}

	}

	@Override
	public Class<MerchantShopJsonData> getJsonDataClass() {
		return MerchantShopJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		MerchantShopJsonData data = (MerchantShopJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setObjects(data.objects);
		setTradeConditions(data.tradeConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setAdminStock(data.adminStock);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setMerchantId(data.merchantId);
	}

	@Override
	public Object writeJsonData() {
		return new MerchantShopJsonData(this);
	}

}
