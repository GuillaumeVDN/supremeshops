package com.guillaumevdn.supremeshops.module.shop.fake;

import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public interface FakeShop {

	void withObject(TradeObject object, Double stock);
	void withTradeCondition(Condition condition);
	
}
