package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPLocation;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

public class AdminShopTriggerBlock extends Trigger {

	// base
	private PPLocation block = addComponent(new PPLocation("block", this, null, true, SETTINGS_SLOT_START, EditorGUI.ICON_BLOCK, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_BLOCKLORE.getLines()));

	public AdminShopTriggerBlock(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.BLOCK, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPLocation getBlock() {
		return block;
	}

	public Location getBlock(Player parser) {
		return block.getParsedValue(parser);
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(Player player) {
		final Location block = getBlock(player);
		return block == null ? Utils.emptyList(BaseLocationFetcher.class) : Utils.asList(new BaseLocationFetcher() {
			@Override
			public Location getBaseLocation() {
				return block;
			}
		});
	}

}
