package com.guillaumevdn.supremeshops.task;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.data.DataManager.BackEnd;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.BlockMerchant;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.SignMerchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class InvalidMerchantDestroyerTask extends BukkitRunnable {

	@Override
	public void run() {
		// check all known merchants
		Set<Merchant> merchantsToDestroy = new HashSet<Merchant>();
		boolean disableBlockValidity = SupremeShops.inst().getData().getBackEnd().equals(BackEnd.MYSQL) && SupremeShops.inst().getConfiguration().getBoolean("data.sync_disable_blockvalidity", false);
		for (Merchant merchant : SupremeShops.inst().getData().getMerchants().getAll().values()) {
			// block merchant
			if (merchant instanceof BlockMerchant) {
				// no sign
				BlockCoords coords = ((BlockMerchant) merchant).getSign();
				if (coords == null) {
					merchantsToDestroy.add(merchant);
					continue;
				}
				// chunk isn't loaded, check it later
				if (coords.getWorld() == null || !coords.getWorld().isChunkLoaded(coords.getX() >> 4, coords.getZ() >> 4)) {
					continue;
				}
				// sign isn't a sign or block isn't there
				if (!disableBlockValidity) {
					Block block = coords.toBlock();
					if (!Mat.fromBlock(block).getModernName().contains("SIGN") || Mat.fromBlock(block = ((BlockMerchant) merchant).getBlock().toBlock()).isAir()) {
						merchantsToDestroy.add(merchant);
						continue;
					}
				}
			}
			// sign merchant
			if (merchant instanceof SignMerchant) {
				// no sign
				BlockCoords coords = ((BlockMerchant) merchant).getSign();
				if (coords == null) {
					merchantsToDestroy.add(merchant);
					continue;
				}
				// chunk isn't loaded, check it later
				if (coords.getWorld() == null || !coords.getWorld().isChunkLoaded(coords.getX() >> 4, coords.getZ() >> 4)) {
					continue;
				}
				// sign isn't a sign
				if (!disableBlockValidity) {
					Block block = ((SignMerchant) merchant).getSign().toBlock();
					if (!Mat.fromBlock(block).getModernName().contains("SIGN")) {
						merchantsToDestroy.add(merchant);
						continue;
					}
				}
			}
			// owner is online and the merchant doesn't match existence conditions
			if (!merchant.isCurrentOwnerAdmin() && !(merchant instanceof Rentable)) {
				Player owner = merchant.getCurrentOwner() != null ? merchant.getCurrentOwner().toPlayer() : null;
				if (owner != null) {
					if (!SupremeShops.inst().getModuleManager().canMerchantExist(owner, merchant, true)) {
						merchantsToDestroy.add(merchant);
						continue;
					}
				}
			}
		}
		// destroy merchants
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		if (!merchantsToDestroy.isEmpty()) {
			for (Merchant merchant : merchantsToDestroy) {
				shopsToDestroy.addAll(merchant.destroy(DestroyCause.INVALID_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<Merchant>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.INVALID_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		moreMerchantsToPush.removeAll(merchantsToDestroy);
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
		// log
		if (!merchantsToDestroy.isEmpty() || !shopsToDestroy.isEmpty()) {
			SupremeShops.inst().pluginLog(null, null, null, null, "{INVALID-MERCHANT-DESTROYER} ", "Destroyed " + merchantsToDestroy.size() + " merchant" + Utils.getPlural(merchantsToDestroy.size()) + " (and " + shopsToDestroy.size() + " shop" + Utils.getPlural(shopsToDestroy.size()) + ")");
		}
	}

}
