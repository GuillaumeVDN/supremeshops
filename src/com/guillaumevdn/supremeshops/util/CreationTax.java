package com.guillaumevdn.supremeshops.util;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPDouble;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public class CreationTax extends ContainerParseable {

	// base
	private CPConditions conditions = addComponent(new CPConditions("conditions", this, false, 0, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_CONDITIONSLORE.getLines()));
	private PPDouble vaultMoneyTax = addComponent(new PPDouble("vault_money_tax", this, "100", 0d, Double.MAX_VALUE, true, 1, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_VAULTMONEYTAXLORE.getLines()));

	public CreationTax(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "shop creation tax", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public CPConditions getConditions() {
		return conditions;
	}

	public PPDouble getVaultMoneyTax() {
		return vaultMoneyTax;
	}

	public Double getVaultMoneyTax(Player parser) {
		return vaultMoneyTax.getParsedValue(parser);
	}

	// clone
	protected CreationTax() {
	}

	@Override
	public CreationTax clone() {
		return (CreationTax) super.clone();
	}

}
