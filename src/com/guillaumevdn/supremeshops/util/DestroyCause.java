package com.guillaumevdn.supremeshops.util;

public enum DestroyCause {
	
	PURGE_PLAYER_COMMAND,
	INVALID_DESTROYER_TASK,
	INACTIVE_DESTROYER_TASK,
	EDITION_DESTROY_BUTTON,
	RELOAD_ADMIN_SHOP,
	ALLOWED_BREAK,
	MERCHANT_DESTROYED,
	MERCHANT_UNRENTED,
	AREASHOP_REGION_END
	
}
