package com.guillaumevdn.supremeshops.util;

import org.bukkit.entity.Entity;

/**
 * @author GuillaumeVDN
 */
public class GravityUtils {

	public static final void setGravity(Entity entity, boolean gravity) {
		try {
			entity.setGravity(gravity);
		} catch (Throwable ignored) {}
	}

}
