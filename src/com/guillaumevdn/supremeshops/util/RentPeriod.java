package com.guillaumevdn.supremeshops.util;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.supremeshops.SSLocaleMisc;

public enum RentPeriod {

	// values
	DAILY(SSLocaleMisc.MISC_SUPREMESHOPS_DAILY),
	WEEKLY(SSLocaleMisc.MISC_SUPREMESHOPS_PERIODWEEKLY),
	MONTHLY(SSLocaleMisc.MISC_SUPREMESHOPS_PERIODMONTHLY);

	// base
	private Text name;

	private RentPeriod(Text name) {
		this.name = name;
	}

	// get
	public Text getName() {
		return name;
	}

}
