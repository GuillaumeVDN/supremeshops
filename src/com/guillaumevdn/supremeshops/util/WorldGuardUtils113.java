package com.guillaumevdn.supremeshops.util;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.World;

public class WorldGuardUtils113 {

	public static boolean isLocInRegion(Location location, World regionWorld, String regionId) {
		if (!location.getWorld().equals(regionWorld)) {
			return false;
		}
		try {
			Object wgInstance = Class.forName("com.sk89q.worldguard.WorldGuard").getMethod("getInstance").invoke(null);
			Object wgPlatform = wgInstance.getClass().getMethod("getPlatform").invoke(wgInstance);
			Object rgContainer = wgPlatform.getClass().getMethod("getRegionContainer").invoke(wgPlatform);
			Object bukkitWorld = Class.forName("com.sk89q.worldedit.bukkit.BukkitWorld").getConstructor(Class.forName("org.bukkit.World")).newInstance(regionWorld);
			Object rgContainerInstance = rgContainer.getClass().getMethod("get", Class.forName("com.sk89q.worldedit.world.World")).invoke(rgContainer, bukkitWorld);
			Object vectorLocation = Class.forName("com.sk89q.worldedit.bukkit.BukkitAdapter").getMethod("asBlockVector", Location.class).invoke(null, location);
			Object regionSet = rgContainerInstance.getClass().getMethod("getApplicableRegions", Class.forName("com.sk89q.worldedit.math.BlockVector3")).invoke(rgContainerInstance, vectorLocation);
			Collection<?> regions = (Collection<?>) regionSet.getClass().getMethod("getRegions").invoke(regionSet);
			for (Object region : regions) {
				String id = (String) region.getClass().getMethod("getId").invoke(region);
				if (id.equalsIgnoreCase(regionId)) {
					return true;
				}
			}
			return false;
		} catch (Throwable exception) {
			exception.printStackTrace();
			return false;
		}
	}

}
