package com.guillaumevdn.supremeshops.util.parseable.list;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ListParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.ParseableContainment;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public class LPTradeObject extends ListParseable<TradeObject> implements ParseableContainment<TradeObject> {

	// base
	public LPTradeObject(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "condition", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	@Override
	public TradeObject createElement(String elementId) {
		// create data
		ConfigData data = new ConfigData(getLastData().getPlugin(), getLastData().getSuperId(), getLastData().getConfig(), getLastData().getPath().isEmpty() ? elementId : getLastData().getPath() + "." + elementId);
		// create
		TradeObject element = ObjectType.ITEM.createNew(elementId, ObjectSide.GIVING, true, this, data, false, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

	@Override
	public TradeObject loadElement(String elementId, ConfigData data) {
		// create
		ObjectSide side = data.getConfig().contains(data.getPath() + ".side") ? data.getConfig().getEnumValue(data.getPath() + ".side", ObjectSide.class, (ObjectSide) null) : null;
		TradeObject element = TradeObject.load(elementId, side == null ? ObjectSide.GIVING : side, true, this, data, false, -1, getEditorIcon(), null);
		// add and return
		if (element != null) {
			addElement(element);
		}
		return element;
	}

	@Override
	public void replaceContaining(TradeObject element) {
		addElement(element);
	}

}
