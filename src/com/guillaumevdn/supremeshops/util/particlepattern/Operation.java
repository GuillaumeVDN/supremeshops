package com.guillaumevdn.supremeshops.util.particlepattern;

import java.util.Collection;

import org.bukkit.entity.Player;

public interface Operation {

	/**
	 * @param execution the execution instance
	 * @param players the players to perform the action to, or null if none (all actions might not be affected by this parameter)
	 * @return the amount of ticks to wait before executing the next operation
	 */
    int perform(ParticleScriptExecution execution, Collection<Player> players, boolean isAsync);

}
