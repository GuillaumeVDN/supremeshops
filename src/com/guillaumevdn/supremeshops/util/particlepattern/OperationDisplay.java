package com.guillaumevdn.supremeshops.util.particlepattern;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.util.Handler;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.particle.ParticleManager;

public class OperationDisplay implements Operation {

	// base
	private ParticleManager.Type effect;
	private String x, y, z, count;

	public OperationDisplay(ParticleManager.Type effect, String x, String y, String z, String count) {
		this.effect = effect;
		this.x = x;
		this.y = y;
		this.z = z;
		this.count = count;
	}

	// get
	public ParticleManager.Type getEffect() {
		return effect;
	}

	public String getX() {
		return x;
	}

	public String getY() {
		return y;
	}

	public String getZ() {
		return z;
	}

	public String getCount() {
		return count;
	}

	// methods
	@Override
	public int perform(final ParticleScriptExecution execution, final Collection<Player> players, boolean isAsync) {
		// calculate
		final double x = execution.parseAndCalculate(this.x);
		final double y = execution.parseAndCalculate(this.y);
		final double z = execution.parseAndCalculate(this.z);
		final int count = (int) execution.parseAndCalculate(this.count);
		// sync
		if (!isAsync) {
			new Handler() {
				@Override
				public void execute() {
					Location baseLocation = execution.getBaseLocation();
					if (players == null) {
						ParticleManager.INSTANCE.send(effect, new Location(baseLocation.getWorld(), x, y, z), 0f, count, baseLocation.getWorld());
					} else {
						ParticleManager.INSTANCE.send(effect, new Location(baseLocation.getWorld(), x, y, z), 0f, count, players);
					}
				}
			}.runSync();
		}
		// don't resync
		else {
			Location baseLocation = execution.getBaseLocation();
			ParticleManager.INSTANCE.send(effect, new Location(baseLocation.getWorld(), x, y, z), 0f, count, baseLocation.getWorld());
		}
		return 0;
	}

}
